<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ include file="/WEB-INF/views/Header.jsp" %>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
      <%@page import="java.sql.*"%>
      <%@page import="util.DbUtil"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

  <title>Campsite Registration</title>
  
  
  
      

</head>
<!-- <script type="text/javascript">
 function update(){
	 alert("Active Details Updated Successfully...")
 }

</script> -->

<style>

.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
table {
    border: 1px solid black;
    border-collapse: collapse;
    width: 80%;
    margin: 0% 0% 1% 9%
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #3399ff;
    color: white;
}

</style>
 

 </head>
 
 
<body>
<br>
<br>


  <div class='container'>
    <div class='panel panel-primary dialog-panel'>
      <div class='panel-heading'>
        <center><h5>ACTIVE DETAILS</h5></center>
      </div>
     <div class='panel-body'>
        <form:form action="UpdateATS_Studies.html" ModelAttribute="command">
          <div class='form-group'>
          <input class='form-control' name='rid' id='rid' value="${atsstudies.rid}" type='hidden'>
           <input class='form-control' name='ats_id' id='ats_id' value="${atsstudies.ats_id}" type='hidden'>
            <label class='control-label col-md-2 col-md-offset-1' for='id_accomodation'>Analytical Studies:</label>
             
            
              <div class='col-md-2'>
                <div class='form-group internal'>
                  <select class='form-control' name='analyticalstudies' id='analyticalstudies'>
                  <option><c:out value="${atsstudies.analyticalstudies}"/></option>
                  <c:forEach items="${analysis}" var="analysis">
                    <option><c:out value="${analysis.analysisname}"/></option>
                   
                     </c:forEach>
                  </select>
                </div>
              
              
            </div>
             <div class='form-group'>
        
            <label class='control-label col-md-2 col-md-offset-1' for='id_accomodation'>Days:</label>
             
          
              <div class='col-md-2'>
                <div class='form-group internal'>
                  <select class='form-control' name='days' id='days'>
                  <option><c:out value="${atsstudies.days}"/></option>
                  <c:forEach items="${atslist}" var="atslist">
                    <option><c:out value="${atslist.days_description}"/></option>
                   
                     </c:forEach>
                  </select>
                </div>
              
              
            </div>
            
             <label class='control-label col-md-2 col-md-offset-1' for='concentration'>Remarks:</label>
            <div class='col-md-2'>
             <input class='form-control' name='remarks' id='remarks' value="${atsstudies.remarks}" type='text'>
            </div>
          </div>
            
<% 

Connection con  = DbUtil.getConnection(); 
PreparedStatement stmt1=con.prepareStatement("select * from active where rid="+session.getAttribute("rid"));
 System.out.println("select * from active where rid="+session.getAttribute("rid"));
 ResultSet rs1=stmt1.executeQuery();
 int i=0;
 while(rs1.next()){
	i++;
 }
 
 PreparedStatement stmt=con.prepareStatement("select * from atsstudies1 where rid="+session.getAttribute("rid")+" AND ats_id="+session.getAttribute("ats_id") );
 System.out.println("select * from atsstudies1 where rid="+session.getAttribute("rid")+" AND ats_id="+session.getAttribute("ats_id") );
 ResultSet rs=stmt.executeQuery();
 int j=0;
 while(rs.next()){
	 System.out.println(rs.getString("ats_active"));
%>
   
             </br></br> 
                    <label class='control-label col-md-2 col-md-offset-1' for='id_accomodation' style="margin-top: 10px;"><%=rs.getString("ats_active") %>:</label>
                    <input class='form-control' name='<%=rs.getString("ats_active") %>' id='<%=rs.getString("ats_active") %>' value="<%=rs.getString("ats_active") %>" type='hidden'>
                    <div class='col-md-2'>
                    <input class='form-control' name='C<%=rs.getString("ats_active") %>' id='C<%=rs.getString("ats_active") %>' value="<%=rs.getString("concentration") %>" type='text' style="margin-top: 10px;">
                    </div>
         
  	  <%
     }
 
  %>  
           <label class='control-label col-md-2 col-md-offset-1' for='id_accomodation' style="margin-top: 10px;">Temperature :</label>
                   
                    <div class='col-md-2'>
                    <input class='form-control' name='temp' id='temp' value="${atsstudies.temp}" type='text' >
                    </div>
                    
                     <label class='control-label col-md-2 col-md-offset-1' for='id_accomodation' style="margin-top: 10px;">Humidity:</label>
                   
                    <div class='col-md-2'>
                    <input class='form-control' name='humidity' id='humidity' value="${atsstudies.humidity}" type='text' >
                    </div>
         
  
          
           
        </br></br></br>
          <div class='form-group'>
            <div class='col-md-offset-2 col-md-4'>
              <button class='btn-lg btn-primary' type='submit' onclick="save()">SUBMIT</button>
              
            </div>
            
          </div>
        </form:form>
      </div>
    </div>
  </div>
  </br>
  </br>
  <%-- <h2 align="center"> ATS Studies</h2></br></br>
   <table>
					  <tr>
					    <th>#</th>
					    <th>Analytical Study</th>
					    <th>Days</th>
					    <th>Remarks</th>
					    <th>Emamectin</th>
					    <th>Edit</th>
					    <th>delete</th>
					  </tr>
					   
					  <c:forEach items="${atsstudies1}" var="atsstudies1">
                        <tr>
					    <td><c:out value="${atsstudies1.ats_id}"/></td>
					    <td><c:out value="${atsstudies1.analyticalstudies}"/></td>
					    <td><c:out value="${atsstudies1.days}"/></td>
					    <td><c:out value="${atsstudies1.remarks}"/></td>
					    <td><c:out value="${atsstudies1.emamectin}"/></td>
					    <td> <a href="EditATS_Studies.html?ats_id=${atsstudies1.ats_id}"><font color="Blue">Edit</font></a></td>
					    <td> <a href="DeleteATS_Studies.html?ats_id=${atsstudies1.ats_id}"><font color="Red">Delete </font></a></td>
					    </tr>
					  </c:forEach>
					    	
</table>
 --%>

  
 


</body>
</html>