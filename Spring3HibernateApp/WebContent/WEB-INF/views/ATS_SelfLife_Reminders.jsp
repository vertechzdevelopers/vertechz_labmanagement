<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ include file="/WEB-INF/views/Header.jsp" %>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

  <title>Campsite Registration</title>
  
  
  
  <head>
  <!-- <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css'> -->
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/css/bootstrap-datetimepicker.min.css'>
<link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
 <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
 <!-- <script src='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js'></script>  -->
<script src='http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js'></script>

    <script src="js3/index1.js"></script>    

</head>

<!-- <script type="text/javascript">
	function save() {
		alert("Active Details added Successfully");
	}
</script> -->
<style>

.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
table {
    border: 1px solid black;
    border-collapse: collapse;
    width: 80%;
     margin: 0% 0% 1% 9%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #3399ff;
    color: white;
}

</style>
 

 </head>
 
 
<body>
<br>
<br>


  <div class='container'>
    <div class='panel panel-primary dialog-panel'>
      <div class='panel-heading'>
        <center><h5>ATS/Selflife ReminderDate</h5></center>
      </div>
      <div class='panel-body'>
        <form:form action="SaveATS_SelfLife_Reminders.html" ModelAttribute="command">
          <div class='form-group'>
          <input class='form-control' name='rid' id='rid' value="${records.rid}" type='hidden'>
          <input class='form-control' name='date' id='date' value="${records.date}" type='hidden'>
          <input class='form-control' name='referance_no' id='referance_no' value="${records.referance_no}" type='hidden'>
          <input class='form-control' name='objective' id='objective' value="${records.objective}" type='hidden'>
          <input class='form-control' name='formulation' id='formulation' value="${records.formulation}" type='hidden'>
          <input class='form-control' name='research_executive' id='research_executive' value="${records.research_executive}" type='hidden'>
          <input class='form-control' name='status' id='status' value="${records.status}" type='hidden'>
          <input class='form-control' name='observations' id='observations' value="${records.observations}" type='hidden'>
          <input class='form-control' name='approved_by' id='approved_by' value="${records.approved_by}" type='hidden'>
          <input class='form-control' name='process_details' id='process_details' value="${records.process_details}" type='hidden'>
             
          
          
            <label class='control-label col-md-2 col-md-offset-1' for='id_accomodation'>Reminder Date:</label>
            <div class='col-md-2'>
            <div class='input-group date' id='datetimepicker1' style="margin-right: -14px;">
             <input class='form-control' name='reminder_date' id='reminder_date' placeholder='reminder_date' type='text'>
             <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                </span>
          </div>
            </div>
          </div>
          
         
  
          
           
        </br></br></br>
          <div class='form-group'>
            <div class='col-md-offset-2 col-md-4'>
              <button class='btn-lg btn-primary' type='submit' style="margin-top: 0px;border-top-width: 0px;border-top-style: solid;padding-top: 6px;padding-bottom: 6px;">SUBMIT</button>
              <a href="Close_&_Return_Records.html?rid=${records.rid}" class='btn-lg btn-primary'  type='submit'>Close & Return</a>
            </div>
            
          </div>
        </form:form>
      </div>
    </div>
  </div>
  </br>
  </br>
  <h2 align="center"> Active Details</h2></br></br>
   <table>
					  <tr>
					   <!--  <th>#</th> -->
					    <th>REMINDER DATE</th>
					    <th>EDIT</th>
					    
					  </tr>
					  <tr>
					
					    <td><c:out value="${records.reminder_date}"/></td>
					    <td> <a href="EditATS_SelfLife_Reminders.html?rid=${records.rid}"><font color="Blue"><img src="img/edit.png" ></font></a></td>
					     </tr>
				
					    	
</table>


  
 


</body>
</html>