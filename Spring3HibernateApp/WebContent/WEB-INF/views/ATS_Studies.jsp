  <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@include file="/WEB-INF/views/Header.jsp" %>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
           <%@page import="java.sql.*"%>
      <%@page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

  <title>Campsite Registration</title>
  
  
  
      

</head>

<!-- <script type="text/javascript">
	function save() {
		alert("Active Details added Successfully");
	}
</script> -->
<style>

.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
table {
    border: 1px solid black;
    border-collapse: collapse;
    width: 80%;
    margin:0% 0% 1% 9%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #3399ff;
    color: white;
}

</style>
 

 </head>
 
 
<body>
<br>
<br>
 <form:form action="SaveATS_Studies.html" ModelAttribute="command">

  <div class='container'>
    <div class='panel panel-primary dialog-panel' style="margin-bottom: 23px;padding-bottom: 60px;">
      <div class='panel-heading'>
        <center><h5>ATS Studies</h5></center>
     <!-- <label class='control-label col-md-2 col-md-offset-1' style="margin: 1% 0% 0% 31%;" for='concentration'>Temp:</label>
            
             <input  name='temp' id='temp' style="width: 10%;margin: 1% 0% 0% -27%;" type='text'>
              <label class='control-label col-md-2 col-md-offset-1' style="margin: 1% 0% 0% 0%;" for='concentration'>Humidity:</label>
            
             <input  name='humidity' id='humidity' style="width: 10%;margin: 1% 0% 0% 9%;" type='text'>   -->
        
      </div>
     <label  style="margin: 1% 0% 0% 31%;" for='concentration'>Temp:</label> 
            
             <input  name='temp' id='temp' style="width: 10%;margin: 1% 0% 0% 0%;"  placeholder='e.g.21�C' type='text' required="required">
              <label  style="margin: 1% 0% 0% 0%;" for='concentration'>Humidity:</label>
            
             <input  name='humidity' id='humidity' style="width: 10%;margin: 1% 0% 0% 0%;"  placeholder='e.g.70%RH' type='text' required="required">  
      <div class='panel-body'>
       
          <div class='form-group'>
          <input class='form-control' name='rid' id='rid' value="${records.rid}" type='hidden'>
            <label class='control-label col-md-2 col-md-offset-1' for='id_accomodation'>Analytical Studies:</label>
             
            
              <div class='col-md-2'>
                <div class='form-group internal'>
                  <select class='form-control' name='analyticalstudies' id='analyticalstudies' required="true">
                  <option></option>
                  <c:forEach items="${analysis}" var="analysis">
                    <option><c:out value="${analysis.analysisname}"/></option>
                   
                     </c:forEach>
                  </select>
              
              </div>
              
            </div>
             <div class='form-group'>
        
            <label class='control-label col-md-2 col-md-offset-1' for='id_accomodation'>Days:</label>
             
           
              <div class='col-md-2'>
                <div class='form-group internal'>
                  <select class='form-control' name='days' id='days'>
                  
                  <c:forEach items="${atslist}" var="atslist">
                    <option><c:out value="${atslist.days_description}"/></option>
                   
                     </c:forEach>
                  </select>
                
              </div>
              
            </div>
            
             <label class='control-label col-md-2 col-md-offset-1' for='concentration'>Remarks:</label>
            <div class='col-md-2'>
           <!--   <input class='form-control' name='remarks' id='remarks' placeholder='remarks' type='text'> -->
               <textarea class='form-control' name='remarks' id='remarks'  ></textarea>
             
            </div>
          </div>
          
         <!--  <label class='control-label col-md-2 col-md-offset-1' for='concentration'>Temp:</label>
            <div class='col-md-2'>
             <input class='form-control' name='temp' id='temp' placeholder='e.g.21�C' type='text'>
             <br>
            </div> -->
          </div>
          
          <!--  <label class='control-label col-md-2 col-md-offset-1' for='concentration'>Humidity:</label>
            <div class='col-md-2'>
             <input class='form-control' name='humidity' id='humidity' placeholder='e.g.70%RH' type='text'>
            </div> -->
          </div> 
            
            <c:forEach items="${ active}" var="active">
                    <br><br> 
                    <label class='control-label col-md-2 col-md-offset-1' for='id_accomodation' style="margin-top: -35px;"><c:out value="${active.active}"/>:</label>
                    <input class='form-control' name='${active.active}' id='${active.active}' value="${active.active}" type='hidden' style="margin-top: 10px;">
                    <div class='col-md-2' style="margin-left: 525px;">
                    <input class='form-control' name='C${active.active}' id='C${active.active}' style="margin-top: -36px;" type='text' style="margin-top: 10px;">
                    </div>
                    
                  </c:forEach>
          
         
  
          
           
        <br>
          <div class='form-group'>
            <div class='col-md-offset-2 col-md-4' >
              <button class='btn-lg btn-primary' type='submit' onclick="save()">SUBMIT</button>
              <a href="Close_&_Return_Records.html?rid=${records.rid}" class='btn-lg btn-primary'  type='submit'>Close & Return</a>
            </div>
            
          </div>
        
      </div>
    </div>
 </div>
 </div>
 
  </form:form>
 <!--  <h2 align="center"> ATS Studies</h2></br></br> -->
   <table>
					  <tr>
					   <!--  <th>#</th> -->
					    <th>Analytical Study</th>
					    <th>Days</th>
					    <th>Remarks</th>
					    <th>Temp</th>
					    <th>Humidity</th>
					    
					      <c:forEach items="${ active}" var="active">
					    <th><c:out value="${active.active}"/></th>
					    </c:forEach>
					    <th>Edit</th>
					    <th>delete</th>
					  </tr>
					   
<% 
try {
Class.forName("com.mysql.jdbc.Driver");

 String []analytical_active={};
 String []concentration={};
 Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/labmanagement","root","root");  
PreparedStatement stmt1=con.prepareStatement("select * from active where rid="+session.getAttribute("rid"));
 System.out.println("select * from active where rid="+session.getAttribute("rid"));
 ResultSet rs1=stmt1.executeQuery();
 int i=0;
 while(rs1.next()){
	i++;
 }
 
 PreparedStatement stmt=con.prepareStatement("select * from atsstudies where rid="+session.getAttribute("rid") + "  order by temp");
 System.out.println("select * from atsstudies where rid="+session.getAttribute("rid") );
 ResultSet rs=stmt.executeQuery();
 int j=0;
 while(rs.next()){
	 System.out.println(rs.getString("analyticalstudies"));
%>	

                        <tr>
					  <%-- <td><%=rs.getString("ats_id") %></td> --%>
					    <td><%=rs.getString("analyticalstudies") %></td>
					    <td><%=rs.getString("days") %></td>
					    <td><%=rs.getString("remarks") %></td>
					     <td><%=rs.getString("temp") %></td>
					      <td><%=rs.getString("humidity") %></td>

<%
PreparedStatement stmt2=con.prepareStatement("select * from atsstudies1 where ats_id="+rs.getInt("ats_id"));
System.out.println("select * from atsstudies1 where ats_id="+rs.getInt("ats_id"));
ResultSet rs2=stmt2.executeQuery();
ArrayList<String> al=new ArrayList<String>(); 
String active[]=new String[i];
while(rs2.next()){
	System.out.println("analytical_active"+rs2.getString("ats_active"));
	System.out.println("concentration"+rs2.getString("concentration"));
      al.add(rs2.getString("concentration"));
     System.out.println("i: "+i);
    
}
active=al.toArray(active);
                         for(int z=0; z<active.length; z++){ 
                        	 if(active[z]!= null){
%>					    
					    <td><%= active[z] %></td> 
<%}else{ %>                  
                          <td><%=" " %></td> 
  <%} } %> 
					   
					    <td> <a href="EditATS_Studies.html?ats_id=<%= rs.getInt("ats_id")%>"><font color="Blue"><img src="img/edit.png"></font></a></td>
					    <td> <a href="DeleteATS_Studies.html?ats_id=<%= rs.getInt("ats_id")%>&rid=<%= rs.getInt("rid")%>"><font color="Red"><img src="img/delete.png"></font></a></td>

					    </tr>
 <%                       }
	
 } catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
} 
%>
					    	
</table>

</body>
</html>