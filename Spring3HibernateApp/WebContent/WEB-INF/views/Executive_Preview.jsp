<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@page import="java.util.List"%>
          <%@ include file="/WEB-INF/views/Header_Exe.jsp" %>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
      <%@page import="java.sql.*"%>
      <%@page import="java.util.*"%>
      <%@page import="util.DbUtil"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<script>
function myFunction() {
    window.print();
}
</script>
<style>
.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
.pagination {
    background-color: #3399ff;
    border: solid;
    color: white;
    padding: 3px 5px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 12px;
    margin: 4px 2px;
    cursor: pointer;
}
.spanValue{
	background-color: #F01212;
	 border: solid;
    color: white;
    padding: 3px 5px;
	text-align: center;
	text-decoration: none;
	font-size: 12px;
}
table 
{
    border: 1px solid black;
    border-collapse: collapse;
    width: 100%;
    margin : 0% 0% 1% 0%;
}
th, td {
    text-align: left;
    padding: 8px;
}
tr:nth-child(even){background-color: #f2f2f2}
th {
    background-color: #634aa84d;
    color: white;
}
</style>
</head>
<body>

<br>
<br>




<div class="col-md-1"></div>
<div class="col-md-6">
<h2 align="center">RECORD PREVIEW</h2>
<button onclick="myFunction()" style="margin-left: 950px;margin-top: -40px;margin-bottom: 20px;">Print</button>

</br></br>

  <table class="table table-bordered" Style="border: 1px solid black;">
					    <!-- <thead>
					      <tr>
					        <th></th>
					        <th></th>
					      </tr>
					    </thead> -->
					    <tbody>              
                      
					  <%-- <tr class="success">
					    <td>#</td>
					    <td><c:out value="${records.rid}"/></td>
					  </tr>  --%>
					  <tr class="success">
					    <td>DATE</td>
					    <td><c:out value="${records.date}"/></td>					    
					  </tr> 
					  <tr class="success">
					    <td>REFERENCE NO</td>
					    <td><c:out value="${records.referance_no}"/></td>
					  </tr> 
					   <tr class="success">
					    <td>RESEARCH EXECUTIVE</td>
					    <td><c:out value="${records.research_executive}"/></td>
					  </tr> 
					   <tr class="success">
					    <td>APPROVED BY</td>
					    <td><c:out value="${records.approved_by}"/></td>
					  </tr>
					    <tr class="success">
					    <td>FORMULATION TYPE</td>
					    <td><c:out value="${records.formulation}"/></td>
					  </tr> 
					  <tr class="success">
					    <td>OBJECTIVE</td>
					    <td><c:out value="${records.objective}"/></td>
					  </tr>
					   <tr class="success">
					    <td>PROCESS DETAILS</td>
					    <td><c:out value="${records.process_details}"/></td>
					  </tr>
					   <tr class="success">
					    <td>OBSERVATIONS/REMARKS</td>
					    <td><c:out value="${records.observations}"/></td>
					  </tr> 
					
					 
					  <tr class="success">
					    <td >STATUS</td>
					    <td><c:out value="${records.status}"/></td>
					  </tr> 
					  
					   
					  </tbody>	
		  
 </table>
 </br>
 <h2 align="center">ACTIVE DETAILS</h2>
</br>
   <table class="table table-bordered">
    <thead>
      <tr class="success">
       <!--  <th><font color="black">#</font></th> -->
        <th><font color="black">ACTIVE</font></th>
        <th><font color="black">CONCENTRATION</font></th>
      </tr>
    </thead>
    <tbody>
    <c:forEach items="${active}" var="active">
      <tr>
        <%-- <td><c:out value="${active.active_id}"/></td> --%>
        <td><c:out value="${active.active}"/></td>
        <td><c:out value="${active.concentration}"/></td>
      </tr>
     </c:forEach>
    </tbody>
  </table>
  
   </br>
 <h2 align="center">COMPOSITION DETAILS</h2>
</br>
   <table class="table table-bordered">
    <thead>
      <tr class="success">
       <!--  <th><font color="black">#</font></th> -->
         <th><font color="black">CONSTITUENT</font></th>
        <th><font color="black">AMOUNT</font></th>
      
        <th><font color="black">PERCENTAGE</font></th>
      </tr>
    </thead>
    <tbody>
    <c:forEach items="${composition}" var="composition">
      <tr>
        <%-- <td><c:out value="${composition.composition_id}"/></td> --%>
        <td><c:out value="${composition.constituent}"/></td>
        <td><c:out value="${composition.amount}"/></td>
        
        <td><c:out value="${composition.percentage}"/></td>
      </tr>
     </c:forEach>
    </tbody>
  </table>
     </br>
 <h2 align="center">ANALYTICAL STUDIES</h2>
</br>
   <table class="table table-bordered">
    <thead>
      <tr class="success">
        <th><font color="black">ANALYTICAL STUDY</font></th>
        <th><font color="black"><c:out value="${records.formulation}"/></font></th>
       <c:forEach items="${active}" var="active">
		 <th><font color="black"><c:out value="${active.active}"/></font></th>
		 </c:forEach>
      </tr>
    </thead>
    <tbody>
<% 
try {

 Connection con  = DbUtil.getConnection();
 String []analytical_active={};
 String []concentration={};
 PreparedStatement stmt1=con.prepareStatement("select * from active where rid="+session.getAttribute("rid"));
 System.out.println("select * from active where rid="+session.getAttribute("rid"));
 ResultSet rs1=stmt1.executeQuery();
 int i=0;
 while(rs1.next()){
	i++;
 }
 
 PreparedStatement stmt=con.prepareStatement("select * from analyticalstudy where rid="+session.getAttribute("rid"));
 System.out.println("select * from analyticalstudy where rid="+session.getAttribute("rid"));
 ResultSet rs=stmt.executeQuery();
 int j=0;
 while(rs.next()){
	 System.out.println(rs.getString("analytical_study"));
%>

                        
                        <tr>
					   <%-- <td><%=rs.getString("analyticalstudy_id") %></td> --%>
					    <td><%= rs.getString("analytical_study")%></td>
					    <td><%= rs.getString("formulation")%></td>
<%
PreparedStatement stmt2=con.prepareStatement("select * from analyticalstudy1 where analyticalstudy_id="+rs.getInt("analyticalstudy_id"));
System.out.println("select * from analyticalstudy1 where analyticalstudy_id="+rs.getInt("analyticalstudy_id"));
ResultSet rs2=stmt2.executeQuery();
ArrayList<String> al=new ArrayList<String>(); 
String active[]=new String[i];
while(rs2.next()){
	System.out.println("analytical_active"+rs2.getString("analytical_active"));
	System.out.println("concentration"+rs2.getString("concentration"));
      al.add(rs2.getString("concentration"));
     System.out.println("i: "+i);
    
}
active=al.toArray(active);
                         for(int z=0; z<active.length; z++){ 
                        	 if(active[z]!= null){
%>					    
					    <td><%= active[z] %></td> 
<%}else{ %>                  
                          <td><%=" " %></td> 
  <%} } %> 
      </tr>
                        
 <%                       }
	
 } catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
} 
%>
    </tbody>
  </table>
  
    </br>
 <h2 align="center">SCAN COPY</h2>
</br>
   <table class="table table-bordered">
    <thead>
      <tr class="success">
       <!--  <th><font color="black">#</font></th> -->
       <th><font color="black">Name</font></th>
        <th><font color="black">Download</font></th>
       
      </tr>
    </thead>
    <tbody>
    <c:forEach items="${scancopy}" var="scancopy">
      <tr>
        <%-- <td><c:out value="${composition.composition_id}"/></td> --%>
        <td><c:out value="${scancopy.name}"/></td>
        <td><a href="UploadDownloadFileServlet?file=${scancopy.name}"><font color="Blue">Download</font></a></td>
      </tr>
     </c:forEach>
    </tbody>
  </table>
  
    </br>
 <h2 align="center">PHOTO / VIDEOS</h2>
</br>
   <table class="table table-bordered">
    <thead>
      <tr class="success">
       <!--  <th><font color="black">#</font></th> -->
       <th><font color="black">Name</font></th>
        <th><font color="black">Download</font></th>
       
      </tr>
    </thead>
    <tbody>
    <c:forEach items="${images}" var="images">
      <tr>
        <%-- <td><c:out value="${composition.composition_id}"/></td> --%>
        <td><c:out value="${images.name}"/></td>
        <td> 
				<img class="player"  height="100px" width="200px" src="DisplayImage1.html?id=<c:out value="${images.id}"/>"/>
        </td>
      </tr>
     </c:forEach>
    </tbody>
  </table>
  
     </br>
 <h2 align="center">COLD TEST</h2>
</br>
   <table class="table table-bordered">
    <thead>
      <tr class="success">
        <!-- <th><font color="black">#</font></th> -->
        <th><font color="black">DEGREE</font></th>
        <th><font color="black">OBSERVATION</font></th>
        <th><font color="black">TEST DATE</font></th>
      </tr>
    </thead>
    <tbody>
    <c:forEach items="${coldtest}" var="coldtest">
      <tr>
        <%-- <td><c:out value="${coldtest.cold_id}"/></td> --%>
        <td><c:out value="${coldtest.degree}"/></td>
        <td><c:out value="${coldtest.observation}"/></td>
        <td><c:out value="${coldtest.testdate}"/></td>
      </tr>
     </c:forEach>
    </tbody>
  </table>
  
     </br>
 <h2 align="center">SELFLIFE STUDIES</h2>
</br>
   <table class="table table-bordered">
    <thead>
      <tr class="success">
        <!-- <th><font color="black">#</font></th> -->
        <th><font color="black">DAYS</font></th>
        <th><font color="black">OBSERVATION</font></th>
        <th><font color="black">STUDY DATE</font></th>
      </tr>
    </thead>
    <tbody>
    <c:forEach items="${selflifestudy}" var="selflifestudy">
      <tr>
        <%-- <td><c:out value="${coldtest.cold_id}"/></td> --%>
        <td><c:out value="${selflifestudy.days}"/></td>
        <td><c:out value="${selflifestudy.observation}"/></td>
        <td><c:out value="${selflifestudy.studydate}"/></td>
      </tr>
     </c:forEach>
    </tbody>
  </table>
  
     </br>
 <h2 align="center">ATS STUDIES</h2>
</br>
   <table class="table table-bordered">
    <thead>
      <tr class="success">
        <th><font color="black">#</font></th>
        <th><font color="black">ANALYTICAL STUDIES</font></th>
        <th><font color="black">DAYS</font></th>
       <c:forEach items="${ active}" var="active">
					    <th><font color="black"><c:out value="${active.active}"/></font></th>
		</c:forEach>
        <th><font color="black">REMARKS</font></th>
      </tr>
    </thead>
    <tbody>
<% 
try {
	Connection con  = DbUtil.getConnection();

 String []analytical_active={};
 String []concentration={};

PreparedStatement stmt1=con.prepareStatement("select * from active where rid="+session.getAttribute("rid"));
 System.out.println("select * from active where rid="+session.getAttribute("rid"));
 ResultSet rs1=stmt1.executeQuery();
 int i=0;
 while(rs1.next()){
	i++;
 }
 
 PreparedStatement stmt=con.prepareStatement("select * from atsstudies where rid="+session.getAttribute("rid"));
 System.out.println("select * from atsstudies where rid="+session.getAttribute("rid"));
 ResultSet rs=stmt.executeQuery();
 int j=0;
 while(rs.next()){
	 System.out.println(rs.getString("analyticalstudies"));
%>	

                        <tr>
					  <%--  <td><%=rs.getString("ats_id") %></td> --%>
					    <td><%=rs.getString("analyticalstudies") %></td>
					    <td><%=rs.getString("days") %></td>
					    
					 
<%
PreparedStatement stmt2=con.prepareStatement("select * from atsstudies1 where ats_id="+rs.getInt("ats_id"));
System.out.println("select * from atsstudies1 where ats_id="+rs.getInt("ats_id"));
ResultSet rs2=stmt2.executeQuery();
ArrayList<String> al=new ArrayList<String>(); 
String active[]=new String[i];
while(rs2.next()){
	System.out.println("analytical_active"+rs2.getString("ats_active"));
	System.out.println("concentration"+rs2.getString("concentration"));
      al.add(rs2.getString("concentration"));
     System.out.println("i: "+i);
    
}
active=al.toArray(active);
                         for(int z=0; z<active.length; z++){ 
                        	 if(active[z]!= null){
%>					    
					    <td><%= active[z] %></td> 
<%}else{ %>                  
                          <td><%=" " %></td> 
  <%} } %> 
                        <td><%=rs.getString("remarks") %></td>
      </tr>
 <%                       }
	
 } catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
} 
%>
    </tbody>
  </table>
      </br>
 <h2 align="center">COMMENTS</h2>
</br>
   <table class="table table-bordered">
    <thead>
      <tr class="success">
       <!--  <th><font color="black">#</font></th> -->
        <th><font color="black">COMMENT</font></th>
        <th><font color="black">DATE</font></th>
      </tr>
    </thead>
    <tbody>
    <c:forEach items="${recordcomment}" var="recordcomment">
      <tr>
        <%-- <td><c:out value="${composition.composition_id}"/></td> --%>
        <td><c:out value="${recordcomment.comment}"/></td>
        <td><c:out value="${recordcomment.date}"/></td>
        
      </tr>
     </c:forEach>
    </tbody>
  </table>
     </br> 
</div>
<div class="col-md-5"></div>
</body>
</html>