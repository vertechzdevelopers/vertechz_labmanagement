<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ include file="/WEB-INF/views/Header.jsp" %>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

  <title>Campsite Registration</title>
  
  
  
      

</head>
<!-- <script type="text/javascript">
	function save() {
		alert("Composition Details added Successfully");
	}
</script> -->

<style>

.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
table {
    border: 1px solid black;
    border-collapse: collapse;
     width: 80%;
    margin: 0% 0% 1% 9%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #3399ff;
    color: white;
}

</style>
 

 </head>
 
 
<body>
<br>
<br>


  <div class='container'>
    <div class='panel panel-primary dialog-panel'>
      <div class='panel-heading'>
        <center><h2>Composition Details</h2></center>
      </div>
      <div class='panel-body'>
        <form:form action="SaveComposition.html" ModelAttribute="command">
          <div class='form-group'>
          <input class='form-control' name='rid' id='rid' value="${records.rid}" type='hidden' >
            <label class='control-label col-md-2 col-md-offset-1' for='constituent'>Constituent:</label>
            <div class='col-md-2'>
             <input class='form-control' name='constituent' id='constituent' placeholder='Constituent' type='text' required="required">
            </div>
            <br><br>
             <label class='control-label col-md-2 col-md-offset-1' for='concentration'>Amount:</label>
            <div class='col-md-2'>
            <!--  <input class='form-control' name='amount' id='amount' placeholder='Amount' type='text'> -->
              <input class='form-control' name='amount' id='amount' placeholder='Constituent' type='text' required="required">
            </div>
            <br><br>
            <label class='control-label col-md-2 col-md-offset-1' for='percentage'>%(w/w):</label>
            <div class='col-md-2'>
             <input class='form-control' name='percentage' id='percentage' placeholder='Percentage' type='text' required="required">
            </div>
          </div>    
        
        <br>
          <div class='form-group'>
            <div class='col-md-offset-2 col-md-4'>
              <button class='btn-lg btn-primary' type='submit'  >SUBMIT</button>
              <a href="Close_&_Return_Records.html?rid=${records.rid}" class='btn-lg btn-danger'  type='submit'>Close & Return</a>
            </div>

          </div>
        </form:form>
      </div>
    </div>
  </div>
  <br>
 
   <table>
					  <tr>
					   <!--  <th>#</th> -->
					    <th>constituent</th>
					    <th>Amount</th>
					    <th>%(w/w)</th>
					    <th>Edit</th>
					    <th>Delete</th>
					  </tr>
					   
					  <c:forEach items="${composition}" var="composition">
                        <tr>
					  <%--   <td><c:out value="${composition.composition_id}"/></td> --%>
					    <td><c:out value="${composition.constituent}"/></td>
					    <td><c:out value="${composition.amount}"/></td>
					    <td><c:out value="${composition.percentage}"/></td>
					    <td> <a href="EditComposition.html?composition_id=${composition.composition_id}"><font color="Blue"><img src="img/edit.png" ></font></a></td>
					    <td> <a href="DeleteComposition.html?composition_id=${composition.composition_id}&rid=${records.rid}"><font color="Red"><img src="img/delete.png"> </font></a></td>
					    </tr>
					  </c:forEach>
					    	
</table>

</body>
</html>