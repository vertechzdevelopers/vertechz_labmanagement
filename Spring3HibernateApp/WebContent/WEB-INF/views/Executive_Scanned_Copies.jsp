<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ include file="/WEB-INF/views/Header_Exe.jsp" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
<script type="text/javascript">
	function save() {
		alert("Scan Copy Uploaded Successfully");
	}
</script>

.button {
    background-color: #4CAF50;
    border: none;
    border-radius:5px;
    color: white;
	padding: 8px 25px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
    
}
table {
    border: 0px solid #337ab7;
    border-collapse: collapse;
    width: 80%;
    margin: 0% 0% 1% 9%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #3399ff;
    color: white;
}
</style>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Scan Copy</title>
</head>
<body>
<br>
<br>
<form:form method="POST" action="Executive_SaveScanned_Copies.html" modelAttrubute="command" enctype="multipart/form-data">
  <div class='container'>
    <div class='panel panel-primary dialog-panel'>
      <div class='panel-heading'>
        <center><h3>SCANNED COPIES</h3></center>
      </div>
      <div>
      
      <br><br>
      <input class='form-control' name='rid' id='rid' value="${records.rid}" type='hidden'>
       <center>
       
           <label>Documents :</label>
           <label class="btn-bs-file btn btn-sm btn-warning">
              <input type="file" name="file" id="file" />
            </label>
       </center>
      </div>
      <br><br>
      <div style="margin-bottom: 20px;">
      <center>
       <input class="btn-lg btn-primary" type="submit" name="upload" value="Upload" onclick="save()">
       <a href="Executive_Close_&_Return_Records.html?rid=${records.rid}" class='btn-lg btn-primary'  type='submit'>Close & Return</a>
           
       </center>
      </div>
      <div>
       </div>
	</div>
  </div>
 </form:form>
     <br> 
     <div class='panel-heading'>

       </div>
      
       <table>
  		<tr>
   		<!--  <th>#</th> -->
   		 <th>DOCUMENT</th>
   		 <th>Edit</th>
   		 <th>DELETE</th>
 		</tr>
 		 <c:forEach items="${scancopy}" var="scancopy">
 		<tr>
    	<%--  <td><c:out value="${scancopy.scan_id}"/> </td> --%>
    	 <td><c:out value="${scancopy.name}"/> </td>
    	 <td> <a href="Executive_EditScanned_Copies.html?scan_id=${scancopy.scan_id}"><font color="Blue"><img src="img/edit.png" ></a></td>
    	 <td> <a href="Executive_DeleteScanned_Copies.html?scan_id=${scancopy.scan_id}&rid=${records.rid}"><font color="Red"><img src="img/delete.png"></font></a></td>
					
  		</tr>
  		</c:forEach>
 	   </table>
       
       
     
</body>
</html>