<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ include file="/WEB-INF/views/Header_Exe.jsp" %>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

  <title>Campsite Registration</title>
  
  
  
      

<head>


<!-- <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css'> -->
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/css/bootstrap-datetimepicker.min.css'>
<!-- <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'> -->
 <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
 <!-- <script src='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js'></script>  -->
<script src='http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js'></script>

    <script src="js3/index1.js"></script>
 
 <style>
.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
.pagination {
    background-color: #3399ff;
    border: solid;
    color: white;
    padding: 3px 5px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 12px;
    margin: 4px 2px;
    cursor: pointer;
}
.spanValue{
	background-color: #F01212;
	 border: solid;
    color: white;
    padding: 3px 5px;
	text-align: center;
	text-decoration: none;
	font-size: 12px;
}
table 
{
    border: 1px solid black;
    border-collapse: collapse;
    width: 100%;
    margin : 0% 0% 1% 0%;
}
th, td {
    text-align: left;
    padding: 8px;
}
tr:nth-child(even){background-color: #f2f2f2}
th {
    background-color: #634aa84d;
    color: white;
}
</style>

 </head>
 
 
<body>
<br>
<br>

<c:if test="${!empty formulation}">
 <div class='container'>
  <div class="col-md-12">
   <div class="col-md-6">
    <div class='panel panel-primary dialog-panel'>
      <div class='panel-heading'>
        <center><h5>LAB RECORD REGISTRATION</h5></center>
      </div>
       <div class='panel-body'>
        <form:form action="Executive_RecordSearchByDate.html" method="post">
          <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-2' for='id_accomodation' style="padding-left: 0px;padding-right: 0px;margin-left: 50px;">FROM DATE :</label>
            <div class='col-md-6' style="margin-left: 0px;padding-left: 0px;">
             <div class='input-group date' id='datetimepicker1'>
              <input class='form-control' id='fromdate' name='fromdate' value=""  type='text'>
              <span class="input-group-addon">
               <span class="glyphicon glyphicon-calendar"></span>
              </span>
             </div>
            </div>
           </div>
          <div class="form-group">
             <label class='control-label col-md-3 col-md-offset-2' for='id_accomodation' style="margin-top: 010px;margin-left: 77px;padding-right: 0px;padding-left: 0px;">To DATE :</label>
             <div class='col-md-6' style="margin-left: -27px;padding-left: 0px;margin-top: 15px;">
             <div class='input-group date' id='datetimepicker1'>
             <input class='form-control' id='todate' name='todate' value=""    type='text'>
             <span class="input-group-addon">
              <span class="glyphicon glyphicon-calendar"></span>
             </span>
             </div>
            </div>
           </div>
           <div class='form-group'>
            <div class='col-md-offset-4 col-md-5'>
              <button class='btn-lg btn-primary' type='submit' style="margin-top: 20px;margin-left: 0px;padding-left: 5px;padding-right: 5px;padding-top: 5px;padding-bottom: 5px;">Search By Date</button>
            </div>
           </div>
           </form:form>
          </div>
           </br>
           </br>
        </div>
   </div> 
      
   <div class="col-md-6">
     <div class='panel panel-primary dialog-panel'>
       <div class='panel-heading'>
         <center><h5>LAB RECORD REGISTRATION</h5></center>
       </div>
       <div class='panel-body'>
        <form:form action="ExecutiveRecordSearch.html" method="post"> 
          <div class='form-group'>
            <label class='control-label col-md-5 col-md-offset-2' for='id_equipment' style="margin-top: 0px;margin-right:0px;margin-left: 0px;padding-left: 5px;padding-right: 0px;">FORMULATION TYPE :</label>
            <div class='col-md-6'>
              <div class='form-group internal'>
                  <select class='form-control' id='formulation' name='formulation' style="margin-left: 0px;padding-top: 6px;">
                   <option></option>
                   <c:forEach items="${formulation}" var="formulation">
                    <option><c:out value="${formulation.formulationtype}"/></option>
                    <!-- <option>Fifth wheel</option>
                    <option>RV/Motorhome</option>
                    <option>Tent trailer</option>
                    <option>Pickup camper</option>
                    <option>Camper van</option> -->
                   </c:forEach>
                  </select>    
              </div>
            </div>
           </div>
              <label class='control-label col-md-5 col-md-offset-2' for='id_accomodation' style="margin-top: 0px;margin-right: 0px;margin-left: 96px;">ACTIVE :</label>
            <div class='col-md-6' >
             <input class='form-control' id='active' name='active' placeholder='Active' type='text' style="margin-top: -18px;margin-left: 205px;">
            </div>
           
         
          <div class='form-group'>
            <label class='control-label col-md-5 col-md-offset-2' for='id_equipment' style="margin-top: 5px;margin-left: 92px;">STATUS :</label>
            <div class='col-md-6'>
               <div class='form-group internal'> 
                  <select class='form-control' id='status' name='status' style="margin-left: 206px;margin-top: -12px;margin-right: 0px;padding-left: 30px;padding-right: 0px;">
                   <option></option>
                   <option>In Process</option>
                   <option>Finalise</option>
                   <option>Rejected</option>
                  </select>
               </div> 
          </div>
          </div>
        
          <div class='form-group'>
            <div class='col-md-offset-4 col-md-5'>
              <button class='btn-lg btn-primary' type='submit' style="padding-top: 5px;padding-bottom: 5px;padding-right: 30px;border-left-width: 2px;border-left-style: solid;padding-left: 30px;margin-left: 44px;">Search</button>
            </div>
          </div>
        </form:form>
      </div>
     </div>
    </div>
 <!-- </div> -->
</c:if>
  

<table>
					  <tr>
					    <th>#</th>
					    <th>DATE</th>
					    <th>REFERENCE NO</th>
					    <th>OBJECTIVE</th>
					    <th>FORMULATION TYPE</th>
					    <th>RESEARCH EXECUTIVE</th>
					    <th>STATUS</th>
					    <th>EDIT</th>
					    <th>DELETE</th>
					  </tr>
					   
					  <c:forEach items="${records}" var="records">
                        <tr>
					    <td><c:out value="${records.rid}"/></td>
					    <td><c:out value="${records.date}"/></td>
					    <td><c:out value="${records.referance_no}"/></td>
					    <td><c:out value="${records.objective}"/></td>
					    <td><c:out value="${records.formulation}"/></td>
					    <td><c:out value="${records.research_executive}"/></td>
					    <td><c:out value="${records.status}"/></td>
					     <td> <a href="Executive_Preview.html?rid=${records.rid}"><font color="Blue">PREVIEW</font></a></td>
					    <td> <a href="Record_Comment_Executive.html?rid=${records.rid}"><font color="Red"><img src="img/comment2.png" height="35" width="70"></font></a></td>
					   </tr>
					    </c:forEach>			  
 </table>

 


</div>
</body>
</html>