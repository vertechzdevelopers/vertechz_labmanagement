<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ include file="/WEB-INF/views/Header_Exe.jsp" %>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
      <%@page import="com.dineshonjava.model.analyticalstudy"%>
      <%@page import="com.dineshonjava.model.Records"%>
      <%@page import="java.sql.*"%>
      <%@page import="java.util.*"%>
      <%@page import="java.util.*"%>
      <%@page import="util.DbUtil"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

  <title>Campsite Registration</title>
  
  
  
      

</head>

<!-- <script type="text/javascript">
	function save() {
		alert("Active Details added Successfully");
	}
</script> -->
<style>

.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
table {
    border: 1px solid black;
    border-collapse: collapse;
    width: 80%;
    margin: 0% 0% 1% 9%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #3399ff;
    color: white;
}

</style>
 

 </head>
 
 
<body>
<br>



  <div class='container'>
    <div class='panel panel-primary dialog-panel'>
      <div class='panel-heading'>
        <center><h3>Analytical Studies Details</h3></center>
      </div>
      <div class='panel-body'>
        <form:form action="Executive_SaveAnalytical_Studies.html" ModelAttribute="command">
           <div class='form-group'>
          <input class='form-control' name='rid' id='rid' value="${records.rid}" type='hidden'>
            <label class='control-label col-md-2 col-md-offset-1' for='id_accomodation'>Analytical Studies:</label>
             
           <!--   <div class='col-md-8'>  -->
              <div class='col-md-3'>
                <div class='form-group internal'>
                  <select class='form-control' name='analytical_study' id='analytical_study' required="true">
                  <option></option>
                  <c:forEach items="${analysis}" var="analysis" >
                  
                    <option><c:out value="${analysis.analysisname}"/></option>
                    <!-- <option>Fifth wheel</option>
                    <option>RV/Motorhome</option>
                    <option>Tent trailer</option>
                    <option>Pickup camper</option>
                    <option>Camper van</option> -->
                     </c:forEach>
                  </select>
                </div>
              </div>

          </div>    
            <label class='control-label col-md-2 col-md-offset-1' for='id_accomodation'>Formulation Type:</label>
                   
                    <div class='col-md-3'>
                    <input class='form-control' name='formulation1' id='formulation1' value="${records.formulation}" type='text' readonly="readonly">
                    
                    <input class='form-control' name='formulation' id='formulation'  type='text' >
                    
                    </div>  
                   

              <br>
                  <c:forEach items="${ active}" var="active">
                    </br></br>
                    <label class='control-label col-md-2 col-md-offset-1' for='id_accomodation'><c:out value="${active.active}"/>:</label>
                    <input class='form-control' name='${active.active}' id='${active.active}' value="${active.active}" type='hidden' >
                    <div class='col-md-3'>
                
                    <input class='form-control' name='C${active.active}' id='C${active.active}'  type='text'>
                    </div>
                    
                  </c:forEach>

         
  
          
           
  
          
           
   
          <div class='form-group'>
            <div class='col-md-offset-2 col-md-4' style="margin: 3% 0% 0% -28%;">
            </br>
              <button class='btn-lg btn-primary' type='submit' onclick="save()">SUBMIT</button>
              <a href="Executive_Close_&_Return_Records.html?rid=${records.rid}" class='btn-lg btn-primary'  type='submit'>Close & Return</a>
            </div>
            
          </div>
        </form:form>
      </div>
    </div>
  </div>
  </br>
 
   <table>             
					  <tr>
					    <!-- <th>#</th> -->
					    <th>Analytical Study</th>
					    <th><c:out value="${records.formulation}"/></th>
					     <c:forEach items="${ active}" var="active">
					    <th><c:out value="${active.active}"/></th>
					    </c:forEach>
					    <th>Edit</th>
					    <th>delete</th>
					  </tr>
<% 
try {


 String []analytical_active={};
 String []concentration={};
 Connection con  = DbUtil.getConnection(); 
PreparedStatement stmt1=con.prepareStatement("select * from active where rid="+session.getAttribute("rid"));
 System.out.println("select * from active where rid="+session.getAttribute("rid"));
 ResultSet rs1=stmt1.executeQuery();
 int i=0;
 while(rs1.next()){
	i++;
 }
 
 PreparedStatement stmt=con.prepareStatement("select * from analyticalstudy where rid="+session.getAttribute("rid"));
 System.out.println("select * from analyticalstudy where rid="+session.getAttribute("rid"));
 ResultSet rs=stmt.executeQuery();
 int j=0;
 while(rs.next()){
	 System.out.println(rs.getString("analytical_study"));
%>

                        
                        <tr>
					    <%-- <td><c:out value="${analyticalstudy.analyticalstudy_id}"/></td> --%>
					    <td><%= rs.getString("analytical_study")%></td>
					    <%if (rs.getString("analytical_study")!=null){ %>
					    <td><%= rs.getString("formulation")%></td>
					    <%}else{ %>
					    <td></td>
					    <%} %>
<%
PreparedStatement stmt2=con.prepareStatement("select * from analyticalstudy1 where analyticalstudy_id="+rs.getInt("analyticalstudy_id"));
System.out.println("select * from analyticalstudy1 where analyticalstudy_id="+rs.getInt("analyticalstudy_id"));
ResultSet rs2=stmt2.executeQuery();
ArrayList<String> al=new ArrayList<String>(); 
String active[]=new String[i];
while(rs2.next()){
	System.out.println("analytical_active"+rs2.getString("analytical_active"));
	System.out.println("concentration"+rs2.getString("concentration"));
      al.add(rs2.getString("concentration"));
     System.out.println("i: "+i);
    
}
active=al.toArray(active);
                         for(int z=0; z<active.length; z++){ 
                        	 if(active[z]!= null){
%>					    
					    <td><%= active[z] %></td> 
<%}else{ %>                  
                          <td><%=" " %></td> 
  <%} } %> 
					    <td> <a href="Executive_EditAnalytical_Studies.html?analyticalstudy_id=<%=rs.getInt("analyticalstudy_id")%>&rid=<%=rs.getInt("rid")%>"><font color="Blue"><img src="img/edit.png" > </font></a></td>
					    <td> <a href="Executive_DeleteAnalytical_Studies.html?analyticalstudy_id=<%=rs.getInt("analyticalstudy_id")%>&rid=<%=rs.getInt("rid")%>"><font color="Red"><img src="img/delete.png"></font></a></td>

					    </tr>
                      
                        
 <%                       }
	
 } catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
} 
%>
					    	
</table>

  
 


</body>
</html>
