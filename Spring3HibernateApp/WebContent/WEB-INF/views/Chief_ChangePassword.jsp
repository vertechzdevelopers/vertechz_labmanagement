<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ include file="/WEB-INF/views/Header.jsp" %>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

  <title>Camposite Registration</title> 
 
    
   
        
       <script type="text/javascript">
       
       
       
       var check = function() {
    	   if (document.getElementById('newPassword').value ==
    	     document.getElementById('cnfPassword').value) {
    	     document.getElementById('message').style.color = 'green';
    	     document.getElementById('message').innerHTML = 'matching';
    	   } else {
    	     document.getElementById('message').style.color = 'red';
    	     document.getElementById('message').innerHTML = 'not matching';
    	   }
    	 }
       </Script>
       
       
 </head>
 
<body>
<br>
<br>
<%-- <c:if test="${empty saveManager}"> --%>
<form:form method="POST" name="myform"  action="Chief_changePassword.html" onsubmit="return validate_form();" modelAttribute="command" >
  <div class='container'>
    <div class='panel panel-primary dialog-panel'>
      <div class='panel-heading'>
        <center><h5>Change Password</h5></center>
      </div>
      <div class='panel-body'>
        <form class='form-horizontal' role='form'>
          <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_accomodation'>UserID</label>
            <div class='col-md-2'>
             <input class='form-control' id='userName' name='userName'  type='text'   value=<%=UserName %> readonly="true">
            </div>
          </div>
          <br>          
           <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_accomodation'>OldPassword:</label>
            <div class='col-md-2'>
             <input class='form-control' id='oldPassword' name='oldPassword' placeholder='oldPassword' required="required" type='password'>
            </div>
          </div>
         <br>          
            <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_accomodation'>New Password</label>
            <div class='col-md-2'>
             <input class='form-control' id='newPassword' name='newPassword' placeholder='newpassword' required="required" type='password' onkeyup='check();'>
            </div>
          </div>
              
            <br>
           
            <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_accomodation'>Confirm New Password:</label>
            <div class='col-md-2'>
             <input class='form-control' id='cnfPassword' name='cnfPassword' placeholder='cnfnewpassword' type="password" onkeyup='check();'>
            <span id='message'></span>
         </div>
          </div>
             <br> 
            
      
        <br>
          <div class='form-group'>
            <div class='col-md-offset-4 col-md-3'style="margin:2% -3% 3% 33%;">
              <button class='btn-lg btn-primary' type='submit' >SUBMIT</button>
            </div>
            <div class='col-md-3' style="margin: 2% 0 0% -6%;">
              <button class='btn-lg btn-danger' style='float:left' type='submit'>Cancel</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
 </form:form>


</body>
</html>