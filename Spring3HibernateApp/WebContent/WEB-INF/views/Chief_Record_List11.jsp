<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
           <%@ include file="/WEB-INF/views/Header.jsp" %>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>

.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
table {
    border: 1px solid black;
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #3399ff;
    color: white;
}

</style>
</head>
<body>
<br>
<br>


<a href="ChiefRecordRegistration.html" class="button">ADD</a>
<a href="#" class="button">EDIT</a>

<c:if test="${!empty records}">
<table>
					  <tr>
					    <th>#</th>
					    <th>DATE</th>
					    <th>REFERENCE NO</th>
					    <th>OBJECTIVE</th>
					    <th>FORMULATION TYPE</th>
					    <th>Research Executive</th>
					    <th>STATUS</th>
					    <th>Edit</th>
					    <th>delete</th>
					  </tr>
					   
					  <c:forEach items="${records}" var="records">
                        <tr>
					    <td><c:out value="${records.rid}"/></td>
					    <td><c:out value="${records.date}"/></td>
					    <td><c:out value="${records.referance_no}"/></td>
					    <td><c:out value="${records.objective}"/></td>
					    <td><c:out value="${records.formulation}"/></td>
					    <td><c:out value="${records.research_executive}"/></td>
					    <td><c:out value="${records.status}"/></td>
					    <td> <a href="EditRecords.html?rid=${records.rid}"><font color="Blue">Edit</font></a></td>
					    <td> <a href="DeleteRecords.html?rid=${records.rid}"><font color="Red">Delete </font></a></td>
					    </tr>
					    </c:forEach>
					    
					  
 
	
</table>
</c:if>

</body>
</html>