<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ include file="/WEB-INF/views/Header.jsp" %>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<script
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<!-- <script type="text/javascript">
	function save() {
		alert("Formulation Type udpated Successfully");
	}
</script> -->
<script>
var app = angular.module('myApp', []);
app.controller('validateCtrl', function($scope) {
    $scope.noofRows=''; 
    $scope.rowNumber='';
   
});
</script>
<body  ng-app="myApp"  ng-controller="validateCtrl">
  <div class='container'>
    <div class='panel panel-primary dialog-panel' style="margin: 10% 0% 0% 0%;">
      <div class='panel-heading'>
        <center><h2>Analysis Types</h2></center>
      </div>
      <div class='panel-body'>
     <form:form method="POST" action="saveAnalysis.html">      
     	<div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_accomodation' style="margin: 0% 0% 0% 33%;">Formulation Id : </label>
            <div class='col-md-2' >
             <input class='form-control'  name="id" type='text' value="${aBean.id}"  readonly="readonly">
            </div>
          </div>         
           <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_accomodation' style="margin: 2% 0% 0% 33%;">Formulation Type : </label>
            <div class='col-md-2'>
             <input class='form-control'  name="analysisname" type='text' value="${aBean.analysisname}" style="margin: 10% 0% 0% 0%;">
            </div>
          </div>            
           <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_equipment' style="margin: 2% 0% 0% 33%;" >Status : </label>
           <div class='col-md-2' style="margin: 2% 0% 0% 0%;">
                  <select class='form-control'  name="status">
                  	<option value="${aBean.status}">${aBean.status}</option>
                    <option value="Enabled">Enabled</option>
                    <option value="Disabled">Disabled</option>
                  </select>
              </div>
          </div>          
          <br>          
          <div class='form-group'>
            <div class='col-md-offset-4 col-md-3'>
              <button class='btn-lg btn-primary' type='submit' style="margin: 10% 0% 0% 23%;" >SUBMIT</button>
            </div>
            <div class='col-md-3'>
              <button class='btn-lg btn-danger'  type='submit' style="margin: 10% 0% 0% -33%;">Cancel</button>
            </div>
          </div>
       </form:form>
      </div>
    </div>
  </div>
</body>
</html>