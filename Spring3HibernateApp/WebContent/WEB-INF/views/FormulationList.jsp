<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="java.util.List"%>
      <%@ include file="/WEB-INF/views/Header.jsp" %>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>

.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 0% 0% 1% 9%;
    cursor: pointer;
}
.pagination {
    background-color: #3399ff;
    border: solid;
    color: white;
    padding: 3px 5px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 12px;
    margin: 4px 2px;
    cursor: pointer;
}
.spanValue{
	background-color: #F01212;
	 border: solid;
    color: white;
    padding: 3px 5px;
	text-align: center;
	text-decoration: none;
	font-size: 12px;
}
table 
{
    border: 1px solid black;
    border-collapse: collapse;
    width: 80%;
    margin: 0% 0% 1% 9%;
}
th, td {
    text-align: left;
    padding: 8px;
}
tr:nth-child(even){background-color: #f2f2f2}
th {
    background-color: #3443b1cc;
    color: white;
}
</style>
</head>
<body>
<br>
<br>
<%
List<Integer>  pageList = (List<Integer>)session.getAttribute("pageList");
String offSet = request.getParameter("offSet");
 /* disabledLINK has been used to to make current page number nonhiperlink i.e unclickable
 e.g if user is at page number 15 then page number 15 should not be clickable*/
int disabledLINK = 0;
if(offSet!=null){
	disabledLINK = Integer.parseInt(offSet);
}
/* size is used for moving user to end page  by clicking on END link*/
int   size = Integer.parseInt(session.getAttribute("size").toString());

%>

<a href="formulation.html" class="button">ADD</a>
<!-- <a href="#" class="button">EDIT</a> -->
<c:if test="${!empty formulationList}">
	<table>
		<tr>
			<!-- <th>Formulation ID</th> -->
			<th>Formulation Type</th>
			<th>Status</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>

		<c:forEach items="${formulationList}" var="formulation">
			<tr>
				<%-- <td><c:out value="${formulation.id}"/></td> --%>
				<td><c:out value="${formulation.formulationtype}"/></td>
				<td><c:out value="${formulation.status}"/></td>	
				<td> <a href="editFormulation.html?id=${formulation.id}"><font color="Blue"><img src="img/edit.png" ></font></a></td>
				<td> <a href="deleteFormulation.html?id=${formulation.id}"><font color="Red"><img src="img/delete.png"></font></a></td>		
			</tr>
		</c:forEach>
	</table>
</c:if>
<%if(disabledLINK != 0){ %>
<!-- if user is on start page then it should not be visible to user or it should not be hyper-link-->
<a style="margin: 0% 0% 0% 9%;" href="formulationList.html?offSet=<%=0%>" class="pagination">Start</a>
<%}else{ %>
<span style="margin: 0% 0% 0% 9%;" class="spanValue">Start</span>
<%} %>
<%for(Integer i:pageList) {
if(disabledLINK == i ){
	if(disabledLINK!=size){
%>
<!-- Current page should not be hyper-link-->
<span class="spanValue"><%=i %></span>
<%-- <%=i %> --%>
<%}}else{ %>
<!-- page previous to current page and next to current page has to be hyper link  -->
<a href="formulationList.html?offSet=<%=i%>" class="pagination"><%=i+"" %></a>
<%}} %>
<%if(disabledLINK == size){ %>
<span class="spanValue">End</span>
<%}else{ %>
<!-- if user is on last page then it should not be visible to user or it should not be hyper-link-->
<%-- <a href="formulationList.html?offSet=<%=size%>" class="pagination">End</a> --%>
<%} %>
<!-- <table>
  <tr>
    <th>#</th>
    <th>DATE</th>
    <th>REFERENCE NO</th>
    <th>OBJECTIVE</th>
    <th>FORMULATION TYPE</th>
    <th>STATUS</th>
  </tr>
  <tr>
    <td>Peter</td>
    <td>Griffin</td>
    <td>$100</td>
    <td>Peter</td>
    <td>Griffin</td>
    <td>$100</td>
    
  </tr>
  <tr>
    <td>Lois</td>
    <td>Griffin</td>
    <td>$150</td>
    <td>Peter</td>
    <td>Griffin</td>
    <td>$100</td>
  </tr>
  <tr>
    <td>Joe</td>
    <td>Swanson</td>
    <td>$300</td>
    <td>Peter</td>
    <td>Griffin</td>
    <td>$100</td>
  </tr>
  <tr>
    <td>Cleveland</td>
    <td>Brown</td>
    <td>$250</td>
    <td>Peter</td>
    <td>Griffin</td>
    <td>$100</td>
</tr>
</table> -->


</body>
</html>