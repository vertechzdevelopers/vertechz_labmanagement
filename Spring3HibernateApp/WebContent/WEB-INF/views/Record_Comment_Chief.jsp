<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ include file="/WEB-INF/views/Header.jsp" %>
      <%@page import="java.util.*" %>
      <%@page import="java.text.*"%>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

  <title>Campsite Registration</title>
  
  
  
      

<head>
<!-- <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css'> -->
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/css/bootstrap-datetimepicker.min.css'>
<!-- <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'> -->
 <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
 <!-- <script src='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js'></script>  -->
<script src='http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js'></script>

    <script src="js3/index1.js"></script>

<!-- <script type="text/javascript">
	function save() {
		alert("Active Details added Successfully");
	}
</script> -->
<style>

.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
table {
    border: 1px solid black;
    border-collapse: collapse;
    width: 80%;
    margin: 0% 0% 1% 9%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #3399ff;
    color: white;
}

</style>
 

 </head>
 
 
<body>
<br>
<br>


  <div class='container'>
    <div class='panel panel-primary dialog-panel'>
      <div class='panel-heading'>
        <center><h5>Records Comment</h5></center>
      </div>
      <div class='panel-body'>
        <form:form action="SaveRecord_Comment.html" ModelAttribute="command">
          <div class='form-group'>
          <input class='form-control' name='rid' id='rid' value="${records.rid}" type='hidden'>
          <%
          Date date=new Date();
  		  Calendar calendar = Calendar.getInstance();
  		  date=calendar.getTime();
  		SimpleDateFormat s;
		//   s=new SimpleDateFormat("MM/dd/yyyy");
		
		s=new SimpleDateFormat("yyyy/MM/dd");
		String todaydate=s.format(date);
          %>
          <input  class="form-control"   name="date" id="date" value="<%= todaydate %>" type="hidden"> 

             <label class='control-label col-md-2 col-md-offset-1' for='concentration' style="margin-top: 50px;">Comment:</label>
            <div class='col-md-4'  style="margin: 1% 0% 0% 0%;">
             <textarea class='form-control' name='comment' id='comment' placeholder='Additional comments' rows='3'></textarea>
         </div>
          </div>
          
         
  
          <br>
        <br>
        <br>
           
        <br>
        <br>
        <br>
          <div class='form-group'>
            <div class='col-md-offset-2 col-md-4'>
              <button class='btn-lg btn-primary' type='submit' >SUBMIT</button>
              <a href="ChiefLabRecordSearch.html?rid=${records.rid}" class='btn-lg btn-primary'  type='submit'>Close & Return</a>
            </div>
            
          </div>
        </form:form>
      </div>
    </div>
  </div>
  </br>
  </br>
  <h2 align="center"> Record Comments</h2></br></br>
   <table>
					  <tr>
					    <th>Date</th>
					    <th>Comment</th>
					    <th>Delete</th>
					  </tr>
					   
					  <c:forEach items="${recordcomment}" var="recordcomment">
                        <tr>
					    <td><c:out value="${recordcomment.date}"/></td>
					    <td><c:out value="${recordcomment.comment}"/></td>
					    <td> <a href="DeleteRecord_Comment.html?comment_id=${recordcomment.comment_id}&rid=${records.rid}"><font color="Red"><img src="img/delete.png"></font></a></td>
					    </tr>
					  </c:forEach>
					    	
</table>


  
 


</body>
</html>