<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ include file="/WEB-INF/views/Header.jsp" %>
      <%@page import="java.util.*"%>
      <%@page import="java.text.*"%>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <title>Campsite Registration</title>  
<head>
<!-- <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css'> -->
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/css/bootstrap-datetimepicker.min.css'>
<link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'> 
 <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
 <!-- <script src='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js'></script>  -->
<script src='http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js'></script>

    <script src="js3/index1.js"></script>
    <script type="text/javascript">
         function Afterdate(){
        	 var date = document.myform.date.value;
        	 //alert ("date"+date);
        	 var currentdate = document.myform.CurrentDate.value;
        	 //alert ("currentdate"+currentdate);
        	 if(date > currentdate){
        		 alert ("Date Must be less than Current Date");
        		 window.location.href = "ChiefRecordRegistration.html";
        	 }
         }
    </script>
 </head>
<body> 
<br>
<br>

<c:if test="${!empty formulation}">
  <div class='container'>
    <div class='panel panel-primary dialog-panel'>
      <div class='panel-heading'>
        <center><h5>LAB RECORD REGISTRATION</h5></center>
      </div>
      <div class='panel-body'>
        <form:form action="SaveRecord.html" ModelAttribute="command1" name="myform">
          <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_accomodation'>DATE:</label>
            <div class='col-md-3'>
            <div class='input-group date' id='datetimepicker1'>
             <input class='form-control' name='date' id='date' placeholder='Date' type='text' required="required" >
             <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
 </span>
</div>
            <input class='form-control' name='active' id='active'  type='hidden' value="null">
            </div>
                       <%int count=1; %>
                       <c:forEach items="${count}" var="count">
                      
                        <%
                       
                        count++;
                        %>
					    </c:forEach>
            
            <%
           /*  Calendar calendar = Calendar.getInstance();
            System.out.println("At present Calendar's Year: " + calendar.get(Calendar.YEAR));  
            System.out.println("At present Calendar's Day: " + calendar.get(Calendar.MONTH));
            System.out.println("At present Calendar's Day: " + calendar.get(Calendar.DATE));
            String referanceNumber="VER/"+calendar.get(Calendar.YEAR)+"/"+calendar.get(Calendar.MONTH)+"/"+calendar.get(Calendar.DATE);
          */  
                System.out.println("count: "+count);
                //count++;

				Date date=new Date();
				Calendar calendar = Calendar.getInstance();
				date=calendar.getTime(); 
				SimpleDateFormat s;
				//   s=new SimpleDateFormat("MM/dd/yyyy");
				
				s=new SimpleDateFormat("yyyy/MM/dd");
				String ref_no="VER"+"/"+s.format(date)+"/"+count;
				System.out.println("VER"+s.format(date)+"/"+count);
				String CurrentDate = s.format(date);
				            
            %>
          </div>          
           <div class='form-group'>
           <input type="hidden"  name='CurrentDate' id='CurrentDate' value="<%= CurrentDate%>">
            <label class='control-label col-md-3 col-md-offset-3' for='id_accomodation' style="margin:4% 0% 0% -50% " >REFERENCE NO:</label>
            <div class='col-md-3' style="margin:4% 0% 0% -25% ">
             <input class='form-control'  type='text' readonly="readonly">
             <input type="hidden"  name='referance_no' id='referance_no' value="null">
            </div>
          </div>
          
           <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_equipment' style="margin:8% 0% 0% -50% ">EXECUTIVE RESEARCH</label>
            
              <div class='col-md-3' style="margin:8% 0% 0% -25% ">
                <div class='form-group internal'>
                  <select class='form-control' name='research_executive' id='research_executive'>
                 <%-- <c:forEach items="${executive_list}" var="executive_list">
                  <option><c:out value="${executive_list.userName}"/></option>
                  
                     </c:forEach> --%> 
                     <option><%=session.getAttribute("UserName")%></option> 
                  </select> 
                </div>
              </div>           
          </div>
          
           <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_equipment'  style="margin:12% 0% 0% -50% ">APPROVED BY:</label>           
              <div class='col-md-3' style="margin:12% 0% 0% -25% ">
                <div class='form-group internal'>
                <select class='form-control' name='approved_by' id='approved_by'>  
                  <c:forEach items="${chief_list}" var="chief_list">
                  <option><c:out value="${chief_list.userName}"/></option>
                  
                     </c:forEach>
                  </select> 
                </div>   
              
            </div>
          </div>
          <br>
          <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_email' style="margin:14% 0% 0% -50% ">OBJECTIVE:</label>
            <div class='col-md-6' style="margin:13% 0% 0% 50% ">              
                  <input class='form-control' name='objective' id='objective' placeholder='Objective' type='text' required="required" onclick="Afterdate()">              
            </div>
          </div>
          
           <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='reminder_date' style="margin:17% 0% 0% -75% ">REMINDER DATE:</label>
            <div class='col-md-6' style="margin:1% 0% 0% 50% ">
            <div class='input-group date' id='datetimepicker1'>              
                  <input class='form-control' name='reminder_date' id='reminder_date' placeholder='reminder_date' type='text'>              
                   <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
 </span>
</div>
              </div>
          </div>
          
          <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_equipment' style="margin:6% 0% 0% -75% ">FORMULATION TYPE</label>            
              <div class='col-md-3' style="margin:1% 0% 0% 50% ">
                <div class='form-group internal'>
                  <select class='form-control' name='formulation' id='formulation'>                  
                  <c:forEach items="${formulation}" var="formulation">
                    <option><c:out value="${formulation.formulationtype}"/></option>
                    <!-- <option>Fifth wheel</option>
                    <option>RV/Motorhome</option>
                    <option>Tent trailer</option>
                    <option>Pickup camper</option>
                    <option>Camper van</option> -->
                     </c:forEach>
                  </select>                
              </div>              
            </div>
          </div>
         
         
          <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_comments' style="margin:6% 0% 0% -50% ">PROCESS DETAILS:</label>
            <div class='col-md-6' style="margin:0% 0% 0% 50% ">
              <textarea class='form-control' name='process_details' id='process_details'  ></textarea>
            </div>    
         </div>
          <BR>
          <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_comments' style="margin: 9% 0% 0% -75%">OBSERVATION/REMARK:</label>
            <div class='col-md-6' style="margin:1% 0% 0% 50%" >
              <textarea class='form-control' name='Observations' id='Observations' ></textarea>
            </div>   
          </div>
         
           <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_equipment' style="margin:11% 0% 0% -75% ">STATUS</label>            
              <div class='col-md-3' style="margin:1% 0% 0% 50% ">
                <div class='form-group internal'>                 
                  <select class='form-control' name='status' id='status'>
                 <option>In Process</option>
                    <option>Finalise</option>
                    <option>Rejected</option>
                  </select>                   
                </div>
              </div>
           </div>
        
          <div class='form-group'>
            <div class='col-md-offset-4 col-md-3'style="margin: 3% 0 0% 38%;">
              <button class='btn-lg btn-primary' type='submit' >SUBMIT</button>
            </div>
            
        
        </form:form>
        <form:form action="Back.html" modelAttribute="command1"  method="get">
       <div class='col-md-3' >
              <button class='btn-lg btn-danger'   style="margin: 10% 0% 0% -33%;"  >Cancel</button>
            </div>
         </form:form>
           </div>
      </div>
    </div>
  </div>

  </c:if>
 


</body>
</html>