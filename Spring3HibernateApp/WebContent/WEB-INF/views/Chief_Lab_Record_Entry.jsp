<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ include file="/WEB-INF/views/Header.jsp" %>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css'> -->
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/css/bootstrap-datetimepicker.min.css'>
<link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'> 
 <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
 <!-- <script src='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js'></script>  -->
<script src='http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js'></script>

    <script src="js3/index1.js"></script>
  <title>Campsite Registration</title>
  </head>
<style>
.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 5px 5px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
    font-size: 13px;
}
table {
    border: 1px solid black;
    border-collapse: collapse;
    width: 100%;
}
th, td {
    text-align: left;
    padding: 8px;
}
tr:nth-child(even){background-color: #f2f2f2}
th {
    background-color: #3399ff;
    color: white;
}
</style>
 </head> 
<body>
<br>
<br>
<a href="ATS_SelfLife_Reminders.html?rid=${records.rid}" class="button" >ATS/SelfLife Reminders</a>
<a href="Active_Details.html?rid=${records.rid}" class="button" >Active Details</a>
<a href="Composition_Details.html?rid=${records.rid}" class="button" >Composition Details</a>
<a href="Analytical_Studies.html?rid=${records.rid}" class="button" >Analytical Studies</a>
<a href="Scanned_Copies.html?rid=${records.rid}" class="button" >Scanned Copies</a>
<a href="Cold_Test.html?rid=${records.rid}" class="button" >Cold Test</a>
<a href="SelfLife_Studies.html?rid=${records.rid}" class="button" >SelfLife Studies</a>
<a href="ATS_Studies.html?rid=${records.rid}" class="button" >ATS Studies</a>
<a href="Image_Upload.html?rid=${records.rid}" class="button" >Upload Videos/Photos</a>
<br>
<br>
  <div class='container'>
    <div class='panel panel-primary dialog-panel'>
      <div class='panel-heading'>
        <center><h5>LAB RECORD REGISTRATION</h5></center>
      </div>
      <div class='panel-body'>
        <form:form action="SaveRecord.html" ModelAttribute="command1">
        <input class='form-control' name='rid' id='rid' value="${records.rid}" type='hidden'>
          <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_accomodation'>DATE : </label>
            <div class='col-md-3'>
            <div class='input-group date' id='datetimepicker1'>
             <input class='form-control' name='date' id='date' placeholder='Date' value="${records.date}" type='text'>
             <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
 </span>
</div>
            </div>
          </div>
          
           <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_accomodation' style="margin:4% 0% 0% -50% ">REFERENCE NO : </label>
            <div class='col-md-3' style="margin:4% 0% 0% -25% ">
             <input class='form-control' name='referance_no' id='referance_no' value="${records.referance_no}"  placeholder='Reference No' type='text'>
            </div>
          </div>
  
          
           <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_equipment' style="margin:8% 0% 0% -50% ">EXECUTIVE RESEARCH : </label>
            
              <div class='col-md-3'  style="margin:8% 0% 0% -25% ">
                <div class='form-group internal'>
                  <select class='form-control' name='research_executive' id='research_executive'>
                   <option><c:out value="${records.research_executive}"/></option>
                 <%--  <c:forEach items="${executive_list}" var="executive_list">
                    <option><c:out value="${executive_list.fname}"/><c:out value="${executive_list.lname}"/></option>
                  
                     </c:forEach> --%>
                    <%--  <option><%=session.getAttribute("UserName")%></option>  --%>
                  </select>
                  
               
                </div>
              </div>             
          
          </div>
          
            <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_equipment' style="margin:12% 0% 0% -50% ">APPROVED BY : </label>
            
              <div class='col-md-3' style="margin:12% 0% 0% -25% " >
                <div class='form-group internal'>
                  <select class='form-control' name='approved_by' id='approved_by'>
                  <option><c:out value="${records.approved_by}"/></option>
                    <c:forEach items="${chief_list}" var="chief_list">
                   <option><c:out value="${chief_list.userName}"/></option>
                  
                     </c:forEach>
                  </select>
                </div>
              </div>           
            
          </div>
          <br>
        
          <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_email' style="margin:14% 0% 0% -50% ">OBJECTIVE : </label>
            
              
                <div class='col-md-6' style="margin:13% 0% 0% 50% ">
                  <input class='form-control' name='objective' id='objective' placeholder='Objective' value="${records.objective}" type='text'>
                </div>
             
              
           
          </div>
            
           <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='reminder_date' style="margin:17% 0% 0% -75% ">REMINDER DATE : </label>
            <div class='col-md-6' style="margin:1% 0% 0% 50% ">
                  <div class='input-group date' id='datetimepicker1'>
                  <input class='form-control' name='reminder_date' id='reminder_date' value="${records.reminder_date}" placeholder='reminder_date' type='text'>
                  <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
 </span>
</div>
                </div>
             
          </div>
          
          <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_equipment' style="margin:6% 0% 0% -75% ">FORMULATION TYPE : </label>
           
              <div class='col-md-3'  style="margin:1% 0% 0% 50% ">
                <div class='form-group internal'>
                  <select class='form-control' name='formulation' id='formulation'>
                  <option><c:out value="${records.formulation}"/></option>                  
                  <c:forEach items="${formulation}" var="formulation">
                    <option><c:out value="${formulation.formulationtype}"/></option>
                    <!-- <option>Fifth wheel</option>
                    <option>RV/Motorhome</option>
                    <option>Tent trailer</option>
                    <option>Pickup camper</option>
                    <option>Camper van</option> -->
                     </c:forEach>
                  </select>
                </div>
              </div>
              
          
          </div>
         
        
          <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_comments' style="margin:6% 0% 0% -50% ">PROCESS DETAILS : </label>
            <div class='col-md-6' style="margin:0% 0% 0% 50% ">
              <textarea class='form-control' name='process_details' id='process_details' ><c:out value="${records.process_details}"/></textarea>
            </div>
            
           
          </div>
          <BR> 
          <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_comments' style="margin: 9% 0% 0% -75%">OBSERVATION/REMARK : </label>
            <div class='col-md-6' style="margin:1% 0% 0% 50% ">
              <textarea class='form-control' name='Observations' id='Observations' ><c:out value="${records.observations}"/></textarea>
            </div>
            
           
          </div>
         
           <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_equipment' style="margin:11% 0% 0% -75% ">STATUS : </label>
 
              <div class='col-md-3' style="margin:1% 0% 0% 50% ">
                <div class='form-group internal'>
                 
                  <select class='form-control' name='status' id='status'>
                  <option><c:out value="${records.status}"/></option>
                   <option>In Process</option>
                    <option>Finalise</option>
                    <option>Rejected</option>
                  </select>                  
                </div>
              </div>              
            
          </div>
        
          <div class='form-group'>
            <div class='col-md-offset-4 col-md-3' style="margin: 3% 0 0% 38%;">
              <button class='btn-lg btn-primary'  type='submit'>SUBMIT</button>
            </div>
           
        </form:form>
          <form:form action="Back.html" ModelAttribute="command1" method="Get">
        <div class='col-md-3' style="margin: 3% 0% 0% -20%;margin-left: -100px;">
              <button class='btn-lg btn-danger' style='float:left' type='submit'>Cancel</button>
            </div>
            </form:form>
           </div>
      </div>
    </div>
  </div>

 
 


</body>
</html>