<%@page import="org.springframework.ui.Model"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@page import="java.util.List"%>
           <%@ include file="/WEB-INF/views/Header_Admin.jsp" %>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
.pagination {
    background-color: #3399ff;
    border: solid;
    color: white;
    padding: 3px 5px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 12px;
    margin: 4px 2px;
    cursor: pointer;
}
.spanValue{
	background-color: #F01212;
	 border: solid;
    color: white;
    padding: 3px 5px;
	text-align: center;
	text-decoration: none;
	font-size: 12px;
}
table 
{
    border: 1px solid black;
    border-collapse: collapse;
    width: 100%;
    margin : 0% 0% 1% 0%;
}
th, td {
    text-align: left;
    padding: 8px;
}
tr:nth-child(even){background-color: #f2f2f2}
th {
    background-color: #634aa84d;
    color: white;
}
</style>
</head>
<body>
<br>
<br>


<c:if test="${!empty chief_list}">
<table>
					  <tr>
					    
					    <th>FName</th>
					    <th>LName</th>
					    <th>Address</th>
					    <th>Contact</th>
					    <th>Country</th> 
					    <th>Designation</th>
					    <th>E-mailId</th>
					    <th>UserName</th>
					    <th>Password</th>
					    
					    <th>Edit</th>
					    <th>Delete</th>
					  </tr>
					   
					  <c:forEach items="${chief_list}" var="clist">
                        <tr>
                        <%  
                        
                       %>
					   
					    <td><c:out value="${clist.fname}"/></td>
					    <td><c:out value="${clist.lname}"/></td>
					   <td><c:out value="${clist.address}"/></td>
					     <td><c:out value="${clist.contactNo}"/></td>
					  <td><c:out value="${clist.country}"/></td>
					    <td><c:out value="${clist.designation}"/></td>
					      <td><c:out value="${clist.emailId}"/></td>
					    <td><c:out value="${clist.userName}"/></td>
					    <td><c:out value="${clist.password}"/></td>
					   
					    <td> <a href="EditChiefManager.html?id=${clist.id}"><font color="Blue">Edit</font></a></td>
					    <td> <a href="DeleteAdmin.html?id=${clist.id}"><font color="Red">Delete </font></a></td>
					    </tr>
					    </c:forEach>			  
 </table>
</c:if>

</body>
</html>