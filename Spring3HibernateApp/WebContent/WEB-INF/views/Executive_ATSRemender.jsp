<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ page import="java.util.*" %>
      <%@ page import="java.text.SimpleDateFormat" %>
      <%@ page import="java.text.DateFormat" %>
     <%@ include file="/WEB-INF/views/Header_Exe.jsp" %>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>

.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 0% 0% 1% 9%;
    cursor: pointer;
}
.pagination {
    background-color: #3399ff;
    border: solid;
    color: white;
    padding: 3px 5px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 12px;
    margin: 4px 2px;
    cursor: pointer;
}
.spanValue{
	background-color: #F01212;
	 border: solid;
    color: white;
    padding: 3px 5px;
	text-align: center;
	text-decoration: none;
	font-size: 12px;
}
table 
{
    border: 1px solid black;
    border-collapse: collapse;
    width: 80%;
    margin: 0% 0% 1% 9%;
}
th, td {
    text-align: left;
    padding: 8px;
}
tr:nth-child(even){background-color: #f2f2f2}
th {
    background-color: #3443b1cc;
    color: white;
}
</style>
  
     

  
</head>
<body>
 <%



 int count = 0;

count++;


Date date=new Date();
Calendar calendar = Calendar.getInstance();
date=calendar.getTime(); 
SimpleDateFormat s;
//   s=new SimpleDateFormat("MM/dd/yyyy");

s=new SimpleDateFormat("yyyy/MM/dd");
String ref_no=s.format(date);

System.out.println("Date is"+ref_no);
System.out.println(s.format(date));

  
calendar.add( Calendar.DATE, 1 );    
String convertedDate=s.format(calendar.getTime());    
System.out.println("Date increase by one.."+convertedDate);







%>
<c:set var="ref_no" value="<%=ref_no %>" scope="request"/>
<%-- <c:out value="${ref_no}" /> --%>

<c:set var="convertedDate" value="<%=convertedDate %>" scope="request"/>
<%-- <c:out value="${convertedDate}" /> --%>

<br>
<br>
<center><h2>ATS Remainder</h2></center>

 
	<table style="border-bottom-width: 1px;margin-bottom: 4%;margin-top: 2%;">
		<tr>
			<!-- <th>Formulation ID</th> -->
			<th>Date.</th>
			<th>Reference No.</th>
			<th>Objective.</th>
			<th>Formulation Type.</th>
			<th>Status.</th>
			<th>Remainder Date .</th>
		</tr>
		<c:forEach items="${remainder}" var="remainder">
		
		<c:if test="${ref_no == remainder.reminder_date }">
			<tr class="danger">
			
				<td><font color="Red"><c:out value="${remainder.date}"/></td>
				<td><font color="Red"><c:out value="${remainder.referance_no}"/></td>
				<td><font color="Red"><c:out value="${remainder.objective}"/></td>
				<td><font color="Red"><c:out value="${remainder.formulation}"/></td>
				<td><font color="Red"><c:out value="${remainder.status}"/></td>
				<td><font color="Red"><c:out value="${remainder.reminder_date}"/></td>	
				<%-- <td> <a href="editFormulation.html?id=${formulation.id}"><font color="Blue">Edit</font></a></td>
				<td> <a href="deleteFormulation.html?id=${formulation.id}"><font color="Red">Delete </font></a></td> --%>		
			</tr>
		
		
	<%-- <%} %>	 --%>
		  </c:if> 
		  <c:if test="${convertedDate == remainder.reminder_date }">
               
  
		  <tr class="danger">
			
				<td><font color="Blue"><c:out value="${remainder.date}"/></td>
				<td><font color="Blue"><c:out value="${remainder.referance_no}"/></td>
				<td><font color="Blue"><c:out value="${remainder.objective}"/></td>
				<td><font color="Blue"><c:out value="${remainder.formulation}"/></td>
				<td><font color="Blue"><c:out value="${remainder.status}"/></td>
				<td><font color="Blue"><c:out value="${remainder.reminder_date}"/></td>	
				<%-- <td> <a href="editFormulation.html?id=${formulation.id}"><font color="Blue">Edit</font></a></td>
				<td> <a href="deleteFormulation.html?id=${formulation.id}"><font color="Red">Delete </font></a></td> --%>		
			</tr>
			</c:if>
		  <c:if test="${ref_no!= remainder.reminder_date && convertedDate!=remainder.reminder_date}">
               
  
		  <tr class="danger">
			
				<td><font color="Green"><c:out value="${remainder.date}"/></td>
				<td><font color="Green"><c:out value="${remainder.referance_no}"/></td>
				<td><font color="Green"><c:out value="${remainder.objective}"/></td>
				<td><font color="Green"><c:out value="${remainder.formulation}"/></td>
				<td><font color="Green"><c:out value="${remainder.status}"/></td>
				<td><font color="Green"><c:out value="${remainder.reminder_date}"/></td>	
				<%-- <td> <a href="editFormulation.html?id=${formulation.id}"><font color="Blue">Edit</font></a></td>
				<td> <a href="deleteFormulation.html?id=${formulation.id}"><font color="Red">Delete </font></a></td> --%>		
			</tr>
			</c:if>
			
			
		</c:forEach> 
	</table>
 



           
</body>
</html>