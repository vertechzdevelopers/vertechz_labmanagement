<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ include file="/WEB-INF/views/Header.jsp" %>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

  <title>Campsite Registration</title>
  
  
  
      

<head>

<!-- <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css'> -->
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/css/bootstrap-datetimepicker.min.css'>
<!-- <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'> -->
 <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
 <!-- <script src='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js'></script>  -->
<script src='http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js'></script>

    <script src="js3/index1.js"></script>
<!-- <script type="text/javascript">
 function update(){
	 alert("Active Details Updated Successfully...")
 }

</script> -->

<style>

.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
table {
    border: 1px solid black;
    border-collapse: collapse;
    width: 80%;
    margin: 0% 0% 1% 9%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #3399ff;
    color: white;
}

</style>
 

 </head>
 
 
<body>
<br>
<br>



  <div class='container'>
    <div class='panel panel-primary dialog-panel'>
      <div class='panel-heading'>
        <center><h5>Cold Test</h5></center>
      </div>
      <div class='panel-body'>
        <form:form action="SaveCold_Test.html" ModelAttribute="command">
          <div class='form-group'>
          <input class='form-control' name='rid' id='rid' value="${coldtest.rid}" type='hidden'>
          <input class='form-control' name='cold_id' id='cold_id' value="${coldtest.cold_id}" type='hidden'>
            <label class='control-label col-md-2 col-md-offset-1' for='id_accomodation'>Degree:</label>
             
           
              <div class='col-md-2'>
                <div class='form-group internal'>
                  <select class='form-control' name='degree' id='degree'>
                  
                  <option><c:out value="${coldtest.degree}"/></option>
                    <option>0 Degree</option>
                    <option>5 Degree</option>
                    <option>10 Degree</option>
                    <option>15 Degree</option>
                   
                  
                  </select>
            
              </div>
              
            </div>


	            
            <label class='control-label col-md-2 col-md-offset-1' for='concentration'>Test Date*</label>
            <div class='col-md-2'>
            <div class='input-group date' id='datetimepicker1'>
            <input type="text" class="form-control input-sm datepicker" name="testdate" id="testdate" value="${coldtest.testdate}">
            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
				 </span>
				</div>
            </div>
            
            <br><br>
             <label class='control-label col-md-2 col-md-offset-1' for='concentration'>Observation:</label>
            <div class='col-md-2' style="margin: 1% 0% 0% 0%;">
             <textarea class='form-control' name='observation' id='observation' placeholder='Additional comments' rows='3'><c:out value="${coldtest.observation}"/></textarea>
         </div>
          </div>
          
         
  
          
           
        </br></br></br>
          <div class='form-group'>
            <div class='col-md-offset-2 col-md-4'>
              <button class='btn-lg btn-primary' type='submit' >SUBMIT</button>
                </div>
           
          </div>
        </form:form>
      </div>
    </div>
  </div>
  </br>
  </br>
  <%-- <h2 align="center"> Cold Test</h2></br></br>
   <table>
					  <tr>
					    <th>#</th>
					    <th>Degree</th>
					    <th>Test Date</th>
					    <th>Observation</th>
					    <th>Edit</th>
					    <th>delete</th>
					  </tr>
					   
					  <c:forEach items="${coldtest1}" var="coldtest1">
                        <tr>
					    <td><c:out value="${coldtest1.cold_id}"/></td>
					    <td><c:out value="${coldtest1.degree}"/></td>
					    <td><c:out value="${coldtest1.testdate}"/></td>
					    <td><c:out value="${coldtest1.observation}"/></td>
					    <td> <a href="EditCold_Test.html?cold_id=${coldtest1.cold_id}"><font color="Blue">Edit</font></a></td>
					    <td> <a href="DeleteCold_Test.html?cold_id=${coldtest1.cold_id}"><font color="Red">Delete </font></a></td>
					    </tr>
					  </c:forEach>
					    	
</table>

 --%>


  
 


</body>
</html>