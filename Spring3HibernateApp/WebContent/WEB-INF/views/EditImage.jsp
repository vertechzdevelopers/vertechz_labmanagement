<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>  <%@ include file="/WEB-INF/views/Header.jsp" %>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<!-- <script type="text/javascript">
	function save() {
		alert("Image / Video updated Successfully");
	}
</script> -->
<style>
.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
table 
{
    border: 1px solid black;
    border-collapse: collapse;
    width: 100%;
}
th, td {
    text-align: left;
    padding: 8px;
}
tr:nth-child(even){background-color: #f2f2f2}
th {
    background-color: #3399ff;
    color: white;
}
</style>
<body>
  <div class='container'>
    <div class='panel panel-primary dialog-panel' style="margin: 10% 0% 0% 0%;">
      <div class='panel-heading'>
        <center><h2>Upload Videos / Photos :</h2></center>
      </div>
      <div class='panel-body'>
     <form:form method="POST" action="saveImage.html" modelAttribute="command" enctype="multipart/form-data"> 
      <input class='form-control' name='rid' id='rid' value="${image.rid}" type='hidden'>
      <input class='form-control' name='id' id='id' value="${image.id }" type='hidden'>  
      		<div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_accomodation'>Name :</label>
            <div class='col-md-3' style="margin: 0% 0% 0% -9%;"> 
             <input class='form-control' name='name' id='name' placeholder='Name of photo/video' value="${image.name}"  type='text'>
            </div>
          </div>   
          <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_comments' style="margin: 4% 0% 0% -41%">Description:</label>
            <div class='col-md-6' style="margin:1% 0% 0% 41%" >
              <textarea class='form-control' name='description' id='description' placeholder='description of photo/video' rows='3'>${image.description}</textarea>
            </div>   
          </div>
           <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_accomodation' style="margin: 1% 0% 0% 25%;">Video / Photo : </label>
            <div class='col-md-3' style="margin: 1% 0% 0% -9%;">
             <input name="file" type='file'>
            </div>
          </div>  
          
           <!-- <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_equipment' style="margin: 2% 0% 0% 41%;" >Status : </label>
           <div class='col-md-2' style="margin: 2% 0% 0% -8%;">
                  <select class='form-control' id='id_equipment' name="status">
                    <option value="Enabled">Enabled</option>
                    <option value="Disabled">Disabled</option>
                  </select>
              </div>
          </div> -->
          
          <br>          
          <div class='form-group'>
            <div class='col-md-offset-4 col-md-3'>
              <button class='btn-lg btn-primary' type='submit' style="margin: 10% 0% 0% 23%;" >SUBMIT</button>
            </div>
            <div class='col-md-3'>
              <button class='btn-lg btn-danger'  type='submit' style="margin: 10% 0% 0% -33%;" >Cancel</button>
            </div>
          </div>
       </form:form>
      </div>
    </div>
  </div>
</html>