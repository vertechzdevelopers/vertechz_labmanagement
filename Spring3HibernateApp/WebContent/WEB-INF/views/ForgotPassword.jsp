<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ include file="/WEB-INF/views/Forgot_Header.jsp" %>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

  <title>Camposite Registration</title>
  
  
  <head>
  
  <%-- <% 
  System.out.println("In Forgot  password.Jsp");
  String UserName=null;
  HttpSession session1=request.getSession();
  if(session1!=null)
  {  
   UserName=session1.getAttribute("UserName").toString();
  System.out.println(UserName);
  }
  %> --%>


 

 </head>
 
 
<body>
<br>
<br>
<%-- <c:if test="${empty saveManager}"> --%>
<form:form method="POST" name="myform" action="forgotPassword.html" modelAttribute="command" >
  <div class='container'>
    <div class='panel panel-primary dialog-panel'>
      <div class='panel-heading'>
        <center><h5>Forgot Password</h5></center>
      </div>
      <div class='panel-body'>
        <form class='form-horizontal' role='form'>
          <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_accomodation'>UserName</label>
            <div class='col-md-2'>
             <input class='form-control' id='userName' name='userName'  type='text'   <%-- value=<%=UserName %> --%> >
            </div>
          </div>
          <br>
          
           <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_accomodation'>EmailId :</label>
            <div class='col-md-2'>
             <input class='form-control' id='emailId' name='emailId' placeholder=' Enter emailId' type='text' required="required" type='text' />
          </div>
      
           
        <br>
          <div class='form-group'>
            <div class='col-md-offset-4 col-md-3'>
              <button class='btn-lg btn-primary' type='submit'>SUBMIT</button>
            </div>
            <div class='col-md-3'>
              <button class='btn-lg btn-danger' style='float:right' type='submit'>Cancel</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

 
 </form:form>


</body>
</html>