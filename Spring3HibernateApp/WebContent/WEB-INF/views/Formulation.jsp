<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ include file="/WEB-INF/views/Header.jsp" %>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- <script type="text/javascript">
	function save() {
		alert("Formulation Type added Successfully");
	}
</script> -->
<body>
  <div class='container'>
    <div class='panel panel-primary dialog-panel' style="margin: 10% 0% 0% 0%;">
      <div class='panel-heading'>
        <center><h2>Formulation Types</h2></center>
      </div>
      <div class='panel-body'>
     <form:form method="POST" action="saveformulation.html">
         
          
           <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_accomodation' style="margin: 0% 0% 0% 33%;">Formulation Type : </label>
            <div class='col-md-2' style="margin: 0% 0% 0% 0%;">
             <input class='form-control' id='id_email' name="formulationtype" type='text' required="required" >
            </div>
          </div>
         
          
           <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_equipment' style="margin: 2% 0% 0% 41%;" >Status : </label>
           <div class='col-md-2' style="margin: 2% 0% 0% -8%;">
                  <select class='form-control' id='id_equipment' name="status">
                    <option value="Enabled">Enabled</option>
                    <option value="Disabled">Disabled</option>
                  </select>
              </div>
          </div>
          
          <br>
          
          <div class='form-group'>
            <div class='col-md-offset-4 col-md-3'>
              <button class='btn-lg btn-primary' type='submit' style="margin: 10% 0% 0% 23%;" >SUBMIT</button>
            </div>
            
       </form:form>
       <form:form action="formulationList.html" method="get">
       <div class='col-md-3'>
              <button class='btn-lg btn-danger'   style="margin: 10% 0% 0% -33%;"  >Cancel</button>
            </div>
         </form:form>
          </div>
      </div>
    </div>
  </div>

    


</body>
</html>