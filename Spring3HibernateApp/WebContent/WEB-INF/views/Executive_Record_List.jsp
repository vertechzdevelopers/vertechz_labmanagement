<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@page import="java.util.List"%>
           <%@ include file="/WEB-INF/views/Header_Exe.jsp" %>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
.pagination {
    background-color: #3399ff;
    border: solid;
    color: white;
    padding: 3px 5px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 12px;
    margin: 4px 2px;
    cursor: pointer;
}
.spanValue{
	background-color: #F01212;
	 border: solid;
    color: white;
    padding: 3px 5px;
	text-align: center;
	text-decoration: none;
	font-size: 12px;
}
table 
{
    border: 1px solid black;
    border-collapse: collapse;
    width: 100%;
    margin : 0% 0% 1% 0%;
}
th, td {
    text-align: left;
    padding: 8px;
}
tr:nth-child(even){background-color: #f2f2f2}
th {
    background-color: #634aa84d;
    color: white;
}
</style>
</head>
<body>
<br>
<br>
<%
List<Integer>  pageList = (List<Integer>)session.getAttribute("pageList");
String offSet = request.getParameter("offSet");
 /* disabledLINK has been used to to make current page number nonhiperlink i.e unclickable
 e.g if user is at page number 15 then page number 15 should not be clickable*/
int disabledLINK = 0;
if(offSet!=null){
	disabledLINK = Integer.parseInt(offSet);
}
/* size is used for moving user to end page  by clicking on END link*/
int   size = Integer.parseInt(session.getAttribute("size").toString());

%>

<a href="ExcutiveRecordRegistration.html" class="button">ADD</a>
<!-- <a href="#" class="button">EDIT</a> -->

<c:if test="${!empty recordsList}">
<table>
					  <tr>
					    
					    <th>DATE</th>
					    <th>REFERENCE NO</th>
					    <th>OBJECTIVE</th>
					    <th>FORMULATION TYPE</th>
					    <th>Research Executive</th>
					    <th>STATUS</th>
					    <th>Edit</th>
					   <!--  <th>Delete</th> -->
					  </tr>
					   
					  <c:forEach items="${recordsList}" var="records">
                        <tr>
					   
					    <td><c:out value="${records.date}"/></td>
					    <td><c:out value="${records.referance_no}"/></td>
					    <td><c:out value="${records.objective}"/></td>
					    <td><c:out value="${records.formulation}"/></td>
					    <td><c:out value="${records.research_executive}"/></td>
					    <td><c:out value="${records.status}"/></td>
					    <td> <a href="Execuitve_EditRecords.html?rid=${records.rid}"><font color="Blue"><img src="img/edit.png"></font></a></td>
					   <%--  <td> <a href="Executive_DeleteRecords.html?rid=${records.rid}"><font color="Red"><img src="img/delete.png"></font></a></td>
					  --%>   </tr>
					    </c:forEach>			  
 </table>
</c:if>
<%if(disabledLINK != 0){ %>
<!-- if user is on start page then it should not be visible to user or it should not be hyper-link-->
<a href="formulationList.html?offSet=<%=0%>" class="pagination">Start</a>
<%}else{ %>
<span class="spanValue">Start</span>
<%} %>
<%for(Integer i:pageList) {
if(disabledLINK == i ){
	if(disabledLINK!=size){
%>
<!-- Current page should not be hyper-link-->
<span class="spanValue"><%=i %></span>
<%-- <%=i %> --%>
<%}}else{ %>
<!-- page previous to current page and next to current page has to be hyper link  -->
<a href="formulationList.html?offSet=<%=i%>" class="pagination"><%=i+"" %></a>
<%}} %>
<%if(disabledLINK == size){ %>
<span class="spanValue">End</span>
<%}else{ %>
<!-- if user is on last page then it should not be visible to user or it should not be hyper-link-->
<%-- <a href="formulationList.html?offSet=<%=size%>" class="pagination">End</a> --%>
<%} %>
</body>
</html>