<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ include file="/WEB-INF/views/Header_Exe.jsp" %>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

  <title>Campsite Registration</title>
  
  
  
      

</head>
<!-- <script type="text/javascript">
 function update(){
	 alert("Active Details Updated Successfully...")
 }

</script> -->

<style>

.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
table {
    border: 1px solid black;
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #3399ff;
    color: white;
}

</style>
 

 </head>
 
 
<body>
<br>
<br>


  <div class='container'>
    <div class='panel panel-primary dialog-panel'>
      <div class='panel-heading'>
        <center><h5>ACTIVE DETAILS</h5></center>
      </div>
      <div class='panel-body'>
        <form:form action="SaveActive_executive.html" ModelAttribute="command">
          <div class='form-group'>
          <input class='form-control' name='active_id' id='active_id' value="${active.active_id}" type='hidden'>
          <input class='form-control' name='rid' id='rid' value="${records.rid}" type='hidden'>
          <input class='form-control' name='date' id='date' value="${records.date}" type='hidden'>
          <input class='form-control' name='referance_no' id='referance_no' value="${records.referance_no}" type='hidden'>
          <input class='form-control' name='objective' id='objective' value="${records.objective}" type='hidden'>
          <input class='form-control' name='formulation' id='formulation' value="${records.formulation}" type='hidden'>
          <input class='form-control' name='research_executive' id='research_executive' value="${records.research_executive}" type='hidden'>
          <input class='form-control' name='status' id='status' value="${records.status}" type='hidden'>
          <input class='form-control' name='observations' id='observations' value="${records.observations}" type='hidden'>
          <input class='form-control' name='approved_by' id='approved_by' value="${records.approved_by}" type='hidden'>
          <input class='form-control' name='process_details' id='process_details' value="${records.process_details}" type='hidden'>
          <input class='form-control' name='reminder_date' id='reminder_date' value="${records.reminder_date}" type='hidden'>
          <input class='form-control' name='records_active' id='records_active' value="${records.records_active}" type='hidden'>  
          <c:forEach items="${ active1}" var="active1">
          <input class='form-control' name='records_active' id='records_active' value="${active1.active}" type='hidden'>
          </c:forEach>
            <label class='control-label col-md-2 col-md-offset-1' for='id_accomodation'>Active:</label>
            <div class='col-md-2'>
             <input class='form-control' name='active' id='active' value="${active.active }" type='text'>
            </div>
            <br><br>
             <label class='control-label col-md-2 col-md-offset-1' for='concentration'>Concentration:</label>
            <div class='col-md-2'>
             <input class='form-control' name='concentration' id='concentration' value="${active.concentration}" type='text'>
            </div>
          </div>
          
         
  
          
           
        </br></br></br>
          <div class='form-group'>
            <div class='col-md-offset-2 col-md-4'>
              <button class='btn-lg btn-primary' type='submit' >SUBMIT</button>
            </div>
           
          </div>
        </form:form>
      </div>
    </div>
  </div>
  </br>
  </br>
 <%--  <h2 align="center"> Active Details</h2></br></br>
   <table>
					  <tr>
					    <th>#</th>
					    <th>Active</th>
					    <th>Concentration</th>
					    <th>Edit</th>
					    <th>delete</th>
					  </tr>
					   
					  <c:forEach items="${active1}" var="active1">
                        <tr>
					    <td><c:out value="${active1.active_id}"/></td>
					    <td><c:out value="${active1.active}"/></td>
					    <td><c:out value="${active1.concentration}"/></td>
					    <td> <a href="EditActive.html?active_id=${active.active_id}"><font color="Blue">Edit</font></a></td>
					    <td> <a href="DeleteActive.html?active_id=${active.active_id}"><font color="Red">Delete </font></a></td>
					    </tr>
					  </c:forEach>
					    	
</table>


   --%>
 


</body>
</html>
