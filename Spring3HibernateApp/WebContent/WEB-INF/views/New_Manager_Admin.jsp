<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ include file="/WEB-INF/views/Forgot_Header.jsp" %>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

  <title>Camposite Registration</title>
  
  
  

<script type="text/javascript">
        function validate_form() 
        
        {
           
 
 
 
            if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/).test(document.myForm.emailId.value)) {
                alert("You have entered an invalid email address!")
                return (false)
            }
            if(!(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/).test(document.myForm.contactNo.value)&& (document.myForm.contactNo.length==10))
            	{
            	alert("You have entered an invalid contact number")
                return (false)
            	}
        }
 
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31 &&
                (charCode < 48 || charCode > 57)) {
                alert("Enter Number");
                return false;
            }
            return true;
        }
 
 
        
        function create_username() {
            var txtFirstValue = document.myForm.fname.value;
            var txtSecondValue =  document.myForm.lname.value;
            var result = txtFirstValue.concat(".").concat(txtSecondValue);
            document.myForm.userName.value = result;
           
        }
        //-->
    </script>
 
      

</head>



 

 </head>
 
 
<body ng-app="myApp"  ng-controller="validateCtrl">
<br>
<br>
<%-- <c:if test="${empty saveManager}"> --%>
<c:if test="${not empty msg}">
			<div class="msg">${msg}</div>
		</c:if>
<form:form method="POST" action="saveManagerAdmin.html" onsubmit="return validate_form();" name="myForm" modelAttribute="command" >
  <div class='container'>
    <div class='panel panel-primary dialog-panel'>
      <div class='panel-heading'>
        <center><h5>NEW MANAGER REGISTRATION</h5></center>
      </div>
      <div class='panel-body'>
        <form class='form-horizontal' role='form'>
          <div class='form-group'>
          
            <label class='control-label col-md-2 col-md-offset-2' for='id_accomodation'>First Name:</label>
            <div class='col-md-2'>
             <input class='form-control' id='fname' name='fname' value="${chieflogin.fname}"placeholder='Enter first name' onkeyup='create_username()' required="required" type='text' data-ng-model="fname" ng-minlength="3" ng-maxlength="20" ng-pattern="/^[A-Za-z]*$/">
          
             </div>
            </div>
          <br>
          
           <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_accomodation'>Last Name:</label>
            <div class='col-md-2'>
             <input class='form-control' id='lname' name='lname' value="${chieflogin.lname}" placeholder='Enter Last Name' type='text' onkeyup='create_username()' required="required" ng-model="lname">
             
						  
            </div>
          </div>
          
         <br>
          
            <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_accomodation'>Email ID:</label>
            <div class='col-md-2'>
             <input class='form-control' id='emailId' name='emailId' value="${chieflogin.emailId}" placeholder='Enter emailId' type='text' required="required"ng-pattern="/^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/"  ng-model="emailId" is-unique is-unique-api="../api/v1/checkUsers">
            
            </div>
            
          
          </div>
              <br>
           
           
           <br>
            <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_accomodation'>UserName:</label>
            <div class='col-md-2'>
             
             <input type="text" class="form-control" name="userName" value="${chieflogin.userName}" placeholder="Enter User Name"  ng-model="Manager.userName" 
										ng-minlength="3" ng-maxlength="20" ng-pattern="/^[a-zA-Z0-9]*$/" autofocus id="userName"  ng-unique="{key='Manager.manager_id', property='username'}" required>
						 
						 
            </div>
          </div>
             <br> 
            
           <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_accomodation'>Password:</label>
            <div class='col-md-2'>
            
             <input class='form-control' id='password' name='password' placeholder='Enter password' value="${chieflogin.password}" type='password' required="required" ng-model="Manager.password" ng-minlength="8" ng-maxlength="20" ng-pattern="/(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z])/">
						
						
             
            </div>
          </div>
          <br>
          <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_accomodation'>Address:</label>
            <div class='col-md-2'>
             <input class='form-control' id='address' name='address' value="${chieflogin.address}" placeholder='Enter address' type='text'>
             
            </div>
          </div>
         
         <br>
         <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_accomodation'>ContactNo:</label>
            <div class='col-md-2'>
             <input class='form-control' id='contactNo' name='contactNo' value="${chieflogin.contactNo}" placeholder='Enter ContactNo' type='text'required="required" ng-model="Manager.contactNo" ng-minlength="10" ng-maxlength="10" ng-pattern="/^[0-9]*$/">
             </div>
           
          </div>
          <br>
         
          
          
           <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_equipment'>Designation</label>
            <div class='col-md-2'>
             <input class='form-control' id='designation' name='designation' value="Admin" readonly="true" placeholder='Enter designation' type='text'required="required" >
                <!-- <div class='form-group internal'>
                  <select class='form-control' id='id_equipment' name="designation">
                     <option>-Select-</option>
                     <option value="Admin">Admin</option>
                     <option value="Chief Manager">Chief Manager</option>
                    <option value="Executive Manager">Executive Manager</option>
                  </select>
                 
                
              </div> -->
          </div>
          </div>
          
           <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_accomodation'>Country:</label>
            <div class='col-md-2'>
             <input class='form-control' id='Country' name='Country' placeholder='Country' type='text'>
            </div>
          </div>
        <br>
           <div class='form-group'>
            <div class='col-md-offset-4 col-md-3'style="margin:2% -3% 3% 33%;">
              <button class='btn-lg btn-primary' type='submit' >SUBMIT</button>
            </div>
            </form>
            </div>
            </form:form>
             </div>
              </div>
    </div>
    </div>
             <form:form action="Back1.html" modelAttribute="command1"  method="get">
       <div class='col-md-3'>
              <button class='btn-lg btn-danger'   style="margin: 10% 0% 0% -33%;"  >Cancel</button>
            </div>
         </form:form>
         
        
     
  

 
 


</body>
</html>