<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ include file="/WEB-INF/views/Header_Exe.jsp" %>
      <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- <script type="text/javascript">
	function save() {
		alert("Image / Video added Successfully");
	}
</script> -->
<style>
.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
table 
{
    border: 1px solid black;
    border-collapse: collapse;
    width: 80%;
    margin: 0% 0% 1% 9%
}
th, td {
    text-align: left;
    padding: 8px;
}
tr:nth-child(even){background-color: #f2f2f2}
th {
    background-color: #3399ff;
    color: white;
}
</style>
<body>
  <div class='container'>
    <div class='panel panel-primary dialog-panel' style="margin: 10% 0% 0% 0%;">
      <div class='panel-heading'>
        <center><h2>Upload Videos / Photos :</h2></center>
      </div>
      <div class='panel-body'>
     <form:form method="POST" action="executive_saveImage.html" modelAttribute="command" enctype="multipart/form-data"> 
      <input class='form-control' name='rid' id='rid' value="${records.rid}" type='hidden'>  
      		<div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_accomodation'>Name :</label>
            <div class='col-md-3' style="margin: 0% 0% 0% -9%;"> 
             <input class='form-control' name='name' id='name' placeholder='Name of photo/video' type='text'>
            </div>
          </div>   
          <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_comments' style="margin: 4% 0% 0% -41%">Description:</label>
            <div class='col-md-6' style="margin:1% 0% 0% 41%" >
              <textarea class='form-control' name='description' id='description' placeholder='description of photo/video' rows='3'></textarea>
            </div>   
          </div>
           <div class='form-group'>
            <label class='control-label col-md-3 col-md-offset-3' for='id_accomodation' style="margin: 1% 0% 0% 25%;">Video / Photo : </label>
            <div class='col-md-3' style="margin: 1% 0% 0% -9%;">
             <input name="file" type='file'>
            </div>
          </div>  
          
           <!-- <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_equipment' style="margin: 2% 0% 0% 41%;" >Status : </label>
           <div class='col-md-2' style="margin: 2% 0% 0% -8%;">
                  <select class='form-control' id='id_equipment' name="status">
                    <option value="Enabled">Enabled</option>
                    <option value="Disabled">Disabled</option>
                  </select>
              </div>
          </div> -->
          
          <br>          
         <div class='form-group'>
            <div class='col-md-offset-2 col-md-4'>
              <button class='btn-lg btn-primary' type='submit' onclick="save()">SUBMIT</button>
              <a href="Executive_Close_&_Return_Records.html?rid=${records.rid}" class='btn-lg btn-primary'  type='submit'>Close & Return</a>
            </div>
            
          </div>
       </form:form>
      </div>
    </div>
  </div>

    <br>

<div class='panel-heading'>

      </div>



 <c:if test="${!empty images}"> 
	<table>
		<tr>
			<!-- <th>ID</th> -->
			<th>File Name</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>

		<c:forEach items="${images}" var="imageBean">
			<tr>
			<%-- 	<td><c:out value="${imageBean.id}"/></td> --%>
				<td><c:out value="${imageBean.name}"/></td>
				<td> <a href="Executive_EditImage_Upload.html?id=${imageBean.id}"><font color="Blue"><img src="img/edit.png" ></font></a></td>
				<td> <a href="deleteImage_executive.html?id=${imageBean.id}&rid=${records.rid}"><font color="Red"><img src="img/delete.png"> </font></a></td>		
			</tr>
		</c:forEach>
	</table>
 </c:if> 


</body>
</html>