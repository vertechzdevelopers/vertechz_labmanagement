package com.dineshonjava.fileUploadServlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.dineshonjava.model.scancopy;
import com.dineshonjava.service.EmployeeServiceImpl;
@WebServlet("/UploadDownloadFileServlet")
public class UploadDownloadFileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private ServletFileUpload uploader = null;
	@Override
	public void init() throws ServletException{
		System.out.println("in init of UploadDownloadFileServlet ");
		DiskFileItemFactory fileFactory = new DiskFileItemFactory();
		File filesDir = (File) getServletContext().getAttribute("FILES_DIR_FILE");
		System.out.println("filesDir  " + filesDir);
		fileFactory.setRepository(filesDir);
		System.out.println("fileFactory "  + fileFactory);
		this.uploader = new ServletFileUpload(fileFactory);
		System.out.println("uploader "  + uploader);
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("in do get .....");
		
		String fileName = request.getParameter("file");
		System.out.println("fileName  " + fileName);
		if(fileName == null || fileName.equals("")){
			throw new ServletException("File Name can't be null or empty");
		}
		File file = new File(request.getServletContext().getAttribute("FILES_DIR")+File.separator+fileName);
		System.out.println("file  " + file);
		if(!file.exists()){
			System.out.println("in if....");
			throw new ServletException("File doesn't exists on server.");
		}
		System.out.println("File location on server::"+file.getAbsolutePath());
		ServletContext ctx = getServletContext();
		System.out.println("ctx  " + ctx);
		InputStream fis = new FileInputStream(file);
		String mimeType = ctx.getMimeType(file.getAbsolutePath());
		response.setContentType(mimeType != null? mimeType:"application/octet-stream");
		response.setContentLength((int) file.length());
		response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
		
		ServletOutputStream os       = response.getOutputStream();
		byte[] bufferData = new byte[1024];
		int read=0;
		while((read = fis.read(bufferData))!= -1){
			os.write(bufferData, 0, read);
		}
		os.flush();
		os.close();
		fis.close();
		System.out.println("File downloaded at client successfully");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("in do post.....");
		if(!ServletFileUpload.isMultipartContent(request)){
			throw new ServletException("Content type is not multipart/form-data");
		}
		HttpSession session=request.getSession(); 
		String srid = session.getAttribute("rid").toString();	
		int rid = Integer.parseInt(srid);
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.write("<html><head></head><body>");
		try {
			System.out.println("request " + request);
			List<FileItem> fileItemsList = uploader.parseRequest(request);
			
			if(fileItemsList.isEmpty())
			{
				System.out.println("fileItemsList is empty....");
			}
			for(FileItem fileItem :fileItemsList){
				System.out.println("FieldName="+fileItem.getFieldName());
				System.out.println("FileName="+fileItem.getName());
				System.out.println("ContentType="+fileItem.getContentType());
				System.out.println("Size in bytes="+fileItem.getSize());
				if(fileItem.getContentType() != null)
				{
					System.out.println("in if ");
					File file = new File(request.getServletContext().getAttribute("FILES_DIR")+File.separator+fileItem.getName());
					System.out.println("Absolute Path at server="+file.getAbsolutePath());
					fileItem.write(file);
					scancopy scancopyBean = new scancopy();
					System.out.println("Name="+fileItem.getName());
					System.out.println("scancopyBean = "+scancopyBean);
					scancopyBean.setName(fileItem.getName());
					scancopyBean.setRid(rid);
								
					EmployeeServiceImpl employeeService = new EmployeeServiceImpl();
					System.out.println("employeeService = "+employeeService);
					employeeService.addScanned_Copies(scancopyBean);
					
					RequestDispatcher reqDispatcher = request.getRequestDispatcher("/WEB-INF/views/Scanned_Copies.jsp");
					reqDispatcher.forward(request, response);
					
					/*out.write("File "+fileItem.getName()+ " uploaded successfully.");				
					out.write("<br>");
					out.write("<a href=\"UploadDownl oadFileServlet?fileName="+fileItem.getName()+"\">Download "+fileItem.getName()+"</a>");
				*/
				}	
				/*out.write("File "+fileItem.getName()+ " uploaded successfully.");				
				out.write("<br>");
				out.write("<a href=\"UploadDownloadFileServlet?fileName="+fileItem.getName()+"\">Download "+fileItem.getName()+"</a>");*/
			}
			/*Iterator<FileItem> fileItemsIterator = fileItemsList.iterator();
			while(fileItemsIterator.hasNext()){
				System.out.println("in while.....");
				FileItem fileItem = fileItemsIterator.next();
				System.out.println("FieldName="+fileItem.getFieldName());
				System.out.println("FileName="+fileItem.getName());
				System.out.println("ContentType="+fileItem.getContentType());
				System.out.println("Size in bytes="+fileItem.getSize());
				if(fileItem.getContentType() != null)
				{
					System.out.println("in if ");
					File file = new File(request.getServletContext().getAttribute("FILES_DIR")+File.separator+fileItem.getName());
					System.out.println("Absolute Path at server="+file.getAbsolutePath());
					fileItem.write(file);
					scancopy scancopyBean = new scancopy();
					System.out.println("Name="+fileItem.getName());
					scancopyBean.setName(fileItem.getName());
					scancopyBean.setRid(rid);
								
					EmployeeServiceImpl employeeService = new EmployeeServiceImpl();
					employeeService.addScanned_Copies(scancopyBean);
					
					out.write("File "+fileItem.getName()+ " uploaded successfully.");				
					out.write("<br>");
					out.write("<a href=\"UploadDownloadFileServlet?fileName="+fileItem.getName()+"\">Download "+fileItem.getName()+"</a>");
				
				}	
				out.write("File "+fileItem.getName()+ " uploaded successfully.");				
				out.write("<br>");
				out.write("<a href=\"UploadDownloadFileServlet?fileName="+fileItem.getName()+"\">Download "+fileItem.getName()+"</a>");
			}*/
			
		} catch (FileUploadException e) {
			e.printStackTrace();
			System.out.println("In  FileUploadException" + e);
			out.write("Exception in uploading file." + e);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("In  Exception" + e);
			out.write("Exception in uploading file." + e);
		}
		//out.write("</body></html>");
		//doMyProcess(request, response);
	}

	/*protected void doMyProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		scancopy scancopyBean = (scancopy) request.getAttribute("scancopyBean");
		String fileName = scancopyBean.getName();
		System.out.println("fileName  " + fileName);
		if(fileName == null || fileName.equals("")){
			throw new ServletException("File Name can't be null or empty");
		}
		File file = new File(request.getServletContext().getAttribute("FILES_DIR")+File.separator+fileName);
		System.out.println("file  " + file);
		if(!file.exists()){
			System.out.println("in if....");
			throw new ServletException("File doesn't exists on server.");
		}
		System.out.println("File location on server::"+file.getAbsolutePath());
		ServletContext ctx = getServletContext();
		System.out.println("ctx  " + ctx);
		InputStream fis = new FileInputStream(file);
		String mimeType = ctx.getMimeType(file.getAbsolutePath());
		response.setContentType(mimeType != null? mimeType:"application/octet-stream");
		response.setContentLength((int) file.length());
		response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
		
		ServletOutputStream os       = response.getOutputStream();
		byte[] bufferData = new byte[1024];
		int read=0;
		while((read = fis.read(bufferData))!= -1){
			os.write(bufferData, 0, read);
		}
		os.flush();
		os.close();
		fis.close();
		System.out.println("File downloaded at client successfully");
		
		
		
		if(!ServletFileUpload.isMultipartContent(request)){
			throw new ServletException("Content type is not multipart/form-data");
		}
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.write("<html><head></head><body>");
		try {
			List<FileItem> fileItemsList = uploader.parseRequest(request);
			Iterator<FileItem> fileItemsIterator = fileItemsList.iterator();
			while(fileItemsIterator.hasNext()){
				FileItem fileItem = fileItemsIterator.next();
				System.out.println("FieldName="+fileItem.getFieldName());
				System.out.println("FileName="+fileItem.getName());
				System.out.println("ContentType="+fileItem.getContentType());
				System.out.println("Size in bytes="+fileItem.getSize());
				
				File file1 = new File(request.getServletContext().getAttribute("FILES_DIR")+File.separator+fileItem.getName());
				System.out.println("Absolute Path at server="+file1.getAbsolutePath());
				fileItem.write(file1);
				out.write("File "+fileItem.getName()+ " uploaded successfully.");
				
				
				out.write("<br>");
				out.write("<a href=\"UploadDownloadFileServlet?fileName="+fileItem.getName()+"\">Download "+fileItem.getName()+"</a>");
			}
		} catch (FileUploadException e) {
			System.out.println("In  FileUploadException" + e);
			out.write("Exception in uploading file." + e);
		} catch (Exception e) {
			System.out.println("In  Exception" + e);
			out.write("Exception in uploading file." + e);
		}
		out.write("</body></html>");
	}*/
}
