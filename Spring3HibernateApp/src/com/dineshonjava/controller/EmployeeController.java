
package com.dineshonjava.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Blob;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;


import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import com.dineshonjava.bean.SearchBean;
import com.dineshonjava.model.Active;
import com.dineshonjava.model.ChangePassword;
import com.dineshonjava.model.Composition;
import com.dineshonjava.model.FileSearch;
import com.dineshonjava.model.formulation;
import com.dineshonjava.model.image;
import com.dineshonjava.model.recordcomment;
import com.dineshonjava.model.scancopy;
import com.dineshonjava.model.selflifestudy;
import com.dineshonjava.model.Records;
import com.dineshonjava.model.chief_login;
import com.dineshonjava.model.coldtest;
import com.dineshonjava.model.coldtestdays;
import com.dineshonjava.model.analysis;
import com.dineshonjava.model.analyticalstudy;
import com.dineshonjava.model.analyticalstudy1;
import com.dineshonjava.model.ats;
import com.dineshonjava.model.atsstudies;
import com.dineshonjava.service.EmployeeService;
import util.DbUtil;


@Controller
public class EmployeeController {	
	@Autowired
	private EmployeeService employeeService;
	@Autowired
	private MailSender mailSender;

	
/*Close_&_Return_Records Start*/
	
	@RequestMapping(value = "/Back.html", method = RequestMethod.GET)
	public ModelAndView Back(@ModelAttribute("command1") chief_login Bean,HttpSession session)
	{
		System.out.println("in Back Controller");
		//employeeService.addchief(Bean);
		
		
		
		Map<String, Object> model = new HashMap<String, Object>();
		String userid = (String) session.getAttribute("UserName");
		
		System.out.println("UserName is 1 :"+userid);
		
		//String username1=session.getAttribute("username").toString();
		
		//System.out.println("UserName is"+username);
		
		List<Records> l=employeeService.listRemainder1(userid);
		
		
        for(Records m:l)
        {
        	System.out.println("Reference no is "+m.getReferance_no());
        	System.out.println("Remendar is"+m.getReminder_date());
        }		
		
		model.put("remainder",   employeeService.listRemainder1(userid));
		
		
		
		 return new ModelAndView("ATSRemender",model);
	}
	
	@RequestMapping(value = "/Back1.html", method = RequestMethod.GET)
	public ModelAndView Back1(@ModelAttribute("command1") chief_login Bean,HttpSession session)
	{
		System.out.println("in Back Controller");
		//employeeService.addchief(Bean);
		
		
		
		Map<String, Object> model = new HashMap<String, Object>();
		String userid = (String) session.getAttribute("UserName");
		
		System.out.println("UserName is 1 :"+userid);
		
		//String username1=session.getAttribute("username").toString();
		
		//System.out.println("UserName is"+username);
		
		List<Records> l=employeeService.listRemainder1(userid);
		
		
        for(Records m:l)
        {
        	System.out.println("Reference no is "+m.getReferance_no());
        	System.out.println("Remendar is"+m.getReminder_date());
        }
		
		
		model.put("remainder",   employeeService.listRemainder1(userid));
		
		
		
		 return new ModelAndView("Executive_ATSRemender",model);
	}
	
	
	
	@RequestMapping(value = "/Close_&_Return_Records.html", method = RequestMethod.GET)
	public ModelAndView CloseReturnRecords(HttpServletRequest request,HttpSession session,@ModelAttribute("command") Records records, 
			BindingResult result){
		int rid=(int) session.getAttribute("rid");
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("formulation",  employeeService.listFormulation());
		model.put("records", employeeService.getRecords(rid));
		return new ModelAndView("Chief_Lab_Record_Entry", model);
		
	}

/*Close_&_Return_Records End*/

	/*ATS Studies Start*/
	
	@RequestMapping(value = "/ATS_Studies.html", method = RequestMethod.GET)
	public ModelAndView ATS_Studies(@ModelAttribute("command") Records records, 
			BindingResult result){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("atsstudies",  employeeService.listATS_Studies(records.getRid()));
		model.put("analysis",  employeeService.listAnalysis());
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("atslist",  employeeService.listATS());
		model.put("active",  employeeService.listActive(records.getRid()));
		return new ModelAndView("ATS_Studies", model);
		
	}
	@RequestMapping(value = "/SaveATS_Studies.html", method = RequestMethod.POST)
	public ModelAndView SaveATS_Studies(HttpSession session, HttpServletRequest request, @ModelAttribute("command") atsstudies atsstudies,Records records, 
			BindingResult result) {		
		employeeService.addATS_Studies(atsstudies);
		
		int ats_id = atsstudies.getAts_id();
		int rid= (int) session.getAttribute("rid");
		 try {
		Connection con  = DbUtil.getConnection();	 
		 PreparedStatement stmt1=con.prepareStatement("select * from active where rid="+session.getAttribute("rid"));
		 System.out.println("select * from active where rid="+session.getAttribute("rid"));
		 ResultSet rs1=stmt1.executeQuery();
		 while(rs1.next()){
			//request.getParameter(rs1.getString("active"));
			//System.out.println("Analytical_Id:"+analyticalstudy.getAnalyticalstudy_id());
			System.out.println("Active:"+request.getParameter(rs1.getString("active")));
			System.out.println("Concentration:"+request.getParameter("C"+rs1.getString("active"))); 
			
			employeeService.addATS_Studies1(ats_id, request.getParameter(rs1.getString("active")), request.getParameter("C"+rs1.getString("active")), rid);
		 } 
		 } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		} 
      
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("atsstudies",  employeeService.listATS_Studies(records.getRid()));
		model.put("analysis",  employeeService.listAnalysis());
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("atslist",  employeeService.listATS());
		model.put("active",  employeeService.listActive(records.getRid()));
		return new ModelAndView("ATS_Studies", model);
	}
	
	@RequestMapping(value = "/UpdateATS_Studies.html", method = RequestMethod.POST)
	public ModelAndView UpdateATS_Studies(HttpSession session, HttpServletRequest request, @ModelAttribute("command") atsstudies atsstudies,Records records, 
			BindingResult result) {		
		employeeService.addATS_Studies(atsstudies);
		
		//int Analyticalstudy_id = analyticalstudy.getAnalyticalstudy_id();
		int rid= (int) session.getAttribute("rid");
		 try {
			 Connection con  = DbUtil.getConnection(); 
		 PreparedStatement stmt1=con.prepareStatement("select * from atsstudies1 where rid="+session.getAttribute("rid")+" AND ats_id="+atsstudies.getAts_id());
		 
		 ResultSet rs1=stmt1.executeQuery();
		 while(rs1.next()){
			//request.getParameter(rs1.getString("active"));
			 int ats_id =rs1.getInt("ats1_id");
			System.out.println("Active:"+request.getParameter(rs1.getString("ats_active")));
			System.out.println("Concentration:"+request.getParameter("C"+rs1.getString("ats_active"))); 
			
			employeeService.updateATS_Studies1(ats_id, request.getParameter(rs1.getString("ats_active")), request.getParameter("C"+rs1.getString("ats_active")), rid);
		 } 
		 } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		} 
      
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("atsstudies",  employeeService.listATS_Studies(records.getRid()));
		model.put("analysis",  employeeService.listAnalysis());
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("atslist",  employeeService.listATS());
		model.put("active",  employeeService.listActive(records.getRid()));
		return new ModelAndView("ATS_Studies", model);
	}
	
	@RequestMapping(value = "/DeleteATS_Studies.html", method = RequestMethod.GET)
	public ModelAndView deleteATS_Studies(@ModelAttribute("command")  atsstudies atsstudies,Records records,
			BindingResult result) {
		//System.out.println("Active id="+active.getActive_id());
		employeeService.deleteATS_Studies(atsstudies);
		employeeService.deleteATS_Studies1(atsstudies.getAts_id());
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("atsstudies", null);
		model.put("analysis",  employeeService.listAnalysis());
		model.put("atsstudies",  employeeService.listATS_Studies(records.getRid()));
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("atslist",  employeeService.listATS());
		model.put("active",  employeeService.listActive(records.getRid()));
		return new ModelAndView("ATS_Studies", model);
	}
	
	@RequestMapping(value = "/EditATS_Studies.html", method = RequestMethod.GET)
	public ModelAndView editATS_Studies(HttpSession session , @ModelAttribute("command")   atsstudies atsstudies,Records records,
			BindingResult result) {	
		session.setAttribute("ats_id", atsstudies.getAts_id());
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("atsstudies", employeeService.getATS_Studies(atsstudies.getAts_id()));
		model.put("analysis",  employeeService.listAnalysis());
		model.put("atslist",  employeeService.listATS());
		model.put("atsstudies1",  employeeService.listATS_Studies(records.getRid()));
		return new ModelAndView("EditATS_Studies", model);
	}

/*ATS Studies End*/

/*Cold Test Start*/
	
	@RequestMapping(value = "/Cold_Test.html", method = RequestMethod.GET)
	public ModelAndView Cold_Test(@ModelAttribute("command") Records records, 
			BindingResult result){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("coldtest",  employeeService.listCold_Test(records.getRid()));
		model.put("analysis",  employeeService.listAnalysis());
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Cold_Test", model);
		
	}
	@RequestMapping(value = "/SaveCold_Test.html", method = RequestMethod.POST)
	public ModelAndView SaveCold_Test(@ModelAttribute("command") coldtest coldtest,Records records, 
			BindingResult result) {		
		employeeService.addCold_Test(coldtest);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("coldtest",  employeeService.listCold_Test(records.getRid()));
		model.put("analysis",  employeeService.listAnalysis());
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Cold_Test", model);
	}
	@RequestMapping(value = "/DeleteCold_Test.html", method = RequestMethod.GET)
	public ModelAndView deleteCold_Test(@ModelAttribute("command")  coldtest coldtest,Records records,
			BindingResult result) {
		//System.out.println("Active id="+active.getActive_id());
		employeeService.deleteCold_Test(coldtest);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("coldtest", null);
		model.put("analysis",  employeeService.listAnalysis());
		model.put("coldtest",  employeeService.listCold_Test(records.getRid()));
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Cold_Test", model);
	}
	
	@RequestMapping(value = "/EditCold_Test.html", method = RequestMethod.GET)
	public ModelAndView editCold_Test(@ModelAttribute("command")   coldtest coldtest,Records records,
			BindingResult result) {		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("coldtest", employeeService.getCold_Test(coldtest.getCold_id()));
		model.put("analysis",  employeeService.listAnalysis());
		model.put("coldtest1",  employeeService.listCold_Test(records.getRid()));
		return new ModelAndView("EditCold_Test", model);
	}

/*Cold Test End*/

	/*Scanned_Copies Start*/


	@RequestMapping(value = "/Scanned_Copies.html", method = RequestMethod.GET)
	public ModelAndView Scanned_Copies(@ModelAttribute("command") Records records, 
			BindingResult result,HttpServletRequest request){
		Map<String, Object> model = new HashMap<String, Object>();
		HttpSession session = request.getSession();
		session.setAttribute("rid", records.getRid());
		model.put("scancopy",  employeeService.listScanned_Copies(records.getRid()));				
		model.put("records", employeeService.getRecords(records.getRid()));
		
		return new ModelAndView("Scanned_Copies", model);					
	}
	
	
	@RequestMapping(value="/UpdateScanned_Copies.html", method = RequestMethod.POST)
	public ModelAndView UpdateScanned_Copies(@ModelAttribute("command") scancopy scancopyBean,Records records,BindingResult result,
			@RequestParam("file") MultipartFile file,HttpServletRequest request, HttpServletResponse response) {
		System.out.println();System.out.println();System.out.println();System.out.println();
		System.out.println("In UpdateScanned_Copies.....");
		Map<String, Object> model = new HashMap<String, Object>();
		String name = file.getOriginalFilename();
		System.out.println("File:" + file.getOriginalFilename());
		System.out.println("ContentType:" + file.getContentType());
		scancopyBean.setName(file.getOriginalFilename());
		String rootPath = System.getProperty("catalina.home");
		File dir = new File(rootPath + File.separator + "tmpFiles");
		
		/*FileSearch fileSearch = new FileSearch();
		 //try different directory and filename :)
		fileSearch.searchDirectoryForUpdate(dir, file.getOriginalFilename());

		int count = fileSearch.getResult().size();
		if(count ==0){
		    System.out.println("\nNo result found!");
		}
		else
		{
		    System.out.println("\nFound " + count + " result!\n");
		    for (String matched : fileSearch.getResult())
		    {
		    	System.out.println("Found : " + matched);
		    	File delfile = new File(dir.getAbsolutePath()
						+ File.separator + name);;
				System.out.println("delfile name" + delfile);
	    		if(delfile.delete())
	    		{
	    			System.out.println(delfile.getName() + " is deleted!");
	    		}
	    		else
	    		{
	    			System.out.println("Delete operation is failed.");
	    		}
		    }
		}
		*/
		String str = employeeService.addScanned_Copies(scancopyBean);
		if(str.equals("success"))
		{
			System.out.println("insert is successfull...");
			
			
			    if (!file.isEmpty())
				{
					try {
							byte[] bytes = file.getBytes();
			
							// Creating the directory to store file
							
							if (!dir.exists())
							{
								dir.mkdirs();
								System.out.println("temFiles folder is created");
							}
							// Create the file on server
							File serverFile = new File(dir.getAbsolutePath()
									+ File.separator + name);
							BufferedOutputStream stream = new BufferedOutputStream(
									new FileOutputStream(serverFile));
							stream.write(bytes);
							stream.close();
							
							model.put("scancopy",  employeeService.listScanned_Copies(records.getRid()));
							model.put("analysis",  employeeService.listAnalysis());
							model.put("records", employeeService.getRecords(records.getRid()));
							
							System.out.println("Server File Location=" + serverFile.getAbsolutePath());

					System.out.println("You successfully uploaded file=" + name);
					
					} catch (Exception e)
					{
					System.out.println("You failed to upload " + name + " => " + e.getMessage());
					}
				} 
				else 
				{
					System.out.println("You failed to upload " + name
						+ " because the file was empty.");
				}
			
			
		}
		else{
			System.out.println("insert is unsuccessfull...");
			return new ModelAndView("Scanned_Copies_Error");
	       
		}
		return new ModelAndView("Scanned_Copies" ,model);
		
	}

	@RequestMapping(value = "/SaveScanned_Copies.html", method = RequestMethod.POST)
	public ModelAndView SaveScanned_Copies(@ModelAttribute("command") scancopy scancopyBean,Records records,BindingResult result,
			@RequestParam("file") MultipartFile file,HttpServletRequest request, HttpServletResponse response) {
		
		Map<String, Object> model = new HashMap<String, Object>();
		String name = file.getOriginalFilename();
		System.out.println("File:" + file.getOriginalFilename());
		System.out.println("ContentType:" + file.getContentType());
		scancopyBean.setName(file.getOriginalFilename());
		String rootPath = System.getProperty("catalina.home");
		File dir = new File(rootPath + File.separator + "tmpFiles");
		
		/*FileSearch fileSearch = new FileSearch();
		 //try different directory and filename :)
		fileSearch.searchDirectory(dir, file.getOriginalFilename());

		int count = fileSearch.getResult().size();*/
		/*if(count> 0)
		{
			File delfile = new File(dir.getAbsolutePath()
					+ File.separator + name);;

    		if(delfile.delete()){
    			System.out.println(delfile.getName() + " is deleted!");
    		}else{
    			System.out.println("Delete operation is failed.");
    		}
		}
		*/
		String str = employeeService.addScanned_Copies(scancopyBean);
		if(str.equals("success"))
		{
			System.out.println("insert is successfull...");
			
			
			    
			    if (!file.isEmpty())
				{
					try {
							byte[] bytes = file.getBytes();
			
							// Creating the directory to store file
							
							if (!dir.exists())
							{
								dir.mkdirs();
								System.out.println("temFiles folder is created");
							}
							// Create the file on server
							File serverFile = new File(dir.getAbsolutePath()
									+ File.separator + name);
							BufferedOutputStream stream = new BufferedOutputStream(
									new FileOutputStream(serverFile));
							stream.write(bytes);
							stream.close();
							
							model.put("scancopy",  employeeService.listScanned_Copies(records.getRid()));
							model.put("analysis",  employeeService.listAnalysis());
							model.put("records", employeeService.getRecords(records.getRid()));
							
							System.out.println("Server File Location=" + serverFile.getAbsolutePath());

					System.out.println("You successfully uploaded file=" + name);
					
					} catch (Exception e)
					{
					System.out.println("You failed to upload " + name + " => " + e.getMessage());
					}
				} 
				else 
				{
					System.out.println("You failed to upload " + name
						+ " because the file was empty.");
				}
			
			
		}
		else{
			System.out.println("insert is unsuccessfull...");
			return new ModelAndView("Scanned_Copies_Error");
	       
		}
		return new ModelAndView("Scanned_Copies" ,model);
		
	}

	
	@RequestMapping(value = "/DeleteScanned_Copies.html", method = RequestMethod.GET)
	public ModelAndView deleteScanned_Copies(@ModelAttribute("command")  scancopy scancopy,Records records,
			BindingResult result) {
		//System.out.println("Active id="+active.getActive_id());
		
		Map<String, Object> model = new HashMap<String, Object>();
		scancopy scancopyBean = employeeService.getScanned_Copies(scancopy.getScan_id());
		String rootPath = System.getProperty("catalina.home");
		File dir = new File(rootPath + File.separator + "tmpFiles");
		
		FileSearch fileSearch = new FileSearch();
		 //try different directory and filename :)
		fileSearch.deleteFile(dir,scancopyBean.getName());
		employeeService.deleteScanned_Copies(scancopy);
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("scancopy",  employeeService.listScanned_Copies(records.getRid()));
		return new ModelAndView("Scanned_Copies", model);
	}				
	@RequestMapping(value = "/editScanned_Copies", method = RequestMethod.GET)
	public ModelAndView editScanned_Copies(@ModelAttribute("command") scancopy scancopy,Records records,
			BindingResult result) {
		System.out.println("scan id " + scancopy.getScan_id());
		System.out.println("records id " + records.getRid());
		Map<String, Object> model = new HashMap<String, Object>();
		scancopy scancopyBean = employeeService.getScanned_Copies(scancopy.getScan_id());
		String rootPath = System.getProperty("catalina.home");
		File dir = new File(rootPath + File.separator + "tmpFiles");
		
		FileSearch fileSearch = new FileSearch();
		 //try different directory and filename :)
		fileSearch.deleteFile(dir,scancopyBean.getName());
		model.put("scancopy", employeeService.getScanned_Copies(scancopy.getScan_id()));
		model.put("records", employeeService.getRecords(records.getRid()));			
		model.put("scancopy1",  employeeService.listScanned_Copies(records.getRid()));
		return new ModelAndView("EditScanned_Copies", model);
	}
	
	@RequestMapping(value="/DisplaySacnCopy1" , method = RequestMethod.GET )
	public ModelAndView DisplaySacnCopy(@ModelAttribute("command") scancopy scancopy,Records records,
			BindingResult result) {
		System.out.println("in dispalyscancopy controller method");
	return new ModelAndView("DisplayScanCopy");
	}
	@RequestMapping(value="/DisplayImage1" , method = RequestMethod.GET )
	public ModelAndView DisplayImage(@ModelAttribute("command") scancopy scancopy,Records records,
			BindingResult result) {
		System.out.println("in DisplayImage controller method");
	return new ModelAndView("DisplayImage");
	}
/*Scanned_Copies End*/

/*SelfLife_Studies Start*/

@RequestMapping(value = "/SelfLife_Studies.html", method = RequestMethod.GET)
public ModelAndView SelfLife_Studies(@ModelAttribute("command") Records records, 
		BindingResult result){
	Map<String, Object> model = new HashMap<String, Object>();
	model.put("selflifestudy",  employeeService.listSelfLife_Studies(records.getRid()));
	model.put("atslist",  employeeService.listATS());
	model.put("records", employeeService.getRecords(records.getRid()));
	return new ModelAndView("SelfLife_Studies", model);
	
}
@RequestMapping(value = "/SaveSelfLife_Studies.html", method = RequestMethod.POST)
public ModelAndView SaveSelfLife_Studies(@ModelAttribute("command") selflifestudy selflifestudy,Records records, 
		BindingResult result) {
	
	employeeService.addSelfLife_Studies(selflifestudy);
	Map<String, Object> model = new HashMap<String, Object>();
	model.put("selflifestudy",  employeeService.listSelfLife_Studies(records.getRid()));
	model.put("atslist",  employeeService.listATS());
	model.put("records", employeeService.getRecords(records.getRid()));
	return new ModelAndView("SelfLife_Studies", model);
}
@RequestMapping(value = "/DeleteSelfLife_Studies.html", method = RequestMethod.GET)
public ModelAndView deleteSelfLife_Studies(@ModelAttribute("command")  selflifestudy selflifestudy,Records records,
		BindingResult result) {
	//System.out.println("Active id="+active.getActive_id());
	employeeService.deleteSelfLife_Studies(selflifestudy);
	Map<String, Object> model = new HashMap<String, Object>();
	//model.put("selflifestudy", null);
	model.put("analysis",  employeeService.listAnalysis());
	model.put("selflifestudy",  employeeService.listSelfLife_Studies(records.getRid()));
	return new ModelAndView("SelfLife_Studies", model);
}		
@RequestMapping(value = "/EditSelfLife_Studies.html", method = RequestMethod.GET)
public ModelAndView editSelfLife_Studies(@ModelAttribute("command")   selflifestudy selflifestudy,Records records,
		BindingResult result) {			
	Map<String, Object> model = new HashMap<String, Object>();
	model.put("selflifestudy", employeeService.getSelfLife_Studies(selflifestudy.getSelflife_id()));
	model.put("analysis",  employeeService.listAnalysis());
	model.put("selflifestudy1",  employeeService.listSelfLife_Studies(records.getRid()));
	return new ModelAndView("EditSelfLife_Studies", model);
}

/*SelfLife_Studies End*/

/*Analytical Studies Start*/

		@RequestMapping(value = "/Analytical_Studies.html", method = RequestMethod.GET)
		public ModelAndView Analytical_Studies(@ModelAttribute("command") Records records, 
				BindingResult result){
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("analyticalstudy",  employeeService.listAnalytical_Studies(records.getRid()));
			model.put("analysis",  employeeService.listAnalysis());
			model.put("records", employeeService.getRecords(records.getRid()));
			model.put("active",  employeeService.listActive(records.getRid()));
			return new ModelAndView("Analytical_Studies", model);
			
		}
		@RequestMapping(value = "/SaveAnalytical_Studies.html", method = RequestMethod.POST)
		public ModelAndView SaveAnalytical_Studies(HttpSession session, HttpServletRequest request, @ModelAttribute("command") analyticalstudy analyticalstudy,Records records, 
				BindingResult result) {
			
			employeeService.addAnalytical_Studies(analyticalstudy);
			/*analyticalstudy1 analyticalstudy11 = new analyticalstudy1();
			analyticalstudy1 analyticalstudy12 = new analyticalstudy1();//create the user entity object
		      
			analyticalstudy analyticalstudy1 = new analyticalstudy(); //create the first vehicle entity object
			
			
			//String active1=request.getParameter("Pramod123");
	        analyticalstudy11.setActive("Bhushan"); 
	        analyticalstudy11.setConcentration("Pramod");
	        analyticalstudy12.setActive("Abc");
	        analyticalstudy12.setConcentration("xyz");//Set the value to the user entity
	        analyticalstudy1.getAnalyticalstudy().add(analyticalstudy11);
	        analyticalstudy1.getAnalyticalstudy().add(analyticalstudy12);//add vehicle to the list of the vehicle
	        SessionFactory sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory(); //create session factory object
	        Session session1 = sessionFactory.openSession(); //create the session object
	        session1.beginTransaction(); //start the transaction of the session object
	      
	        session1.save(analyticalstudy11); //saving the vehicle to the database
	        session1.save(analyticalstudy12);
	        session1.save(analyticalstudy1); //save the user to the database
	        
	        session1.getTransaction().commit(); */
			//System.out.println("Analytical_Id:"+analyticalstudy.getAnalyticalstudy_id());
			int Analyticalstudy_id = analyticalstudy.getAnalyticalstudy_id();
			int rid= (int) session.getAttribute("rid");
			 try {
				 Connection con  = DbUtil.getConnection();
			 PreparedStatement stmt1=con.prepareStatement("select * from active where rid="+session.getAttribute("rid"));
			 System.out.println("select * from active where rid="+session.getAttribute("rid"));
			 ResultSet rs1=stmt1.executeQuery();
			 while(rs1.next()){
				//request.getParameter(rs1.getString("active"));
				//System.out.println("Analytical_Id:"+analyticalstudy.getAnalyticalstudy_id());
				System.out.println("Active:"+request.getParameter(rs1.getString("active")));
				System.out.println("Concentration:"+request.getParameter("C"+rs1.getString("active"))); 
				
				employeeService.addAnalytical_Studies1(Analyticalstudy_id, request.getParameter(rs1.getString("active")), request.getParameter("C"+rs1.getString("active")), rid);
			 } 
			 } catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			} 
	      
	       
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("analyticalstudy",  employeeService.listAnalytical_Studies(records.getRid()));
			model.put("analysis",  employeeService.listAnalysis());
			model.put("records", employeeService.getRecords(records.getRid()));
			model.put("active",  employeeService.listActive(records.getRid()));
			return new ModelAndView("Analytical_Studies", model);
		}
		
		@RequestMapping(value = "/UpdateAnalytical_Studies.html", method = RequestMethod.POST)
		public ModelAndView UpdateAnalytical_Studies1(HttpSession session, HttpServletRequest request, @ModelAttribute("command") analyticalstudy analyticalstudy,Records records, 
				BindingResult result) {
			
			employeeService.addAnalytical_Studies(analyticalstudy);		
			//int Analyticalstudy_id = analyticalstudy.getAnalyticalstudy_id();
			int rid= (int) session.getAttribute("rid");
			 try {
				 Connection con  = DbUtil.getConnection(); 
			 PreparedStatement stmt1=con.prepareStatement("select * from analyticalstudy1 where rid="+session.getAttribute("rid")+" AND analyticalstudy_id="+analyticalstudy.getAnalyticalstudy_id());
			 System.out.println("select * from active where rid="+session.getAttribute("rid"));
			 ResultSet rs1=stmt1.executeQuery();
			 while(rs1.next()){
				//request.getParameter(rs1.getString("active"));
				 int Analyticalstudy_id =rs1.getInt("analyticalstudy1_id");
				System.out.println("Active:"+request.getParameter(rs1.getString("analytical_active")));
				System.out.println("Concentration:"+request.getParameter("C"+rs1.getString("analytical_active"))); 
				
				employeeService.updateAnalytical_Studies1(Analyticalstudy_id, request.getParameter(rs1.getString("analytical_active")), request.getParameter("C"+rs1.getString("analytical_active")), rid);
			 } 
			 } catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			}    
	       
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("analyticalstudy",  employeeService.listAnalytical_Studies(records.getRid()));
			model.put("analysis",  employeeService.listAnalysis());
			model.put("records", employeeService.getRecords(records.getRid()));
			model.put("active",  employeeService.listActive(records.getRid()));
			return new ModelAndView("Analytical_Studies", model);
		}
		@RequestMapping(value = "/DeleteAnalytical_Studies.html", method = RequestMethod.GET)
		public ModelAndView deleteAnalytical_Studies(@ModelAttribute("command")  analyticalstudy analyticalstudy,Records records,
				BindingResult result) {
			System.out.println("analyticalstudy id="+analyticalstudy.getAnalyticalstudy_id());
			employeeService.deleteAnalytical_Studies(analyticalstudy);
			employeeService.deleteAnalytical_Studies1(analyticalstudy.getAnalyticalstudy_id());
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("analyticalstudy", null);
			model.put("records", employeeService.getRecords(records.getRid()));
			model.put("analysis",  employeeService.listAnalysis());
			model.put("analyticalstudy",  employeeService.listAnalytical_Studies(records.getRid()));
			model.put("active",  employeeService.listActive(records.getRid()));
			return new ModelAndView("Analytical_Studies", model);
		}		
		@RequestMapping(value = "/EditAnalytical_Studies.html", method = RequestMethod.GET)
		public ModelAndView editAnalytical_Studies(HttpSession session, HttpServletRequest request,@ModelAttribute("command")   analyticalstudy analyticalstudy,Records records,
				BindingResult result) {	
			session.setAttribute("analyticalstudy_id", analyticalstudy.getAnalyticalstudy_id());
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("active",  employeeService.listActive(records.getRid()));
			model.put("analyticalstudy", employeeService.getAnalytical_Studies(analyticalstudy.getAnalyticalstudy_id()));
			model.put("analysis",  employeeService.listAnalysis());
			model.put("records", employeeService.getRecords(records.getRid()));
			model.put("analyticalstudy1",  employeeService.listAnalytical_Studies(records.getRid()));
			return new ModelAndView("EditAnalytical_Studies", model);
		}

/*Analytical Studies End*/

/*Active details Start*/
	
	@RequestMapping(value = "/Active_Details.html", method = RequestMethod.GET)
	public ModelAndView Active_Details(@ModelAttribute("command") Records records, 
			BindingResult result){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("active",  employeeService.listActive(records.getRid()));
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Active_Details", model);		
	}	
	//Delete  active
	@RequestMapping(value = "/DeleteActive.html", method = RequestMethod.GET)
	public ModelAndView deleteActive(HttpSession session, HttpServletRequest request,Active active,Records records, 
			BindingResult result) {
		System.out.println("Active id="+active.getActive_id());
		System.out.println("record  id="+records.getRid());
		System.out.println("Active is="+active.getActive());
		int rid= (int) session.getAttribute("rid");
		employeeService.deleteActive(active);
		String activevalue =request.getParameter("active");
		employeeService.deleteAnalytical_Studies1(activevalue, rid );
		employeeService.deleteATS_Studies1(activevalue, rid);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("active",  employeeService.listActive(records.getRid()));
		return new ModelAndView("Active_Details", model);
	}	
	@RequestMapping(value = "/EditActive.html", method = RequestMethod.GET)
	public ModelAndView editActive(@ModelAttribute("command")   Active active,Records records, 
			BindingResult result) {		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("active", employeeService.getActive(active.getActive_id()));
		model.put("active1",  employeeService.listActive(records.getRid()));
		return new ModelAndView("EditActive", model);
	}
/*Active details End*/

/*ATS_SelfLife_Reminders Start*/

@RequestMapping(value = "/ATS_SelfLife_Reminders.html", method = RequestMethod.GET)
public ModelAndView ATS_SelfLife_Reminders(@ModelAttribute("command") Records records, 
		BindingResult result){
	Map<String, Object> model = new HashMap<String, Object>();
	model.put("records", employeeService.getRecords(records.getRid()));
	model.put("recordslist", employeeService.ListRecords());
	return new ModelAndView("ATS_SelfLife_Reminders", model);	
}	

@RequestMapping(value = "/SaveATS_SelfLife_Reminders.html", method = RequestMethod.POST)
public ModelAndView SaveATS_SelfLife_Reminders(@ModelAttribute("command")Records records, 
		BindingResult result) {		
	employeeService.addRecords(records);
	Map<String, Object> model = new HashMap<String, Object>();
	model.put("records", employeeService.getRecords(records.getRid()));
	model.put("recordslist", employeeService.ListRecords());
	return new ModelAndView("ATS_SelfLife_Reminders", model);
}
/*@RequestMapping(value = "/DeleteATS_SelfLife_Reminders.html", method = RequestMethod.GET)
public ModelAndView deleteATS_SelfLife_Reminders(@ModelAttribute("command")Records records, 
		BindingResult result) {
	employeeService.deleteRecords(records);
	Map<String, Object> model = new HashMap<String, Object>();
	model.put("active",  employeeService.listActive(records.getRid()));
	return new ModelAndView("Active_Details", model);
}*/	
@RequestMapping(value = "/EditATS_SelfLife_Reminders.html", method = RequestMethod.GET)
public ModelAndView editATS_SelfLife_Reminders(@ModelAttribute("command")Records records, 
		BindingResult result) {		
	Map<String, Object> model = new HashMap<String, Object>();
	model.put("records", employeeService.getRecords(records.getRid()));
	return new ModelAndView("EditATS_SelfLife_Reminders", model);
}
/*ATS_SelfLife_Reminders End*/
	
	
/*Active details Start
	
	@RequestMapping(value = "/SelfLife_Studies.html", method = RequestMethod.GET)
	public ModelAndView SelfLife_Studies(@ModelAttribute("command") Records records, 
			BindingResult result){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("active",  employeeService.listSelfLife_Studies(records.getRid()));
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Active_Details", model);		
	}	
	@RequestMapping(value = "/DeleteActive.html", method = RequestMethod.GET)
	public ModelAndView deleteActive(@ModelAttribute("command")  Active active,Records records, 
			BindingResult result) {
		System.out.println("Active id="+active.getActive_id());
		System.out.println("record  id="+records.getRid());
		employeeService.deleteActive(active);
		Map<String, Object> model = new HashMap<String, Object>();
		//model.put("active", null);
		model.put("active",  employeeService.listActive(records.getRid()));
		return new ModelAndView("Active_Details", model);
	}	
	@RequestMapping(value = "/EditActive.html", method = RequestMethod.GET)
	public ModelAndView editActive(@ModelAttribute("command")   Active active,Records records, 
			BindingResult result) {		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("active", employeeService.getActive(active.getActive_id()));
		model.put("active1",  employeeService.listActive(records.getRid()));
		return new ModelAndView("EditActive", model);
	}
Active details End*/
	
/*Composition details Start*/
	
	@RequestMapping(value = "/Composition_Details.html", method = RequestMethod.GET)
	public ModelAndView Composition_Details(@ModelAttribute("command") Records records, 
			BindingResult result){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("composition",  employeeService.listComposition(records.getRid()));
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Composition_Details", model);		
	}
	@RequestMapping(value = "/SaveComposition.html", method = RequestMethod.POST)
	public ModelAndView SaveComposition(@ModelAttribute("command") Composition composition,Records records, 
			BindingResult result) {		
		employeeService.addComposition(composition);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("composition",  employeeService.listComposition(records.getRid()));
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Composition_Details", model);
	}
	@RequestMapping(value = "/DeleteComposition.html", method = RequestMethod.GET)
	public ModelAndView deleteComposition(@ModelAttribute("command")  Composition composition,Records records, 
			BindingResult result) {		
		employeeService.deleteComposition(composition);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("active", null);
		model.put("composition",  employeeService.listComposition(records.getRid()));
		return new ModelAndView("Composition_Details", model);
	}	
	@RequestMapping(value = "/EditComposition.html", method = RequestMethod.GET)
	public ModelAndView editComposition(@ModelAttribute("command")   Composition composition,Records records, 
			BindingResult result) {		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("composition", employeeService.getComposition(composition.getComposition_id()));
		model.put("composition1",  employeeService.listComposition(records.getRid()));
		return new ModelAndView("EditComposition", model);
	}
/*Composition details End*/
	
	@RequestMapping(value = "/analysis", method = RequestMethod.GET)
	public ModelAndView analysis() {
		System.out.println("analysis ......");
		return new ModelAndView("Analysis");
	}	
	@RequestMapping(value="saveAnalysis" , method=RequestMethod.POST)
	public ModelAndView saveAnalysis(@ModelAttribute("command") analysis analysisBean,HttpServletRequest request,HttpSession session)
	{
		Map<String, Object> model = new HashMap<String, Object>();
		employeeService.addAnalysis(analysisBean);
		model.put("analysis",  employeeService.listAnalysis());		
		return listAnalysis(request,session);		
	}
	@RequestMapping(value="/analysisList", method = RequestMethod.GET)
	public ModelAndView listAnalysis(HttpServletRequest request,HttpSession session){
		
		
		//offset is page number
				String offset = (String)request.getParameter("offSet");
				// result is number of record displayed on each page
				int result = 10;
				// size is the total number of record present in DB
				int size ;
				List<Integer> pageList = new ArrayList<Integer>();
				List<analysis> analysisList ;
				/*in the beginning we set page number zero
				*/	if(offset!=null){
						int offsetreal = Integer.parseInt(offset);
						offsetreal = offsetreal*10;
						analysisList = employeeService.getAnalysisList(result,offsetreal);
						 
					}else{
						analysisList = employeeService.getAnalysisList(result,0);
						size = employeeService.getAnalysis_Size();
						/*if total record are divisible by 10 then we set page list 
						 * size one less than total size to avoid empty last page i.e if total record are 1220 then page list
						 *  size will be 121 because here we are taking page list size from 0-121 which is 122 pages*/
						if((size%result) == 0){
						session.setAttribute("size", (size/10)-1);
						}else{
							session.setAttribute("size", size/10);
						}
					}
				System.out.println("size = " + session.getAttribute("size").toString());
				/*when user click on any page number then this part will be executed. 
				 * else part will be executed on load i.e first time on page*/
				if(offset!=null){
					int listsize = Integer.parseInt(session.getAttribute("size").toString());
					if(Integer.parseInt(offset) <6){
						if(listsize>=10){
							for(int i= 1; i<=9;i++){
								pageList.add(i);
							}
						}else{
							for(int i= 1; i<=listsize;i++){
								pageList.add(i);
							}
						}

					}else{
						if(listsize >= 10 && Integer.parseInt(offset)-5 >0){
							List<Integer> temp = new ArrayList<Integer>(); 
							for(int i=Integer.parseInt(offset);i>Integer.parseInt(offset)-5;i--){
								temp.add(i);
							}
							for(int i=temp.size()-1;i>=0;i--){
								pageList.add(temp.get(i));
							}
						}
						if(listsize >= 10 && Integer.parseInt(offset)+5<listsize){
							for(int i=Integer.parseInt(offset)+1;i<Integer.parseInt(offset)+5;i++){
								pageList.add(i);
							}
						}else if(listsize >= 10){
							for(int i=Integer.parseInt(offset)+1;i<listsize;i++){
								pageList.add(i);
							}
						}
					}
				}else{
					int listsize = Integer.parseInt(session.getAttribute("size").toString());
					if(listsize>=10){
						for(int i= 1; i<=10;i++){
							pageList.add(i);
						}
					}else{
						for(int i= 1; i<=listsize;i++){
							pageList.add(i);
						}
					}
				}
				
				session.setAttribute("pageList", pageList);				
				Map<String, Object> model = new HashMap<String, Object>();		
				model.put("analysisList", analysisList);				
				return new ModelAndView("AnalysisList", model);
		
	}
	
	@RequestMapping(value="/editAnalysis", method = RequestMethod.GET)
	public ModelAndView editAnalysis(@ModelAttribute ("command") analysis analysisBean) {
		Map<String, Object> model = new HashMap<String, Object>();		
		model.put("aBean", employeeService.getAnalysis(analysisBean.getId()));		
		return new ModelAndView("AnalysisEdit",model);		
	}		
	@RequestMapping(value="deleteAnalysis" ,  method = RequestMethod.GET)
	public ModelAndView deleteAnalysis(@ModelAttribute("command") analysis analysisBean,HttpServletRequest request,HttpSession session){
		Map<String, Object> model = new HashMap<String, Object>();
		employeeService.deleteAnalysis(analysisBean);
		
		return listAnalysis(request,session);
	} 
	@RequestMapping(value="/atsList", method = RequestMethod.GET)
	public ModelAndView listATS(HttpServletRequest request,HttpSession session){
		
		//offset is page number
		String offset = (String)request.getParameter("offSet");
		// result is number of record displayed on each page
		int result = 10;
		// size is the total number of record present in DB
		int size ;
		List<Integer> pageList = new ArrayList<Integer>();
		List<ats> atsList ;
		/*in the beginning we set page number zero
		*/	if(offset!=null){
				int offsetreal = Integer.parseInt(offset);
				offsetreal = offsetreal*10;
				atsList = employeeService.getATSList(result,offsetreal);
				 
			}else{
				atsList = employeeService.getATSList(result,0);
				size = employeeService.getATS_Size();
				/*if total record are divisible by 10 then we set page list 
				 * size one less than total size to avoid empty last page i.e if total record are 1220 then page list
				 *  size will be 121 because here we are taking page list size from 0-121 which is 122 pages*/
				if((size%result) == 0){
				session.setAttribute("size", (size/10)-1);
				}else{
					session.setAttribute("size", size/10);
				}
			}
		System.out.println(session.getAttribute("size").toString());
		/*when user click on any page number then this part will be executed. 
		 * else part will be executed on load i.e first time on page*/
		if(offset!=null){
			int listsize = Integer.parseInt(session.getAttribute("size").toString());
			if(Integer.parseInt(offset) <6){
				if(listsize>=10){
					for(int i= 1; i<=9;i++){
						pageList.add(i);
					}
				}else{
					for(int i= 1; i<=listsize;i++){
						pageList.add(i);
					}
				}

			}else{
				if(listsize >= 10 && Integer.parseInt(offset)-5 >0){
					List<Integer> temp = new ArrayList<Integer>(); 
					for(int i=Integer.parseInt(offset);i>Integer.parseInt(offset)-5;i--){
						temp.add(i);
					}
					for(int i=temp.size()-1;i>=0;i--){
						pageList.add(temp.get(i));
					}
				}
				if(listsize >= 10 && Integer.parseInt(offset)+5<listsize){
					for(int i=Integer.parseInt(offset)+1;i<Integer.parseInt(offset)+5;i++){
						pageList.add(i);
					}
				}else if(listsize >= 10){
					for(int i=Integer.parseInt(offset)+1;i<listsize;i++){
						pageList.add(i);
					}
				}
			}
		}else{
			int listsize = Integer.parseInt(session.getAttribute("size").toString());
			if(listsize>=10){
				for(int i= 1; i<=10;i++){
					pageList.add(i);
				}
			}else{
				for(int i= 1; i<=listsize;i++){
					pageList.add(i);
				}
			}
		}		
		session.setAttribute("pageList", pageList);		
		Map<String, Object> model = new HashMap<String, Object>();		
		model.put("atsList", atsList);		
		return new ModelAndView("ATSList", model);		
	}	
	@RequestMapping(value = "/ats", method = RequestMethod.GET)
	public ModelAndView ats() {
		System.out.println("ats......");
		return new ModelAndView("ATS");
	}	
	@RequestMapping(value="saveATS" , method=RequestMethod.POST)
	public ModelAndView saveATS(@ModelAttribute("command") ats atsBean,HttpServletRequest request,HttpSession session)
	{
		Map<String, Object> model = new HashMap<String, Object>();
		employeeService.addATS(atsBean);
		model.put("atslist",  employeeService.listATS());
		return listATS(request, session);	
	}
	@RequestMapping(value="/editATS", method = RequestMethod.GET)
	public ModelAndView editATS(@ModelAttribute ("command") ats atsBean) {
		Map<String, Object> model = new HashMap<String, Object>();		
		model.put("aBean", employeeService.getATS(atsBean.getId()));		
		return new ModelAndView("ATSEdit",model);		
	}	
	@RequestMapping(value="deleteATS" ,  method = RequestMethod.GET)
	public ModelAndView deleteATS(@ModelAttribute("command") ats atsBean,HttpServletRequest request,HttpSession session){
		Map<String, Object> model = new HashMap<String, Object>();
		employeeService.deleteATS(atsBean);
		
		return listATS(request, session);
	}	
	
	// Back to Admin
			@RequestMapping(value = "/Back2.html", method = RequestMethod.GET)
			public ModelAndView Back2(@ModelAttribute("command1") chief_login Bean)
			{
				
				 return new ModelAndView("Header_Admin"); 
			}
//Back to Admin
	@RequestMapping(value="/coldtestdaysList", method = RequestMethod.GET)
	public ModelAndView listColdTestDays(HttpServletRequest request,HttpSession session){
		
		
		
		//offset is page number
				String offset = (String)request.getParameter("offSet");
				// result is number of record displayed on each page
				int result = 10;
				// size is the total number of record present in DB
				int size ;
				List<Integer> pageList = new ArrayList<Integer>();
				List<coldtestdays> coldtestdaysList ;
				/*in the beginning we set page number zero
				*/	if(offset!=null){
						int offsetreal = Integer.parseInt(offset);
						offsetreal = offsetreal*10;
						coldtestdaysList = employeeService.getColdTestDaysList(result,offsetreal);						 
					}else{
						coldtestdaysList = employeeService.getColdTestDaysList(result,0);
						size = employeeService.getColdTestDays_Size();
						/*if total record are divisible by 10 then we set page list 
						 * size one less than total size to avoid empty last page i.e if total record are 1220 then page list
						 *  size will be 121 because here we are taking page list size from 0-121 which is 122 pages*/
						if((size%result) == 0){
						session.setAttribute("size", (size/10)-1);
						}else{
							session.setAttribute("size", size/10);
						}
					}
				System.out.println(session.getAttribute("size").toString());
				/*when user click on any page number then this part will be executed. 
				 * else part will be executed on load i.e first time on page*/
				if(offset!=null){
					int listsize = Integer.parseInt(session.getAttribute("size").toString());
					if(Integer.parseInt(offset) <6){
						if(listsize>=10){
							for(int i= 1; i<=9;i++){
								pageList.add(i);
							}
						}else{
							for(int i= 1; i<=listsize;i++){
								pageList.add(i);
							}
						}

					}else{
						if(listsize >= 10 && Integer.parseInt(offset)-5 >0){
							List<Integer> temp = new ArrayList<Integer>(); 
							for(int i=Integer.parseInt(offset);i>Integer.parseInt(offset)-5;i--){
								temp.add(i);
							}
							for(int i=temp.size()-1;i>=0;i--){
								pageList.add(temp.get(i));
							}
						}
						if(listsize >= 10 && Integer.parseInt(offset)+5<listsize){
							for(int i=Integer.parseInt(offset)+1;i<Integer.parseInt(offset)+5;i++){
								pageList.add(i);
							}
						}
						else if(listsize >= 10){
							for(int i=Integer.parseInt(offset)+1;i<listsize;i++){
								pageList.add(i);
							}
						}
					}
				}
				else{
					int listsize = Integer.parseInt(session.getAttribute("size").toString());
					if(listsize>=10){
						for(int i= 1; i<=10;i++){
							pageList.add(i);
						}
					}else{
						for(int i= 1; i<=listsize;i++){
							pageList.add(i);
						}
					}
				}				
				session.setAttribute("pageList", pageList);				
				Map<String, Object> model = new HashMap<String, Object>();		
				model.put("coldtestdaysList", coldtestdaysList);		
				return new ModelAndView("ColdTestDaysList", model);			
	}	
	@RequestMapping(value = "/coldtestdays", method = RequestMethod.GET)
	public ModelAndView atss() {
		System.out.println("coldtestdays......");
		return new ModelAndView("ColdTestDays");
	}	
	@RequestMapping(value="saveColdTestDays" , method=RequestMethod.POST)
	public ModelAndView saveColdTestDays(@ModelAttribute("command") coldtestdays ctdBean,HttpServletRequest request,HttpSession session)
	{
		Map<String, Object> model = new HashMap<String, Object>();
		employeeService.addColdTestDays(ctdBean);
		model.put("ctdlist",  employeeService.listColdTestDays());
		return listColdTestDays(request, session);		
	}
	@RequestMapping(value="/editColdTestDays", method = RequestMethod.GET)
	public ModelAndView editColdTestDays(@ModelAttribute ("command") coldtestdays ctdBean) {
		Map<String, Object> model = new HashMap<String, Object>();		
		model.put("ctdBean", employeeService.getColdTestDays(ctdBean.getId()));		
		return new ModelAndView("ColdTestDaysEdit",model);		
	}		
	@RequestMapping(value="deleteColdTestDays" ,  method = RequestMethod.GET)
	public ModelAndView deleteColdTestDays(@ModelAttribute("command") coldtestdays ctdBean,HttpServletRequest request,HttpSession session){
		Map<String, Object> model = new HashMap<String, Object>();
		employeeService.deleteColdTestDays(ctdBean);
		/*model.put("ctdlist",  employeeService.listATS());
		return new ModelAndView("ColdTestDaysList",model);*/
		return listColdTestDays(request, session);	
	}	
	@RequestMapping(value = "/formulation", method = RequestMethod.GET)
	public ModelAndView formulation() {
		System.out.println("formulation ......");
		return new ModelAndView("Formulation");
	}	
	@RequestMapping(value = "/saveformulation", method = RequestMethod.POST)
	public ModelAndView saveFormulation(@ModelAttribute("command") formulation formulationBean,HttpServletRequest request,HttpSession session) {		
		Map<String, Object> model = new HashMap<String, Object>();
		employeeService.addFormulation(formulationBean);
		model.put("formulations",  employeeService.listFormulation());
		return listFormulation(request, session);		
	}	
	@RequestMapping(value="/formulationList", method = RequestMethod.GET)
	public ModelAndView listFormulation(HttpServletRequest request,HttpSession session) {
		//offset is page number
		String offset = (String)request.getParameter("offSet");
		// result is number of record displayed on each page
		int result = 10;
		// size is the total number of record present in DB
		int size ;
		List<Integer> pageList = new ArrayList<Integer>();
		List<formulation> formulationList ;
		/*in the beginning we set page number zero
		*/	if(offset!=null){
				int offsetreal = Integer.parseInt(offset);
				offsetreal = offsetreal*10;
				formulationList = employeeService.getFormulationList(result,offsetreal);
				 
			}else{
				formulationList = employeeService.getFormulationList(result,0);
				size = employeeService.getFormulationSize();
				/*if total record are divisible by 10 then we set page list 
				 * size one less than total size to avoid empty last page i.e if total record are 1220 then page list
				 *  size will be 121 because here we are taking page list size from 0-121 which is 122 pages*/
				if((size%result) == 0){
				session.setAttribute("size", (size/10)-1);
				}else{
					session.setAttribute("size", size/10);
				}
			}
		System.out.println(session.getAttribute("size").toString());
		/*when user click on any page number then this part will be executed. 
		 * else part will be executed on load i.e first time on page*/
		if(offset!=null){
			int listsize = Integer.parseInt(session.getAttribute("size").toString());
			if(Integer.parseInt(offset) <6){
				if(listsize>=10){
					for(int i= 1; i<=9;i++){
						pageList.add(i);
					}
				}else{
					for(int i= 1; i<=listsize;i++){
						pageList.add(i);
					}
				}

			}else{
				if(listsize >= 10 && Integer.parseInt(offset)-5 >0){
					List<Integer> temp = new ArrayList<Integer>(); 
					for(int i=Integer.parseInt(offset);i>Integer.parseInt(offset)-5;i--){
						temp.add(i);
					}
					for(int i=temp.size()-1;i>=0;i--){
						pageList.add(temp.get(i));
					}
				}
				if(listsize >= 10 && Integer.parseInt(offset)+5<listsize){
					for(int i=Integer.parseInt(offset)+1;i<Integer.parseInt(offset)+5;i++){
						pageList.add(i);
					}
				}else if(listsize >= 10){
					for(int i=Integer.parseInt(offset)+1;i<listsize;i++){
						pageList.add(i);
					}
				}
			}
		}
		else{
			int listsize = Integer.parseInt(session.getAttribute("size").toString());
			if(listsize>=10){
				for(int i= 1; i<=10;i++){
					pageList.add(i);
				}
			}else{
				for(int i= 1; i<=listsize;i++){
					pageList.add(i);
				}
			}
		}		
		session.setAttribute("pageList", pageList);		
		Map<String, Object> model = new HashMap<String, Object>();		
		model.put("formulationList", formulationList);		
		return new ModelAndView("FormulationList", model);	
	}	
	@RequestMapping(value="/editFormulation", method = RequestMethod.GET)
	public ModelAndView editFormulation(@ModelAttribute ("command") formulation formualtionBean) {
		Map<String, Object> model = new HashMap<String, Object>();		
		model.put("fBean", employeeService.getFormulation(formualtionBean.getId()));		
		return new ModelAndView("FormulationEdit",model);	
	}		
	@RequestMapping(value="deleteFormulation" ,  method = RequestMethod.GET)
	public ModelAndView deleteFormulation(@ModelAttribute("command") formulation formulationBean,HttpServletRequest request,HttpSession session){
		employeeService.deleteFormulation(formulationBean);		
		return listFormulation(request, session);
	}
	
	@RequestMapping(value="/Image_Upload.html" , method = RequestMethod.GET)
	public ModelAndView uploadVideos(@ModelAttribute("command") image imageBean,Records records,
			BindingResult result){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("images",  employeeService.listImage(records.getRid()));
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("UploadImage", model);
	}	
	@RequestMapping(value="saveImage" , method = RequestMethod.POST)
	public ModelAndView saveImage(@ModelAttribute("command") image imageBean,Records records,BindingResult result,
			@RequestParam("file") MultipartFile file)
	{	
		System.out.println("Name:" + imageBean.getName());
		System.out.println("Desc:" + imageBean.getDescription());
		System.out.println("File:" + file.getName());
		System.out.println("ContentType:" + file.getContentType());		
		try {
			Blob blob = Hibernate.createBlob(file.getInputStream());
			imageBean.setName(file.getOriginalFilename());
			imageBean.setPhoto(blob);
			imageBean.setContentType(file.getContentType());
		} catch (IOException e) {
			e.printStackTrace();
		}		
		employeeService.addImage(imageBean);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("images",  employeeService.listImage(records.getRid()));
		model.put("records", employeeService.getRecords(records.getRid()));		
		return new ModelAndView("UploadImage", model);
	}	
	@RequestMapping(value="/editImage", method = RequestMethod.GET )
	public ModelAndView editImage(@ModelAttribute("command") image imageBean,Records records,BindingResult result)
	{
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("image", employeeService.getImage(imageBean.getId()));
		model.put("images",  employeeService.listImage(records.getRid()));
		/*model.put("records", employeeService.getRecords(imageBean.getRid()));	*/	
		return new ModelAndView("EditImage", model);
	}	
	@RequestMapping(value="/deleteImage" , method = RequestMethod.GET )
	public ModelAndView deleteImage(@ModelAttribute("command") image imageBean,Records records,BindingResult result)
	{	
		Map<String, Object> model = new HashMap<String, Object>();
		System.out.print("records.getRid() "  + records.getRid());		
		model.put("records", employeeService.getRecords(records.getRid()));	
		model.put("images",  employeeService.listImage(records.getRid()));
		employeeService.deleteImage(imageBean);
		return new ModelAndView("UploadImage", model);
	}	
	@RequestMapping(value = "/SaveActive.html", method = RequestMethod.POST)
	public ModelAndView SaveActive(@ModelAttribute("command") Active active,Records records, 
			BindingResult result) {	
		employeeService.addRecords(records);
		employeeService.addActive(active);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("active",  employeeService.listActive(records.getRid()));
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Active_Details", model);
	}
	
	
	@RequestMapping(value = "/SaveRecord.html", method = RequestMethod.POST)
	public ModelAndView saveRecord(@ModelAttribute("command1") Records records, 
			BindingResult result,HttpServletRequest request, HttpSession session)throws Exception {	
		
		if(request.getParameter("referance_no").equals("null")){
		String ref_no=records.getDate();
		System.out.println("Ref No is in controller"+ref_no);		
		Connection con  = DbUtil.getConnection();
        //  alert("Pramod");
    	Statement st = con.createStatement();
    	String sql = ("SELECT * FROM records where date='"+ref_no+"';");
    	ResultSet rs = st.executeQuery(sql);
    	
    	int count=1;
    	while(rs.next()) {
    		count++;
    		
    	}
    	
    	System.out.println("Count is"+count);
		String reference_no="Ver/"+ref_no+"/"+count;
		records.setReferance_no(reference_no);
		}
		employeeService.addRecords(records);
		String Designation="Chief Manager";
		session.setAttribute("rid", records.getRid());
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("formulation",  employeeService.listFormulation());
		model.put("executive_list",  employeeService.ListChiefLogin());
		model.put("chief_list", employeeService.getEmployeeList(Designation));
		return new ModelAndView("Chief_Lab_Record_Entry", model);
	}
	@RequestMapping(value = "/DeleteRecords.html", method = RequestMethod.GET)
	public ModelAndView DeleteRecords(@ModelAttribute("command")  Records records,
			BindingResult result,HttpServletRequest request, HttpSession session) {
		System.out.println("record id = " + records.getRid());
		employeeService.deleteRecord(records);
		Map<String, Object> model = new HashMap<String, Object>(); 
		//model.put("records",  employeeService.listRecords());
		return ChiefLabRecord(request, session);
	}	
	@RequestMapping(value = "/EditRecords.html", method = RequestMethod.GET)
	public ModelAndView EditRecords(@ModelAttribute("command")   Records records,
			BindingResult result) {
		String Designation="Chief Manager";
		System.out.println("RID="+records.getRid());
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("formulation",  employeeService.listFormulation());
		model.put("executive_list",  employeeService.ListChiefLogin());
		model.put("chief_list", employeeService.getEmployeeList(Designation));
		
		return new ModelAndView("EditRecords", model);
	}
	
	@RequestMapping(value="searchRecord" ,method = RequestMethod.POST)
	public ModelAndView SerachRecord(@ModelAttribute("command") SearchBean bean,BindingResult result){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("records", employeeService.serachRecords(bean));
	
		return new ModelAndView("ChiefLabRecordSearch", model);
	}
	
	
	/*@RequestMapping(value = "/ATS_SelfLife_Reminders.html", method = RequestMethod.GET)
	public ModelAndView ATS_SelfLife_Reminders(@ModelAttribute("command1") Records records, 
			BindingResult result){		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("records",  employeeService.listRecords());
		return new ModelAndView("Chief_Lab_Record_Entry", model);
	}*/
   
	@RequestMapping(value="/ChiefRecordRegistration", method = RequestMethod.GET)
	public ModelAndView ChiefRecordRegistration() throws HibernateException, Exception  {
		Date date=new Date();
		Calendar calendar = Calendar.getInstance();
		date=calendar.getTime();
		String Designation="Chief Manager";
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("formulation",  employeeService.listFormulation());
		model.put("count",  employeeService.TodayRecordCount(date));
		model.put("executive_list",  employeeService.ListChiefLogin());
		model.put("chief_list", employeeService.getEmployeeList(Designation));
		
		return new ModelAndView("Chief_Record_Registration", model);
	}	
	@RequestMapping(value="/ExecRecordRegistration", method = RequestMethod.GET)
	public ModelAndView ExecRecordRegistration() {
		 
		Map<String, Object> model = new HashMap<String, Object>();
		
		model.put("formulation",  employeeService.listFormulation());
		return new ModelAndView("Exec_Record_Registration", model);
	}	
	@RequestMapping(value = "/chieflogin", method = RequestMethod.GET)
	public ModelAndView addEmployee1() {			
		Map<String, Object> model = new HashMap<String, Object>();
		return new ModelAndView("ChiefManagerLogin", model);
	}	
	@RequestMapping(value = "/executivelogin", method = RequestMethod.GET)
	public ModelAndView addEmployee2() {		
		Map<String, Object> model = new HashMap<String, Object>();
		return new ModelAndView("ManagerLogin", model);	
	}	
	@RequestMapping(value = "/DashBoard", method = RequestMethod.GET)
	public ModelAndView welcome() {
		return new ModelAndView("DashBoard");
	}	
	@RequestMapping(value = "/ChiefLabRecordSearch.html", method = RequestMethod.GET)
	public ModelAndView ChiefLabRecordSearch(HttpServletRequest request,HttpSession session) {
		String username= (String) session.getAttribute("UserName");
		System.out.println("username:"+username);
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("formulation",  employeeService.listFormulation());
			model.put("chiefrecord",  employeeService.listChiefRecord(username));
			return new ModelAndView("ChiefLabRecordSearch",model);
	}	
	@RequestMapping(value = "/ChiefLabRecord", method = RequestMethod.GET)
	public ModelAndView ChiefLabRecord(HttpServletRequest request, HttpSession session) {
		/*Map<String, Object> model = new HashMap<String, Object>();
		model.put("records",  prepareListofBean2(employeeService.listRecords()));
		return new ModelAndView("Chief_Record_List", model);*/
		
		//offset is page number
				String offset = (String)request.getParameter("offSet");
				// result is number of record displayed on each page
				int result = 10;
				// size is the total number of record present in DB
				int size ;
				List<Integer> pageList = new ArrayList<Integer>();
				List<Records> recordsList ;
				/*in the beginning we set page number zero
				*/	if(offset!=null){
						int offsetreal = Integer.parseInt(offset);
						offsetreal = offsetreal*10;
						recordsList = employeeService.getRecordsList(result,offsetreal);
						 
					}else{
						recordsList = employeeService.getRecordsList(result,0);
						size = employeeService.getRecords_Size();
						/*if total record are divisible by 10 then we set page list 
						 * size one less than total size to avoid empty last page i.e if total record are 1220 then page list
						 *  size will be 121 because here we are taking page list size from 0-121 which is 122 pages*/
						if((size%result) == 0){
						session.setAttribute("size", (size/10)-1);
						}else{
							session.setAttribute("size", size/10);
						}
					}
				System.out.println(session.getAttribute("size").toString());
				/*when user click on any page number then this part will be executed. 
				 * else part will be executed on load i.e first time on page*/
				if(offset!=null){
					int listsize = Integer.parseInt(session.getAttribute("size").toString());
					if(Integer.parseInt(offset) <6){
						if(listsize>=10){
							for(int i= 1; i<=9;i++){
								pageList.add(i);
							}
						}else{
							for(int i= 1; i<=listsize;i++){
								pageList.add(i);
							}
						}

					}else{
						if(listsize >= 10 && Integer.parseInt(offset)-5 >0){
							List<Integer> temp = new ArrayList<Integer>(); 
							for(int i=Integer.parseInt(offset);i>Integer.parseInt(offset)-5;i--){
								temp.add(i);
							}
							for(int i=temp.size()-1;i>=0;i--){
								pageList.add(temp.get(i));
							}
						}
						if(listsize >= 10 && Integer.parseInt(offset)+5<listsize){
							for(int i=Integer.parseInt(offset)+1;i<Integer.parseInt(offset)+5;i++){
								pageList.add(i);
							}
						}else if(listsize >= 10){
							for(int i=Integer.parseInt(offset)+1;i<listsize;i++){
								pageList.add(i);
							}
						}
					}
				}else{
					int listsize = Integer.parseInt(session.getAttribute("size").toString());
					if(listsize>=10){
						for(int i= 1; i<=10;i++){
							pageList.add(i);
						}
					}else{
						for(int i= 1; i<=listsize;i++){
							pageList.add(i);
						}
					}
				}				
				session.setAttribute("pageList", pageList);				
				Map<String, Object> model = new HashMap<String, Object>();		
				model.put("recordsList", recordsList);					
				return new ModelAndView("Chief_Record_List", model);		
	}
	
	@RequestMapping(value = "/ChiefRecordSearch.html", method = RequestMethod.POST)
	public ModelAndView ChiefRecordSearch( HttpServletRequest request,HttpSession session, @RequestParam("formulation") String formulation,  @RequestParam("status") String status, @RequestParam("active") String active ) {
		//System.out.println("fromdate:"+fromdate);	
		//System.out.println("todate:"+todate);
		System.out.println("formulation:"+formulation);
		System.out.println("active:"+active);
		System.out.println("status:"+status);
		String username= (String) session.getAttribute("UserName"); 
		System.out.println("username:"+username);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("formulation",  employeeService.listFormulation());
		if(formulation!="" && status!="" && active!="")
			{
			    model.put("records",  employeeService.ChiefRecordSerchBy_Fomulation_Status_Active(formulation, status, active, username));
			}
		else if(formulation!="" && status!="")
		{
		    model.put("records",  employeeService.ChiefRecordSerchBy_Fomulation_Status(formulation, status, username));
		}
		else if(formulation!="" && active!="")
		{
		    model.put("records",  employeeService.ChiefRecordSerchBy_Fomulation_Active(formulation, active, username));
		}
		else if(status!="" && active!="")
		{
		    model.put("records",  employeeService.ChiefRecordSerchBy_Status_Active(status, active, username));
		}
		
		else if(status !="")
		{
			model.put("records",  employeeService.ChiefRecordSerchByStatus(status, username));
		}
		else if(formulation !="")
		{
		    model.put("records",  employeeService.ChiefRecordSerchByFormulation(formulation, username));
		}
		else if(active != "" )
		{
		    model.put("records",  employeeService.ChiefRecordSerchByActive(active, username));
		}
	
		return new ModelAndView("ChiefRecordSearch", model);
		
	}
//  @RequestParam("todate") Date todate,  @RequestParam("fromdate") Date fromdate,  @RequestParam("active") String active
 @RequestMapping(value = "/ChiefRecordSearchByDate.html", method = RequestMethod.POST)
	public ModelAndView ChiefRecordSearchByDate(HttpSession session, HttpServletRequest request) throws Exception {
		 Date fromdate=null;
			Date todate=null;
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("formulation",  employeeService.listFormulation());
			 String fromdate1=request.getParameter("fromdate");
			String todate1=request.getParameter("todate");
			SimpleDateFormat s= new SimpleDateFormat("yyyy/MM/dd");
			String username = (String) session.getAttribute("UserName"); 
			 
			 System.out.println("fromdate:"+fromdate1);	
				System.out.println("todate:"+todate1);
				if(!(todate1.equals("")) && fromdate1.equals(""))
				{
					todate=s.parse(todate1);
					model.put("records",  employeeService.SerchByDate1(todate, username));
				}
				if(!(fromdate1.equals("")) && todate1.equals(""))
				{
					fromdate=s.parse(fromdate1);
					model.put("records",  employeeService.SerchByDate1(fromdate, username));
				}
								
				if(!(todate1.equals("")) && !(fromdate1.equals("")))
				{
					todate=s.parse(todate1);
					fromdate=s.parse(fromdate1);
				    model.put("records",  employeeService.SerchByDate(todate, fromdate, username));
				}
	    return new ModelAndView("ChiefRecordSearch", model);
		
	}
	
 @RequestMapping(value = "/RecordSearchAll.html", method = RequestMethod.POST)
	public ModelAndView RecordSearchAll( @RequestParam("formulation") String formulation,  @RequestParam("status") String status, @RequestParam("active") String active ) {
		//System.out.println("fromdate:"+fromdate);	
		//System.out.println("todate:"+todate);
		System.out.println("formulation:"+formulation);
		System.out.println("active:"+active);
		System.out.println("status:"+status);
		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("formulation",  employeeService.listFormulation());
		if(formulation!="" && status!="" && active!="")
			{
			    model.put("records",  employeeService.SerchBy_Fomulation_Status_Active(formulation, status, active));
			}
		else if(formulation!="" && status!="")
		{
		    model.put("records",  employeeService.SerchBy_Fomulation_Status(formulation, status));
		}
		else if(formulation!="" && active!="")
		{
		    model.put("records",  employeeService.SerchBy_Fomulation_Active(formulation, active));
		}
		else if(status!="" && active!="")
		{
		    model.put("records",  employeeService.SerchBy_Status_Active(status, active));
		}
		
		else if(status != "")
		{
			model.put("records",  employeeService.SerchByStatus(status));
		}
		else if(formulation != "")
		{
		    model.put("records",  employeeService.SerchByFormulation(formulation));
		}
		else if(active != "" )
		{
		    model.put("records",  employeeService.SerchByActive(active));
		}
	
		return new ModelAndView("Chief_Lab_Record_Search_All_Next", model);
		
	}

@RequestMapping(value = "/RecordSearchAllByDate.html", method = RequestMethod.POST)
public ModelAndView RecordSearchAllByDate(HttpSession session, HttpServletRequest request) throws Exception{
 Date fromdate=null;
	Date todate=null;
 SimpleDateFormat s= new SimpleDateFormat("yyyy/MM/dd");
  String todate1=request.getParameter("todate");
  String fromdate1=request.getParameter("fromdate");
   
    
	String executive=request.getParameter("executive");
	Map<String, Object> model = new HashMap<String, Object>();
	model.put("formulation",  employeeService.listFormulation());
	model.put("executive_list",  employeeService.ListChiefLogin());
	 if(!(todate1.equals("")) && !(fromdate1.equals("")))
		{
		    todate=s.parse(todate1);
		    fromdate=s.parse(fromdate1);
		    model.put("records",  employeeService.SerchByDate(todate, fromdate));
		}
		
	 else if(!(todate1.equals("")) && fromdate1.equals(""))
	{
		 todate=s.parse(todate1);
		 //fromdate=s.parse(fromdate1);
	    model.put("records",  employeeService.SerchBySingleDate(todate));
	}
	 else if(!(fromdate1.equals("")) && todate1.equals(""))
		{
			 //todate=s.parse(todate1);
			fromdate=s.parse(fromdate1);
		    model.put("records",  employeeService.SerchBySingleDate1(fromdate));
		}
	 else if(!(executive.equals("")))
		{
		    model.put("records",  employeeService.SerchByExecutive(executive));
		}
    return new ModelAndView("Chief_Lab_Record_Search_All_Next", model);
	
}



	
@RequestMapping(value = "/Chief_Preview.html", method = RequestMethod.GET)
public ModelAndView Chief_Preview(Records records) {
 
	Map<String, Object> model = new HashMap<String, Object>();
	model.put("records", employeeService.getRecords(records.getRid()));
	model.put("active", employeeService.List_Chief_Preview_Active(records.getRid()));
	model.put("composition", employeeService.List_Chief_Preview_Composition(records.getRid()));
	model.put("analyticalstudy", employeeService.List_Chief_Preview_Analytical_Studies(records.getRid()));
	model.put("atsstudies",  employeeService.List_Chief_Preview_ATS_Studies(records.getRid()));
	model.put("coldtest",  employeeService.List_Chief_Preview_Cold_Test(records.getRid()));
	model.put("coldtest",  employeeService.List_Chief_Preview_Cold_Test(records.getRid()));
	model.put("selflifestudy",  employeeService.listSelfLife_Studies(records.getRid()));
	model.put("scancopy",  employeeService.listScanned_Copies(records.getRid()));
	model.put("images",  employeeService.listImage(records.getRid()));
	model.put("recordcomment", employeeService.getRecord_Comment(records.getRid()));
	return new ModelAndView("Chief_Preview",model);
	
}
 
@RequestMapping(value = "/Executive_Preview.html", method = RequestMethod.GET)
public ModelAndView Executive_Preview(Records records) {
 
	Map<String, Object> model = new HashMap<String, Object>();
	model.put("records", employeeService.getRecords(records.getRid()));
	model.put("active", employeeService.List_Chief_Preview_Active(records.getRid()));
	model.put("composition", employeeService.List_Chief_Preview_Composition(records.getRid()));
	model.put("analyticalstudy", employeeService.List_Chief_Preview_Analytical_Studies(records.getRid()));
	model.put("atsstudies",  employeeService.List_Chief_Preview_ATS_Studies(records.getRid()));
	model.put("coldtest",  employeeService.List_Chief_Preview_Cold_Test(records.getRid()));
	model.put("coldtest",  employeeService.List_Chief_Preview_Cold_Test(records.getRid()));
	model.put("selflifestudy",  employeeService.listSelfLife_Studies(records.getRid()));
	model.put("scancopy",  employeeService.listScanned_Copies(records.getRid()));
	model.put("images",  employeeService.listImage(records.getRid()));
	model.put("recordcomment", employeeService.getRecord_Comment(records.getRid()));
	return new ModelAndView("Executive_Preview",model);
	
}
@RequestMapping(value = "/Record_Comment_Chief.html", method = RequestMethod.GET)
public ModelAndView Record_Comment_Chief(HttpSession session, @ModelAttribute("command") Records records, recordcomment recordcomment) {
 String username= (String) session.getAttribute("UserName");
	System.out.println("username:"+username);
	Map<String, Object> model = new HashMap<String, Object>();
	model.put("records", employeeService.getRecords(records.getRid()));
	model.put("recordcomment", employeeService.getRecord_Comment(records.getRid()));

	return new ModelAndView("Record_Comment_Chief",model);
	
} 
	
@RequestMapping(value = "/Record_Comment_Executive.html", method = RequestMethod.GET)
public ModelAndView Record_Comment_Executive(HttpSession session, @ModelAttribute("command") Records records, recordcomment recordcomment) {
	 String username= (String) session.getAttribute("UserName");
		System.out.println("username:"+username);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("recordcomment", employeeService.getRecord_Comment(records.getRid()));
	return new ModelAndView("Record_Comment_Executive", model);
	
}

@RequestMapping(value = "/Record_Comment.html", method = RequestMethod.GET)
public ModelAndView Record_Comment(@ModelAttribute("command") Records records, recordcomment recordcomment) {

	Map<String, Object> model = new HashMap<String, Object>();
	model.put("records", employeeService.getRecords(records.getRid()));
	model.put("recordcomment", employeeService.getRecord_Comment(records.getRid()));
	return new ModelAndView("Record_Comment",model);
	
}
@RequestMapping(value = "/SaveRecord_Comment.html", method = RequestMethod.POST)
	public ModelAndView SaveRecord_Comment(@ModelAttribute("command") Records records, recordcomment recordcomment) {
	    employeeService.addRecord_Comment(recordcomment);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("recordcomment", employeeService.getRecord_Comment(records.getRid()));
		return new ModelAndView("Record_Comment",model);
		
	}
@RequestMapping(value = "/DeleteRecord_Comment.html", method = RequestMethod.GET)
public ModelAndView DeleteRecord_Comment(@ModelAttribute("command") Records records, recordcomment recordcomment) {
    employeeService.DeleteRecord_Comment(recordcomment);
	Map<String, Object> model = new HashMap<String, Object>();
	model.put("records", employeeService.getRecords(records.getRid()));
	model.put("recordcomment", employeeService.getRecord_Comment(records.getRid()));
	return new ModelAndView("Record_Comment",model);
	
}

@RequestMapping(value = "/Chief_Lab_Record_Search_All.html", method = RequestMethod.GET)
public ModelAndView Chief_Lab_Record_Search_All(HttpServletRequest request, HttpSession session) {
	
	String offset = (String)request.getParameter("offSet");
	// result is number of record displayed on each page
	int result = 10;
	// size is the total number of record present in DB
	int size ;
	List<Integer> pageList = new ArrayList<Integer>();
	List<Records> recordsList ;
	/*in the beginning we set page number zero
	*/	if(offset!=null){
			int offsetreal = Integer.parseInt(offset);
			offsetreal = offsetreal*10;
			recordsList = employeeService.getRecordsList(result,offsetreal);
			 
		}else{
			recordsList = employeeService.getRecordsList(result,0);
			size = employeeService.getRecords_Size();
			/*if total record are divisible by 10 then we set page list 
			 * size one less than total size to avoid empty last page i.e if total record are 1220 then page list
			 *  size will be 121 because here we are taking page list size from 0-121 which is 122 pages*/
			if((size%result) == 0){
			session.setAttribute("size", (size/10)-1);
			}else{
				session.setAttribute("size", size/10);
			}
		}
	System.out.println(session.getAttribute("size").toString());
	/*when user click on any page number then this part will be executed. 
	 * else part will be executed on load i.e first time on page*/
	if(offset!=null){
		int listsize = Integer.parseInt(session.getAttribute("size").toString());
		if(Integer.parseInt(offset) <6){
			if(listsize>=10){
				for(int i= 1; i<=9;i++){
					pageList.add(i);
				}
			}else{
				for(int i= 1; i<=listsize;i++){
					pageList.add(i);
				}
			}

		}else{
			if(listsize >= 10 && Integer.parseInt(offset)-5 >0){
				List<Integer> temp = new ArrayList<Integer>(); 
				for(int i=Integer.parseInt(offset);i>Integer.parseInt(offset)-5;i--){
					temp.add(i);
				}
				for(int i=temp.size()-1;i>=0;i--){
					pageList.add(temp.get(i));
				}
			}
			if(listsize >= 10 && Integer.parseInt(offset)+5<listsize){
				for(int i=Integer.parseInt(offset)+1;i<Integer.parseInt(offset)+5;i++){
					pageList.add(i);
				}
			}else if(listsize >= 10){
				for(int i=Integer.parseInt(offset)+1;i<listsize;i++){
					pageList.add(i);
				}
			}
		}
	}else{
		int listsize = Integer.parseInt(session.getAttribute("size").toString());
		if(listsize>=10){
			for(int i= 1; i<=10;i++){
				pageList.add(i);
			}
		}else{
			for(int i= 1; i<=listsize;i++){
				pageList.add(i);
			}
		}
	}
	
	session.setAttribute("pageList", pageList);
	
	Map<String, Object> model = new HashMap<String, Object>();		
	model.put("recordsList", recordsList);
	model.put("formulation",  employeeService.listFormulation());
	model.put("executive_list",  employeeService.ListChiefLogin());

	return new ModelAndView("Chief_Lab_Record_Search_All", model);
	
}
	
	
	
	
	
	
	
	
	
	
	
	
	/* Dipali */
	
	
	
	
	@RequestMapping(value = "loginform", method = RequestMethod.POST)
	public ModelAndView doLogin(@ModelAttribute("command") chief_login cl,HttpServletRequest request,HttpSession session) {
		
		System.out.println(cl.getUserName());
		System.out.println(cl.getPassword());
		List<chief_login> list = employeeService.listChief();
		int i =0;
		String Designation="Chief Manager";
		String Designation1="Executive Manager";
		String Designation2="Admin";
		for(chief_login bean:list)
		{
			System.out.println("in for...." + ++i);
			
			if(cl.getUserName().equals(bean.getUserName()) && cl.getPassword().equals(bean.getPassword()) && bean.getDesignation().equalsIgnoreCase(Designation))
			{
				
				 request.getSession().setAttribute("UserName", cl.getUserName());
				        //modelAndView.addObject("UserName", cl.getUserName());
				System.out.println("in if....");
				// modelAndView.setViewName("ATSRemender");
				
				System.out.println("To Execute Remainder");
				 HttpSession session1=request.getSession();
				 
				 System.out.println("Session is"+session1);
				 
				 session1.getAttribute("UserName");
				
				Map<String, Object> model = new HashMap<String, Object>();
				String username= cl.getUserName();
				
				session=request.getSession();
				
				String userid = (String) session.getAttribute("UserName");
				
				System.out.println("UserName is 1 :"+userid);
				
				//String username1=session.getAttribute("username").toString();
				
				System.out.println("UserName is"+username);
				
				List<Records> l=employeeService.listRemainder1(userid);
				
				
                for(Records m:l)
                {
                	System.out.println("Reference no is "+m.getReferance_no());
                	System.out.println("Remendar is"+m.getReminder_date());
                }
				
				
				model.put("remainder",   employeeService.listRemainder1(userid));
				
				
				
				 return new ModelAndView("ATSRemender",model); 
			}
			if(cl.getUserName().equals(bean.getUserName()) && cl.getPassword().equals(bean.getPassword()) && bean.getDesignation().equalsIgnoreCase(Designation1))
			{
				
				 request.getSession().setAttribute("UserName", cl.getUserName());
				        //modelAndView.addObject("UserName", cl.getUserName());
				System.out.println("in if....");
System.out.println("To Execute Remainder");
				
				Map<String, Object> model = new HashMap<String, Object>();
				
				String username= cl.getUserName();
				System.out.println("username:"+username);
					//Map<String, Object> model = new HashMap<String, Object>();
					//model.put("formulation",  employeeService.listFormulation());
					//model.put("chiefrecord",  employeeService.listChiefRecord(username));
				
				System.out.println("To Execute Remainder");
				
				
				model.put("remainder",  employeeService.listRemainder1(username));
				
				
				
				
				 return new ModelAndView("Executive_ATSRemender",model); 
			}
			if(cl.getUserName().equals(bean.getUserName()) && cl.getPassword().equals(bean.getPassword()) && bean.getDesignation().equalsIgnoreCase(Designation2))
			{
				
				 request.getSession().setAttribute("UserName", cl.getUserName());
				        //modelAndView.addObject("UserName", cl.getUserName());
				System.out.println("in if....");
				// modelAndView.setViewName("ATSRemender");
				 return new ModelAndView("Header_Admin"); 
			}
			
			
		}
		
		return new ModelAndView("ChiefManagerLogin");
	}

	
	@RequestMapping(value="/Registration" , method= RequestMethod.GET)
	public ModelAndView saveManager() {
		return new ModelAndView("New_Manager");
	}
	
	
	
	@RequestMapping(value="/Registration_Admin" , method= RequestMethod.GET)
	public ModelAndView saveAdmin() {
		return new ModelAndView("NewAdmin_Inside");
	}
	@RequestMapping(value="/saveManagerAdmin",method = RequestMethod.POST)
	public ModelAndView addManager1(@Valid @ModelAttribute("command")chief_login managerBean, BindingResult result  ) 
	{
		System.out.println("in addManager");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		   
	    Calendar calobj = Calendar.getInstance();
	    System.out.println(df.format(calobj.getTime()));
		String regDate=df.format(calobj.getTime());
		
		String UserName=managerBean.getUserName();
		String password=managerBean.getPassword();
		String email1=managerBean.getEmailId();
		System.out.println("registraion date="+regDate.toString());
		List<chief_login> list = employeeService.listChief();
		int i =0;
		String Designation="Chief-Manager";
		/*for(chief_login bean:list)
		{
			System.out.println("in for...." + ++i);
			
			if(UserName.equals(bean.getUserName()) && email1.equals(bean.getEmailId()))
			{
				return new ModelAndView("New_Manager");
			}
		}*/
		try
		{
			managerBean.setRegDate(df.parse(regDate));
			
		 employeeService.addManager(managerBean);
		
		}
		catch(Exception ex)
		{
		
			return new ModelAndView("NewManager_Error");
       
		}
 
		System.out.println("For Sending Email");
		
		
		
		String subject="UserName and password";
		
		String str ="Dear"+ UserName+",Your UserName is  "+UserName+" and password is ";
		String message1=password+" thank you";
		String message=str.concat(message1);
		//String str=message+message1;
		// prints debug info
		//String message ="Dear User,Your password is  reset and your new password is "+ password+". Thank you.";
		
		// prints debug info
		System.out.println("To: " + email1);
		System.out.println("Subject: " + subject);
		System.out.println("Message: " + message);
		
		// creates a simple e-mail object
		SimpleMailMessage email = new SimpleMailMessage();
		
		
		email.setTo(email1);
		email.setSubject(subject);
		email.setText(message);
		
		

      mailSender.send(email);
      System.out.println("Email send succefully");
		
	
		if (result.hasErrors()) {
			return new ModelAndView("New_Manager");
        }
 
        
		return new ModelAndView("addSucess_Admin");
	}
	
	////=============================End AdminRegistraion=======================================
	//======================ExeceutiveRegistraion================================
	@RequestMapping(value="/Executive_Registration" , method= RequestMethod.GET)
	public ModelAndView saveManager1() {
		return new ModelAndView("New_Manager_Exceutive");
	}
	
	@RequestMapping(value="/Executive_saveManagerAdmin",method = RequestMethod.POST)
	public ModelAndView addManager_executive(@Valid @ModelAttribute("command")chief_login managerBean, BindingResult result  ) 
	{
		System.out.println("in addManager");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		   
	    Calendar calobj = Calendar.getInstance();
	    System.out.println(df.format(calobj.getTime()));
		String regDate=df.format(calobj.getTime());
		
		String UserName=managerBean.getUserName();
		String password=managerBean.getPassword();
		String email1=managerBean.getEmailId();
		System.out.println("registraion date="+regDate.toString());
		List<chief_login> list = employeeService.listChief();
		int i =0;
		String Designation="Chief-Manager";
		/*for(chief_login bean:list)
		{
			System.out.println("in for...." + ++i);
			
			if(UserName.equals(bean.getUserName()) && email1.equals(bean.getEmailId()))
			{
				return new ModelAndView("New_Manager");
			}
		}*/
		try
		{
			managerBean.setRegDate(df.parse(regDate));
			
		 employeeService.addManager(managerBean);
		
		}
		catch(Exception ex)
		{
		
			return new ModelAndView("NewManager_Error");
       
		}
 
		System.out.println("For Sending Email");
		
		
		
		String subject="UserName and password";
		
		String str ="Dear"+ UserName+",Your UserName is  "+UserName+" and password is ";
		String message1=password+" thank you";
		String message=str.concat(message1);
		//String str=message+message1;
		// prints debug info
		//String message ="Dear User,Your password is  reset and your new password is "+ password+". Thank you.";
		
		// prints debug info
		System.out.println("To: " + email1);
		System.out.println("Subject: " + subject);
		System.out.println("Message: " + message);
		
		// creates a simple e-mail object
		SimpleMailMessage email = new SimpleMailMessage();
		
		
		email.setTo(email1);
		email.setSubject(subject);
		email.setText(message);
		
		

      mailSender.send(email);
      System.out.println("Email send succefully");
		
	
		if (result.hasErrors()) {
			return new ModelAndView("New_Manager");
        }
 
        
		return new ModelAndView("addSucess");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//==========================================end executive registration=============================
	
	
	
	
	
	@RequestMapping(value="/saveManager",method = RequestMethod.POST)
	public ModelAndView addManager(@Valid @ModelAttribute("command")chief_login managerBean, BindingResult result  ) 
	{
		System.out.println("in addManager");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		   
	    Calendar calobj = Calendar.getInstance();
	    System.out.println(df.format(calobj.getTime()));
		String regDate=df.format(calobj.getTime());
		
		String UserName=managerBean.getUserName();
		String password=managerBean.getPassword();
		String email1=managerBean.getEmailId();
		System.out.println("registraion date="+regDate.toString());
		List<chief_login> list = employeeService.listChief();
		int i =0;
		String Designation="Chief-Manager";
		/*for(chief_login bean:list)
		{
			System.out.println("in for...." + ++i);
			
			if(UserName.equals(bean.getUserName()) && email1.equals(bean.getEmailId()))
			{
				return new ModelAndView("New_Manager");
			}
		}*/
		try
		{
			managerBean.setRegDate(df.parse(regDate));
			
		 employeeService.addManager(managerBean);
		
		}
		catch(Exception ex)
		{
		
			return new ModelAndView("NewManager_Error");
       
		}
 
		System.out.println("For Sending Email");
		
		
		
		String subject="UserName and password";
		
		String str ="Dear"+ UserName+",Your UserName is  "+UserName+" and password is ";
		String message1=password+" thank you";
		String message=str.concat(message1);
		//String str=message+message1;
		// prints debug info
		//String message ="Dear User,Your password is  reset and your new password is "+ password+". Thank you.";
		
		// prints debug info
		System.out.println("To: " + email1);
		System.out.println("Subject: " + subject);
		System.out.println("Message: " + message);
		
		// creates a simple e-mail object
		SimpleMailMessage email = new SimpleMailMessage();
		
		
		email.setTo(email1);
		email.setSubject(subject);
		email.setText(message);
		
		

      mailSender.send(email);
      System.out.println("Email send succefully");
		
		
		
		
		
		if (result.hasErrors()) {
			return new ModelAndView("New_Manager");
        }
 
        
		return new ModelAndView("addSucess");
	}
	public ModelAndView addChief(@ModelAttribute("command") chief_login Bean)
	{
		System.out.println("in addManager");
		employeeService.addchief(Bean);
		
		
		
		return new ModelAndView("Header");
	}
	
	@RequestMapping(value="/Logout" , method= RequestMethod.GET)
	public ModelAndView Logout(HttpServletRequest request) {
		
		  
		  String UserName=null;
		  HttpSession session1=request.getSession();
		  if(session1!=null)
		  {  
		   UserName=session1.getAttribute("UserName").toString();
		  System.out.println(UserName);
		  session1.removeAttribute("UserName");
		  session1.invalidate();
		  System.out.println("Logout sucessfully");
		  }
		 
		return new ModelAndView("ChiefManagerLogin");
	}
	@RequestMapping(value="/ForgotPassword" , method= RequestMethod.GET)
	public ModelAndView forgotPassword() {
		return new ModelAndView("ForgotPassword");
		
		
		
	}
	
	//===================================================================
	@RequestMapping(value = "/BacktoManager", method = RequestMethod.POST)
	public ModelAndView BacktoEmployee() {	
		
		Map<String, Object> model = new HashMap<String, Object>();
		return new ModelAndView("New_Manager", model);
	
	}
	
	
	
	
	///===================Admin Registration-------------------------------------------------
		@RequestMapping(value = "/NewRegistraion", method = RequestMethod.GET)
		public ModelAndView addNewEmployee() {	
			
			Map<String, Object> model = new HashMap<String, Object>();
			return new ModelAndView("New_Manager_Admin", model);
		
		}
		
		
	
	@RequestMapping(value="/forgotPassword", method=RequestMethod.POST)
	public ModelAndView forgotPassword1(@ModelAttribute("command") chief_login cpwd,HttpServletRequest request)
	{   System.out.println("In Forgot password ");
	
	
	String recipientAddress = cpwd.getEmailId();;
	String username=cpwd.getUserName();
	
	List<chief_login> list = employeeService.listChief();
	int i =0;
	for(chief_login chief:list)
	{ 
		System.out.println();
		System.out.println("in for...." + ++i);
		///System.out.println(cpwd.getUserName());
		if(username.equals(chief.getUserName()))
		{
	           String uname=chief.getUserName();
	           System.out.println("uname From database="+uname);
			
			String subject ="Reset password";
			StringBuilder sb = new StringBuilder(username);
			Random random = new Random();
			int randeom_no=random.nextInt();
			
			String password = "S"+randeom_no;
			String newpassword=username.concat(password);
			String message ="Dear User,Your password is  reset and your new password is "+newpassword+". Thank you.";
			
			// prints debug info
			System.out.println("To: " + recipientAddress);
			System.out.println("Subject: " + subject);
			System.out.println("Message: " + message);
			
			// creates a simple e-mail object
			SimpleMailMessage email = new SimpleMailMessage();
			email.setTo(recipientAddress);
			email.setSubject(subject);
			email.setText(message);
			
	      mailSender.send(email);
	      chief.setPassword(newpassword);
	      employeeService.addchief(chief);
		    System.out.println(" password change..");
	      return new ModelAndView("PasswordSucess");
		}
		
	}
	 return new ModelAndView("ChiefManagerLogin");
	
		
		}
	

	@RequestMapping(value="/ChangePassword" , method= RequestMethod.GET)
	public ModelAndView changePassword() {
		return new ModelAndView("ChangePassword");
	}
	
	

	@RequestMapping(value="/Chief_ChangePassword" , method= RequestMethod.GET)
	public ModelAndView changePassword1() {
		return new ModelAndView("Chief_ChangePassword");
	}
	
	
	
	
	@RequestMapping(value="/changePassword", method=RequestMethod.POST)
	public ModelAndView changePassword1(@ModelAttribute("command") ChangePassword cpwd,HttpServletRequest request)
	{   System.out.println("In Change password ");
	
		System.out.println(cpwd.getUserName());
		System.out.println(cpwd.getOldPassword());
			System.out.println(cpwd.getNewPassword());
			System.out.println(cpwd.getCnfPassword());
		
			String new_password=cpwd.getNewPassword();
			if(cpwd.getNewPassword().equals(cpwd.getCnfPassword()))
			{
				
				
				List<chief_login> list = employeeService.listChief();
				int i =0;
				for(chief_login chief:list)
				{
					System.out.println("in for...." + ++i);
					if(cpwd.getUserName().equals(chief.getUserName()))
					{
						if(cpwd.getOldPassword().equals(chief.getPassword()))
						{
							chief.setPassword(cpwd.getNewPassword());
							//chief.getUserName();
							//chief.getId();
						   employeeService.addchief(chief);
						    System.out.println(" password change..");
						    
						
					   }
			
			
		      }
			}
		}
	  
		return new ModelAndView("addSucessPassword");
	}
	@RequestMapping(value="/Chief_changePassword", method=RequestMethod.POST)
	public ModelAndView changePassword11(@ModelAttribute("command") ChangePassword cpwd,HttpServletRequest request)
	{   System.out.println("In Change password ");
	
		System.out.println(cpwd.getUserName());
		System.out.println(cpwd.getOldPassword());
			System.out.println(cpwd.getNewPassword());
			System.out.println(cpwd.getCnfPassword());
		
			String new_password=cpwd.getNewPassword();
			if(cpwd.getNewPassword().equals(cpwd.getCnfPassword()))
			{
				
				
				List<chief_login> list = employeeService.listChief();
				int i =0;
				for(chief_login chief:list)
				{
					System.out.println("in for...." + ++i);
					if(cpwd.getUserName().equals(chief.getUserName()))
					{
						if(cpwd.getOldPassword().equals(chief.getPassword()))
						{
							chief.setPassword(cpwd.getNewPassword());
							//chief.getUserName();
							//chief.getId();
						   employeeService.addchief(chief);
						    System.out.println(" password change..");
						    
						
					   }			
		      }
			}
		}	  
		return new ModelAndView("addsucesspassword_Chief");
	}
	
 /////////////////////////////Exceutive-Manager Pages////////////////////////////////////////////////
	@RequestMapping(value="/ExcutiveRecordRegistration", method = RequestMethod.GET)
	public ModelAndView listFormulation2() {
		String Designation="Chief Manager";
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("formulation",  employeeService.listFormulation());
		
		model.put("executive_list",  employeeService.ListChiefLogin());
		model.put("chief_list", employeeService.getEmployeeList(Designation));
		return new ModelAndView("Executive_Record_Registration", model);
	}
	@RequestMapping(value = "/SaveRecord_Excutive.html", method = RequestMethod.POST)
	public ModelAndView saveFormulation_1(@ModelAttribute("command1") Records records, 
			BindingResult result,HttpServletRequest request, HttpSession session)throws Exception {		
		/*employeeService.addRecords(records);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("formulation",  employeeService.listFormulation());*/
		System.out.println("Inside Executive Registration in save");
		if(request.getParameter("referance_no").equals("null")){
			String ref_no=records.getDate();
			System.out.println("Ref No is in controller"+ref_no);		
			Connection con  = DbUtil.getConnection();
	        //  alert("Pramod");
	    	Statement st = con.createStatement();
	    	String sql = ("SELECT * FROM records where date='"+ref_no+"';");
	    	ResultSet rs = st.executeQuery(sql);
	    	
	    	int count=1;
	    	while(rs.next()) {
	    		count++;
	    		
	    	}
	    	
	    	System.out.println("Count is"+count);
			String reference_no="Ver/"+ref_no+"/"+count;
			records.setReferance_no(reference_no);
			}
		employeeService.addRecords(records);
		String Designation="Executive Manager";
		session.setAttribute("rid", records.getRid());
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("formulation",  employeeService.listFormulation());
		model.put("executive_list",  employeeService.ListChiefLogin());
		return new ModelAndView("Executive_Lab_Record_Entry", model);
	}
	@RequestMapping(value = "/ExecutiveLabRecord", method = RequestMethod.GET)
	public ModelAndView ExecutiveLabRecordEntry(HttpServletRequest request, HttpSession session) {
		/*Map<String, Object> model = new HashMap<String, Object>();
		model.put("records",  prepareListofBean2(employeeService.listRecords()));
		return new ModelAndView("Chief_Record_List", model);*/
		String username=(String) session.getAttribute("UserName");
		System.out.println("Usernamein executive lab record="+username);
		//offset is page number
				String offset = (String)request.getParameter("offSet");
				// result is number of record displayed on each page
				int result = 10;
				// size is the total number of record present in DB
				int size ;
				List<Integer> pageList = new ArrayList<Integer>();
				List<Records> recordsList ;
				/*in the beginning we set page number zero
				*/	if(offset!=null){
						int offsetreal = Integer.parseInt(offset);
						offsetreal = offsetreal*10;
						recordsList = employeeService.getRecordsList1(result,offsetreal,username);
						 
					}else{
						recordsList = employeeService.getRecordsList1(result,0,username);
						size = employeeService.getRecords_Size();
						/*if total record are divisible by 10 then we set page list 
						 * size one less than total size to avoid empty last page i.e if total record are 1220 then page list
						 *  size will be 121 because here we are taking page list size from 0-121 which is 122 pages*/
						if((size%result) == 0){
						session.setAttribute("size", (size/10)-1);
						}else{
							session.setAttribute("size", size/10);
						}
					}
				System.out.println(session.getAttribute("size").toString());
				/*when user click on any page number then this part will be executed. 
				 * else part will be executed on load i.e first time on page*/
				if(offset!=null){
					int listsize = Integer.parseInt(session.getAttribute("size").toString());
					if(Integer.parseInt(offset) <6){
						if(listsize>=10){
							for(int i= 1; i<=9;i++){
								pageList.add(i);
							}
						}else{
							for(int i= 1; i<=listsize;i++){
								pageList.add(i);
							}
						}

					}else{
						if(listsize >= 10 && Integer.parseInt(offset)-5 >0){
							List<Integer> temp = new ArrayList<Integer>(); 
							for(int i=Integer.parseInt(offset);i>Integer.parseInt(offset)-5;i--){
								temp.add(i);
							}
							for(int i=temp.size()-1;i>=0;i--){
								pageList.add(temp.get(i));
							}
						}
						if(listsize >= 10 && Integer.parseInt(offset)+5<listsize){
							for(int i=Integer.parseInt(offset)+1;i<Integer.parseInt(offset)+5;i++){
								pageList.add(i);
							}
						}else if(listsize >= 10){
							for(int i=Integer.parseInt(offset)+1;i<listsize;i++){
								pageList.add(i);
							}
						}
					}
				}else{
					int listsize = Integer.parseInt(session.getAttribute("size").toString());
					if(listsize>=10){
						for(int i= 1; i<=10;i++){
							pageList.add(i);
						}
					}else{
						for(int i= 1; i<=listsize;i++){
							pageList.add(i);
						}
					}
				}
				
				session.setAttribute("pageList", pageList);
				
				Map<String, Object> model = new HashMap<String, Object>();		
				model.put("recordsList", recordsList);
				
				return new ModelAndView("Executive_Record_List", model);	
		
	}
    
	//==================================

/*Executive ATS_SelfLife_Reminders Start*/
	/*@RequestMapping(value = "/Executive_ATS_SelfLife_Reminders.html", method = RequestMethod.GET)
	public ModelAndView ATS_SelfLife_Reminders1(@ModelAttribute("command1") Records records, 
			BindingResult result){
		
		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("records",  employeeService.listRecords());
		return new ModelAndView("Executive_Lab_Record_Entry", model);
	}*/
@RequestMapping(value = "/Executive_ATS_SelfLife_Reminders.html", method = RequestMethod.GET)
public ModelAndView ATS_SelfLife_Reminders_executive(@ModelAttribute("command") Records records, BindingResult result){
	Map<String, Object> model = new HashMap<String, Object>();
	model.put("records", employeeService.getRecords(records.getRid()));
	model.put("recordslist", employeeService.ListRecords());
	return new ModelAndView("Executive_ATS_SelfLife_Reminders", model);	
}	

@RequestMapping(value = "/SaveATS_SelfLife_Reminders_Executive.html", method = RequestMethod.POST)
public ModelAndView Executive_SaveATS_SelfLife_Reminders(@ModelAttribute("command")Records records, 
		BindingResult result) {		
	employeeService.addRecords(records);
	Map<String, Object> model = new HashMap<String, Object>();
	model.put("records", employeeService.getRecords(records.getRid()));
	model.put("recordslist", employeeService.ListRecords());
	return new ModelAndView("Executive_ATS_SelfLife_Reminders", model);
}
/*@RequestMapping(value = "/DeleteATS_SelfLife_Reminders.html", method = RequestMethod.GET)
public ModelAndView deleteATS_SelfLife_Reminders(@ModelAttribute("command")Records records, 
		BindingResult result) {
	employeeService.deleteRecords(records);
	Map<String, Object> model = new HashMap<String, Object>();
	model.put("active",  employeeService.listActive(records.getRid()));
	return new ModelAndView("Active_Details", model);
}*/	
@RequestMapping(value = "/Executive_EditATS_SelfLife_Reminders.html", method = RequestMethod.GET)
public ModelAndView editATS_SelfLife_Reminders_Executive(@ModelAttribute("command")Records records, 
		BindingResult result) {		
	Map<String, Object> model = new HashMap<String, Object>();
	model.put("records", employeeService.getRecords(records.getRid()));
	return new ModelAndView("Executive_EditATS_SelfLife_Reminders", model);
}
/*ATS_SelfLife_Reminders End*/
	
	///==============================================================
	
		@RequestMapping(value = "/Executive_SaveRecord.html", method = RequestMethod.POST)
		public ModelAndView executive_saveFormulation(@ModelAttribute("command1") Records records,
				BindingResult result) {	
			employeeService.addRecords(records);
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("records", employeeService.getRecords(records.getRid()));
			model.put("formulation",  employeeService.listFormulation());
			return new ModelAndView("Executive_Lab_Record_Entry", model);
		}
		@RequestMapping(value = "/Executive_DeleteRecords.html", method = RequestMethod.GET)
		public ModelAndView executive_DeleteRecords(@ModelAttribute("command")  Records records,
				BindingResult result,HttpServletRequest request, HttpSession session)
		{
			System.out.println("record id = " + records.getRid());
			employeeService.deleteRecord(records);
			Map<String, Object> model = new HashMap<String, Object>(); 
			
			//model.put("records",  employeeService.listRecords());
			return ExecutiveLabRecordEntry(request, session);
			//return new ModelAndView("Executive_Lab_Record_Entry", model);
		}
		
		@RequestMapping(value = "/Execuitve_EditRecords.html", method = RequestMethod.GET)
		public ModelAndView executive_EditRecords(@ModelAttribute("command")   Records records,
				BindingResult result) {
			System.out.println("RID="+records.getRid());
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("records", employeeService.getRecords(records.getRid()));
			model.put("formulation",  employeeService.listFormulation());
			return new ModelAndView("Execuitve_EditRecords", model);
		}
		///////////////////////////////////////////////////////////////////////////////////////////
	
	
	/////Executive LabRecord Search

/*	@RequestMapping(value = "/ExecutiveLabRecordSearch.html", method = RequestMethod.GET)
	public ModelAndView ExecutiveLabRecordSearch() {
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("formulation",  employeeService.listFormulation());
			return new ModelAndView("ExecutiveLabRecordSearch", model);
		
	}*/
	@RequestMapping(value = "/ExecutiveLabRecordSearch.html", method = RequestMethod.GET)
	public ModelAndView ExecutiveLabRecordSearch(HttpSession session) {
		 String username= (String) session.getAttribute("UserName");
			System.out.println("username:"+username);
				Map<String, Object> model = new HashMap<String, Object>();
				model.put("formulation",  employeeService.listFormulation());
				model.put("chiefrecord",  employeeService.listChiefRecord(username));
			return new ModelAndView("ExecutiveLabRecordSearch", model);
		
	}
	@RequestMapping(value = "/ExecutiveRecordSearch.html", method = RequestMethod.POST)
	public ModelAndView ExecutiveRecordSearch(HttpSession session, @RequestParam("formulation") String formulation,  @RequestParam("status") String status, @RequestParam("active") String active ) {
		//System.out.println("fromdate:"+fromdate);	
		//System.out.println("todate:"+todate);
		System.out.println("formulation:"+formulation);
		System.out.println("active:"+active);
		System.out.println("status:"+status);
		
		String username= (String) session.getAttribute("UserName"); 
		System.out.println("username:"+username);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("formulation",  employeeService.listFormulation());
		if(formulation!="" && status!="" && active!="")
			{
			    model.put("records",  employeeService.ChiefRecordSerchBy_Fomulation_Status_Active(formulation, status, active, username));
			}
		else if(formulation!="" && status!="")
		{
		    model.put("records",  employeeService.ChiefRecordSerchBy_Fomulation_Status(formulation, status, username));
		}
		else if(formulation!="" && active!="")
		{
		    model.put("records",  employeeService.ChiefRecordSerchBy_Fomulation_Active(formulation, active, username));
		}
		else if(status!="" && active!="")
		{
		    model.put("records",  employeeService.ChiefRecordSerchBy_Status_Active(status, active, username));
		}
		
		else if(status !="")
		{
			model.put("records",  employeeService.ChiefRecordSerchByStatus(status, username));
		}
		else if(formulation !="")
		{
		    model.put("records",  employeeService.ChiefRecordSerchByFormulation(formulation, username));
		}
		else if(active != "" )
		{
		    model.put("records",  employeeService.ChiefRecordSerchByActive(active, username));
		}
		
		return new ModelAndView("ExecutiveRecordSearch", model);
		
	}
//  @RequestParam("todate") Date todate,  @RequestParam("fromdate") Date fromdate,  @RequestParam("active") String active
 @RequestMapping(value = "/Executive_RecordSearchByDate.html", method = RequestMethod.POST)
	public ModelAndView ExecutiveRecordSearchByActiveExecutiveRecordSearchByActive(HttpSession session, HttpServletRequest request) throws ParseException {
		Date fromdate=null;
		Date todate=null;
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("formulation",  employeeService.listFormulation());
		 String fromdate1=request.getParameter("fromdate");
		String todate1=request.getParameter("todate");
		SimpleDateFormat s= new SimpleDateFormat("yyyy/MM/dd");
		String username = (String) session.getAttribute("UserName"); 
		 
		 System.out.println("fromdate:"+fromdate1);	
			System.out.println("todate:"+todate1);
			if(!(todate1.equals("")) && fromdate1.equals(""))
			{
				todate=s.parse(todate1);
				model.put("records",  employeeService.SerchByDate1(todate, username));
			}
			if(!(fromdate1.equals("")) && todate1.equals(""))
			{
				fromdate=s.parse(fromdate1);
				model.put("records",  employeeService.SerchByDate1(fromdate, username));
			}
							
			if(!(todate1.equals("")) && !(fromdate1.equals("")))
			{
				todate=s.parse(todate1);
				fromdate=s.parse(fromdate1);
			    model.put("records",  employeeService.SerchByDate(todate, fromdate, username));
			}
		return new ModelAndView("ExecutiveRecordSearch", model);
		
	}
	
	
	
	
	//==============================search end===================================================
	@RequestMapping(value="/Executive_ChangePassword" , method= RequestMethod.GET)
	public ModelAndView changePassword_1() {
		return new ModelAndView("Executive_ChangePassword");
	}
	@RequestMapping(value="/executive_changePassword", method=RequestMethod.POST)
	public ModelAndView changePassword(@ModelAttribute("command") ChangePassword cpwd,HttpServletRequest request)
	{   System.out.println("In executive Change password ");
	
		System.out.println(cpwd.getUserName());
		System.out.println(cpwd.getOldPassword());
			System.out.println(cpwd.getNewPassword());
			System.out.println(cpwd.getCnfPassword());
		
			String new_password=cpwd.getNewPassword();
			if(cpwd.getNewPassword().equals(cpwd.getCnfPassword()))
			{
				
				
				List<chief_login> list = employeeService.listChief();
				int i =0;
				for(chief_login chief:list)
				{
					System.out.println("in for...." + ++i);
					if(cpwd.getUserName().equals(chief.getUserName()))
					{
						if(cpwd.getOldPassword().equals(chief.getPassword()))
						{
							chief.setPassword(cpwd.getNewPassword());
							//chief.getUserName();
							//chief.getId();
						   employeeService.addchief(chief);
						    System.out.println(" password change..");
						    
						
					   }
			
			
		      }
			}
		}
	  
	    
		
		
		return new ModelAndView("Executive_addSucessPassword");
	}
	
	
	///////=============Active Details strat for executive===========================///
	@RequestMapping(value = "/Executive_Active_Details.html", method = RequestMethod.GET)
	public ModelAndView Active_Details_1(@ModelAttribute("command") Records records, 
			BindingResult result)
	{
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("active",  employeeService.listActive(records.getRid()));
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Executive_Active_Details", model);		
	}	
	@RequestMapping(value = "/Executive_DeleteActive.html", method = RequestMethod.GET)
	public ModelAndView deleteActive__1(HttpSession session, HttpServletRequest request,Active active,Records records, 
			BindingResult result) {
		System.out.println("Active id="+active.getActive_id());
		System.out.println("record  id="+records.getRid());
		System.out.println("Active is="+active.getActive());
		int rid= (int) session.getAttribute("rid");
		employeeService.deleteActive(active);
		String activevalue =request.getParameter("active");
		employeeService.deleteAnalytical_Studies1(activevalue, rid );
		employeeService.deleteATS_Studies1(activevalue, rid);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("active",  employeeService.listActive(records.getRid()));
		return new ModelAndView("Active_Details", model);
	}	
	@RequestMapping(value = "/Executive_EditActive.html", method = RequestMethod.GET)
	public ModelAndView editActive_1(@ModelAttribute("command")   Active active,Records records, 
			BindingResult result) 
	{		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("active", employeeService.getActive(active.getActive_id()));
		model.put("active1",  employeeService.listActive(records.getRid()));
		return new ModelAndView("Executive_EditActive", model);
	}
	
	@RequestMapping(value = "/SaveActive_executive.html", method = RequestMethod.POST)
	public ModelAndView SaveActive_1(@ModelAttribute("command") Active active,Records records, 
			BindingResult result) 
	{	
		employeeService.addRecords(records);
		employeeService.addActive(active);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("active",  employeeService.listActive(records.getRid()));
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Executive_Active_Details", model);
	}
/*Composition details Start for Executive*/
	
	@RequestMapping(value = "/Executive_Composition_Details.html", method = RequestMethod.GET)
	public ModelAndView Composition_Details_1(@ModelAttribute("command") Records records, 
			BindingResult result){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("composition",  employeeService.listComposition(records.getRid()));
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Executive_Composition_Details", model);		
	}
	@RequestMapping(value = "/SaveComposition_Executive.html", method = RequestMethod.POST)
	public ModelAndView SaveComposition_1(@ModelAttribute("command") Composition composition,Records records, 
			BindingResult result) {		
		employeeService.addComposition(composition);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("composition",  employeeService.listComposition(records.getRid()));
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Executive_Composition_Details", model);
	}
	@RequestMapping(value = "/Executive_DeleteComposition.html", method = RequestMethod.GET)
	public ModelAndView deleteComposition_1(@ModelAttribute("command")  Composition composition,Records records, 
			BindingResult result) {		
		employeeService.deleteComposition(composition);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("active", null);
		model.put("composition",  employeeService.listComposition(records.getRid()));
		return new ModelAndView("Executive_Composition_Details", model);
	}	
	@RequestMapping(value = "/Executive_EditComposition.html", method = RequestMethod.GET)
	public ModelAndView editComposition_1(@ModelAttribute("command")   Composition composition,Records records, 
			BindingResult result) {		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("composition", employeeService.getComposition(composition.getComposition_id()));
		model.put("composition1",  employeeService.listComposition(records.getRid()));
		return new ModelAndView("Executive_EditComposition", model);
	}
/*Composition details End*/
/*Close_&_Return_Records Start*/
	
	@RequestMapping(value = "/Executive_Close_&_Return_Records.html", method = RequestMethod.GET)
	public ModelAndView CloseReturnRecords1(@ModelAttribute("command") Records records, 
			BindingResult result){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("formulation",  employeeService.listFormulation());
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Executive_Lab_Record_Entry", model);
		
	}
	
	
	
	
	
	
	

/*Close_&_Return_Records End*/
	/*Analytical Studies Start*/

	@RequestMapping(value = "/Executive_Analytical_Studies.html", method = RequestMethod.GET)
	public ModelAndView Analytical_Studies_1(@ModelAttribute("command") Records records, 
			BindingResult result){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("analyticalstudy",  employeeService.listAnalytical_Studies(records.getRid()));
		model.put("analysis",  employeeService.listAnalysis());
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("active",  employeeService.listActive(records.getRid()));
		return new ModelAndView("Executive_Analytical_Studies", model);
		
	}
	@RequestMapping(value = "/Executive_SaveAnalytical_Studies.html", method = RequestMethod.POST)
	public ModelAndView SaveAnalytical_Studies_1(HttpSession session, HttpServletRequest request, @ModelAttribute("command") analyticalstudy analyticalstudy,Records records, 
			BindingResult result) {
		
		employeeService.addAnalytical_Studies(analyticalstudy);
		/*analyticalstudy1 analyticalstudy11 = new analyticalstudy1();
		analyticalstudy1 analyticalstudy12 = new analyticalstudy1();//create the user entity object
	      
		analyticalstudy analyticalstudy1 = new analyticalstudy(); //create the first vehicle entity object
		
		
		//String active1=request.getParameter("Pramod123");
        analyticalstudy11.setActive("Bhushan"); 
        analyticalstudy11.setConcentration("Pramod");
        analyticalstudy12.setActive("Abc");
        analyticalstudy12.setConcentration("xyz");//Set the value to the user entity
        analyticalstudy1.getAnalyticalstudy().add(analyticalstudy11);
        analyticalstudy1.getAnalyticalstudy().add(analyticalstudy12);//add vehicle to the list of the vehicle
        SessionFactory sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory(); //create session factory object
        Session session1 = sessionFactory.openSession(); //create the session object
        session1.beginTransaction(); //start the transaction of the session object
      
        session1.save(analyticalstudy11); //saving the vehicle to the database
        session1.save(analyticalstudy12);
        session1.save(analyticalstudy1); //save the user to the database
        
        session1.getTransaction().commit(); */
		//System.out.println("Analytical_Id:"+analyticalstudy.getAnalyticalstudy_id());
		int Analyticalstudy_id = analyticalstudy.getAnalyticalstudy_id();
		int rid= (int) session.getAttribute("rid");
		 try {
			 Connection con  = DbUtil.getConnection(); 
		 PreparedStatement stmt1=con.prepareStatement("select * from active where rid="+session.getAttribute("rid"));
		 System.out.println("select * from active where rid="+session.getAttribute("rid"));
		 ResultSet rs1=stmt1.executeQuery();
		 while(rs1.next()){
			//request.getParameter(rs1.getString("active"));
			//System.out.println("Analytical_Id:"+analyticalstudy.getAnalyticalstudy_id());
			System.out.println("Active:"+request.getParameter(rs1.getString("active")));
			System.out.println("Concentration:"+request.getParameter("C"+rs1.getString("active"))); 
			
			employeeService.addAnalytical_Studies1(Analyticalstudy_id, request.getParameter(rs1.getString("active")), request.getParameter("C"+rs1.getString("active")), rid);
		 } 
		 } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		} 
      
       
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("analyticalstudy",  employeeService.listAnalytical_Studies(records.getRid()));
		model.put("analysis",  employeeService.listAnalysis());
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("active",  employeeService.listActive(records.getRid()));
		return new ModelAndView("Executive_Analytical_Studies", model);
	}
	
	@RequestMapping(value = "/Executive_UpdateAnalytical_Studies.html", method = RequestMethod.POST)
	public ModelAndView UpdateAnalytical_Studies1_1(HttpSession session, HttpServletRequest request, @ModelAttribute("command") analyticalstudy analyticalstudy,Records records, 
			BindingResult result) {
		
		employeeService.addAnalytical_Studies(analyticalstudy);
	
		//int Analyticalstudy_id = analyticalstudy.getAnalyticalstudy_id();
		int rid= (int) session.getAttribute("rid");
		 try {
			 Connection con  = DbUtil.getConnection(); 
		 PreparedStatement stmt1=con.prepareStatement("select * from analyticalstudy1 where rid="+session.getAttribute("rid")+" AND analyticalstudy_id="+analyticalstudy.getAnalyticalstudy_id());
		 System.out.println("select * from active where rid="+session.getAttribute("rid"));
		 ResultSet rs1=stmt1.executeQuery();
		 while(rs1.next()){
			//request.getParameter(rs1.getString("active"));
			 int Analyticalstudy_id =rs1.getInt("analyticalstudy1_id");
			System.out.println("Active:"+request.getParameter(rs1.getString("analytical_active")));
			System.out.println("Concentration:"+request.getParameter("C"+rs1.getString("analytical_active"))); 
			
			employeeService.updateAnalytical_Studies1(Analyticalstudy_id, request.getParameter(rs1.getString("analytical_active")), request.getParameter("C"+rs1.getString("analytical_active")), rid);
		 } 
		 } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		} 
      
       
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("analyticalstudy",  employeeService.listAnalytical_Studies(records.getRid()));
		model.put("analysis",  employeeService.listAnalysis());
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("active",  employeeService.listActive(records.getRid()));
		return new ModelAndView("Executive_Analytical_Studies", model);
	}
	@RequestMapping(value = "/Executive_DeleteAnalytical_Studies.html", method = RequestMethod.GET)
	public ModelAndView deleteAnalytical_Studies_1(@ModelAttribute("command")  analyticalstudy analyticalstudy,Records records,
			BindingResult result) {
		System.out.println("analyticalstudy id="+analyticalstudy.getAnalyticalstudy_id());
		employeeService.deleteAnalytical_Studies(analyticalstudy);
		employeeService.deleteAnalytical_Studies1(analyticalstudy.getAnalyticalstudy_id());
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("analyticalstudy", null);
		model.put("analysis",  employeeService.listAnalysis());
		model.put("analyticalstudy",  employeeService.listAnalytical_Studies(records.getRid()));
		model.put("active",  employeeService.listActive(records.getRid()));
		return new ModelAndView("Executive_Analytical_Studies", model);
	}		
	@RequestMapping(value = "/Executive_EditAnalytical_Studies.html", method = RequestMethod.GET)
	public ModelAndView editAnalytical_Studies_1(HttpSession session, HttpServletRequest request,@ModelAttribute("command")   analyticalstudy analyticalstudy,Records records,
			BindingResult result) {	
		session.setAttribute("analyticalstudy_id", analyticalstudy.getAnalyticalstudy_id());
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("active",  employeeService.listActive(records.getRid()));
		model.put("analyticalstudy", employeeService.getAnalytical_Studies(analyticalstudy.getAnalyticalstudy_id()));
		model.put("analysis",  employeeService.listAnalysis());
		model.put("analyticalstudy1",  employeeService.listAnalytical_Studies(records.getRid()));
		return new ModelAndView("Executive_EditAnalytical_Studies", model);
	}


/*Analytical Studies End*/

/*Scanned_Copies Start*/

	@RequestMapping(value = "/Executive_Scanned_Copies.html", method = RequestMethod.GET)
	public ModelAndView Scanned_Copies_1(@ModelAttribute("command") Records records, 
			BindingResult result){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("scancopy",  employeeService.listScanned_Copies(records.getRid()));				
					model.put("analysis",  employeeService.listAnalysis());
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Executive_Scanned_Copies", model);					
	}
	@RequestMapping(value = "/Executive_SaveScanned_Copies.html", method = RequestMethod.POST)
	public ModelAndView SaveScanned_Copies_1(@ModelAttribute("command") scancopy scancopyBean,Records records,BindingResult result,
			@RequestParam("file") MultipartFile file) {	
		System.out.println("File:" + file.getName());
		System.out.println("ContentType:" + file.getContentType());
		Map<String, Object> model = new HashMap<String, Object>();					
		/*try {
			Blob blob = Hibernate.createBlob(file.getInputStream());
			scancopyBean.setScanfile(blob);
			scancopyBean.setName(file.getOriginalFilename());
		} catch (IOException e) {
			e.printStackTrace();
										
		}	*/	
		employeeService.addScanned_Copies(scancopyBean);					
		model.put("scancopy",  employeeService.listScanned_Copies(records.getRid()));
		/*model.put("analysis",  employeeService.listAnalysis());*/
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Executive_Scanned_Copies", model);
	}
	@RequestMapping(value = "/Executive_DeleteScanned_Copies.html", method = RequestMethod.GET)
	public ModelAndView deleteScanned_Copies_1(@ModelAttribute("command")  scancopy scancopy,Records records,
			BindingResult result) {
		//System.out.println("Active id="+active.getActive_id());
		employeeService.deleteScanned_Copies(scancopy);
		Map<String, Object> model = new HashMap<String, Object>();
		
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("scancopy",  employeeService.listScanned_Copies(records.getRid()));
		return new ModelAndView("Executive_Scanned_Copies", model);
	}				
	@RequestMapping(value = "/Executive_EditScanned_Copies", method = RequestMethod.GET)
	public ModelAndView editScanned_Copies_1(@ModelAttribute("command") scancopy scancopy,Records records,
			BindingResult result) {
		System.out.println("scan id " + scancopy.getScan_id());
		System.out.println("records id " + records.getRid());
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("scancopy", employeeService.getScanned_Copies(scancopy.getScan_id()));
		model.put("records", employeeService.getRecords(records.getRid()));			
		model.put("scancopy1",  employeeService.listScanned_Copies(records.getRid()));
		return new ModelAndView("Executive_EditScanned_Copies", model);
	}
/*Scanned_Copies End*/
				
				/*Cold Test Start*/
	@RequestMapping(value = "/Executive_Cold_Test.html", method = RequestMethod.GET)
	public ModelAndView Cold_Test_1(@ModelAttribute("command") Records records, 
			BindingResult result){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("coldtest",  employeeService.listCold_Test(records.getRid()));
		model.put("analysis",  employeeService.listAnalysis());
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Executive_Cold_Test", model);
		
	}
	@RequestMapping(value = "/Executive_SaveCold_Test.html", method = RequestMethod.POST)
	public ModelAndView SaveCold_Test_1(@ModelAttribute("command") coldtest coldtest,Records records, 
			BindingResult result) {		
		employeeService.addCold_Test(coldtest);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("coldtest",  employeeService.listCold_Test(records.getRid()));
		model.put("analysis",  employeeService.listAnalysis());
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Executive_Cold_Test", model);
	}
	@RequestMapping(value = "/Executive_DeleteCold_Test.html", method = RequestMethod.GET)
	public ModelAndView deleteCold_Test_1(@ModelAttribute("command")  coldtest coldtest,Records records,
		BindingResult result) {
		//System.out.println("Active id="+active.getActive_id());
		System.out.println("Pramod Powar");
		employeeService.deleteCold_Test(coldtest);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("coldtest", null);
		model.put("analysis",  employeeService.listAnalysis());
		model.put("coldtest",  employeeService.listCold_Test(records.getRid()));
		model.put("records", employeeService.getRecords(records.getRid()));
		
		return new ModelAndView("Executive_Cold_Test", model);
	}
	
	@RequestMapping(value = "/Executive_EditCold_Test.html", method = RequestMethod.GET)
	public ModelAndView editCold_Test_1(@ModelAttribute("command")   coldtest coldtest,Records records,
			BindingResult result) {		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("coldtest", employeeService.getCold_Test(coldtest.getCold_id()));
		model.put("analysis",  employeeService.listAnalysis());
		model.put("coldtest1",  employeeService.listCold_Test(records.getRid()));
		return new ModelAndView("Executive_EditCold_Test", model);
	}

				/*======================Cold Test End=========================*/
	/*SelfLife_Studies Start for Executive*/

	@RequestMapping(value = "/Executive_SelfLife_Studies.html", method = RequestMethod.GET)
	public ModelAndView SelfLife_Studies_1(@ModelAttribute("command") Records records, 
			BindingResult result){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("selflifestudy",  employeeService.listSelfLife_Studies(records.getRid()));
		model.put("atslist",  employeeService.listATS());
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Executive_SelfLife_Studies", model);
		
	}
	@RequestMapping(value = "/Executive_SaveSelfLife_Studies.html", method = RequestMethod.POST)
	public ModelAndView SaveSelfLife_Studies_1(@ModelAttribute("command") selflifestudy selflifestudy,Records records, 
			BindingResult result) {
		
		employeeService.addSelfLife_Studies(selflifestudy);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("selflifestudy",  employeeService.listSelfLife_Studies(records.getRid()));
		model.put("atslist",  employeeService.listATS());
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Executive_SelfLife_Studies", model);
	}
	@RequestMapping(value = "/Executive_DeleteSelfLife_Studies.html", method = RequestMethod.GET)
	public ModelAndView deleteSelfLife_Studies_1(@ModelAttribute("command")  selflifestudy selflifestudy,Records records,
			BindingResult result) {
		//System.out.println("Active id="+active.getActive_id());
		employeeService.deleteSelfLife_Studies(selflifestudy);
		Map<String, Object> model = new HashMap<String, Object>();
		//model.put("selflifestudy", null);
		model.put("analysis",  employeeService.listAnalysis());
		model.put("selflifestudy",  employeeService.listSelfLife_Studies(records.getRid()));
		return new ModelAndView("Executive_SelfLife_Studies", model);
	}		
	@RequestMapping(value = "/Executive_EditSelfLife_Studies.html", method = RequestMethod.GET)
	public ModelAndView editSelfLife_Studies_1(@ModelAttribute("command")   selflifestudy selflifestudy,Records records,
			BindingResult result) {			
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("selflifestudy", employeeService.getSelfLife_Studies(selflifestudy.getSelflife_id()));
		model.put("analysis",  employeeService.listAnalysis());
		model.put("selflifestudy1",  employeeService.listSelfLife_Studies(records.getRid()));
		return new ModelAndView("Executive_EditSelfLife_Studies", model);
	}

	/*SelfLife_Studies End*/
	
	
	
	
					/*ATS Studies Start*/
					
	/*ATS Studies Start*/
	
	@RequestMapping(value = "/Executive_ATS_Studies.html", method = RequestMethod.GET)
	public ModelAndView ATS_Studies_1(@ModelAttribute("command") Records records, 
			BindingResult result){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("atsstudies",  employeeService.listATS_Studies(records.getRid()));
		model.put("analysis",  employeeService.listAnalysis());
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("atslist",  employeeService.listATS());
		model.put("active",  employeeService.listActive(records.getRid()));
		return new ModelAndView("Executive_ATS_Studies", model);
		
	}
	@RequestMapping(value = "/Executive_SaveATS_Studies.html", method = RequestMethod.POST)
	public ModelAndView SaveATS_Studies_1(HttpSession session, HttpServletRequest request, @ModelAttribute("command") atsstudies atsstudies,Records records, 
			BindingResult result) {		
employeeService.addATS_Studies(atsstudies);
		
		int ats_id = atsstudies.getAts_id();
		int rid= (int) session.getAttribute("rid");
		 try {
		Connection con  = DbUtil.getConnection();	 
		 PreparedStatement stmt1=con.prepareStatement("select * from active where rid="+session.getAttribute("rid"));
		 System.out.println("select * from active where rid="+session.getAttribute("rid"));
		 ResultSet rs1=stmt1.executeQuery();
		 while(rs1.next()){
			//request.getParameter(rs1.getString("active"));
			//System.out.println("Analytical_Id:"+analyticalstudy.getAnalyticalstudy_id());
			System.out.println("Active:"+request.getParameter(rs1.getString("active")));
			System.out.println("Concentration:"+request.getParameter("C"+rs1.getString("active"))); 
			
			employeeService.addATS_Studies1(ats_id, request.getParameter(rs1.getString("active")), request.getParameter("C"+rs1.getString("active")), rid);
		 } 
		 } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		} 
      
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("atsstudies",  employeeService.listATS_Studies(records.getRid()));
		model.put("analysis",  employeeService.listAnalysis());
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("atslist",  employeeService.listATS());
		model.put("active",  employeeService.listActive(records.getRid()));
		return new ModelAndView("Executive_ATS_Studies", model);
	}
	
	@RequestMapping(value = "/UpdateATS_Studies_Exeecutive.html", method = RequestMethod.POST)
	public ModelAndView UpdateATS_Studies_1(HttpSession session, HttpServletRequest request, @ModelAttribute("command") atsstudies atsstudies,Records records, 
			BindingResult result) {		
employeeService.addATS_Studies(atsstudies);
		
		//int Analyticalstudy_id = analyticalstudy.getAnalyticalstudy_id();
		int rid= (int) session.getAttribute("rid");
		 try {
			 Connection con  = DbUtil.getConnection(); 
		 PreparedStatement stmt1=con.prepareStatement("select * from atsstudies1 where rid="+session.getAttribute("rid")+" AND ats_id="+atsstudies.getAts_id());
		 
		 ResultSet rs1=stmt1.executeQuery();
		 while(rs1.next()){
			//request.getParameter(rs1.getString("active"));
			 int ats_id =rs1.getInt("ats1_id");
			System.out.println("Active:"+request.getParameter(rs1.getString("ats_active")));
			System.out.println("Concentration:"+request.getParameter("C"+rs1.getString("ats_active"))); 
			
			employeeService.updateATS_Studies1(ats_id, request.getParameter(rs1.getString("ats_active")), request.getParameter("C"+rs1.getString("ats_active")), rid);
		 } 
		 } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		} 
      
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("atsstudies",  employeeService.listATS_Studies(records.getRid()));
		model.put("analysis",  employeeService.listAnalysis());
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("atslist",  employeeService.listATS());
		model.put("active",  employeeService.listActive(records.getRid()));
		return new ModelAndView("Executive_ATS_Studies", model);
	}
	
	@RequestMapping(value = "/Executive_DeleteATS_Studies.html", method = RequestMethod.GET)
	public ModelAndView deleteATS_Studies_1(@ModelAttribute("command")  atsstudies atsstudies,Records records,
			BindingResult result) {
		//System.out.println("Active id="+active.getActive_id());
		employeeService.deleteATS_Studies(atsstudies);
		employeeService.deleteATS_Studies1(atsstudies.getAts_id());
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("atsstudies", null);
		model.put("analysis",  employeeService.listAnalysis());
		model.put("atsstudies",  employeeService.listATS_Studies(records.getRid()));
		model.put("records", employeeService.getRecords(records.getRid()));
		model.put("atslist",  employeeService.listATS());
		model.put("active",  employeeService.listActive(records.getRid()));
		return new ModelAndView("Executive_ATS_Studies", model);
	}
	
	@RequestMapping(value = "/Executive_EditATS_Studies.html", method = RequestMethod.GET)
	public ModelAndView editATS_Studies_1(HttpSession session , @ModelAttribute("command")   atsstudies atsstudies,Records records,
			BindingResult result) {	
		session.setAttribute("ats_id", atsstudies.getAts_id());
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("atsstudies", employeeService.getATS_Studies(atsstudies.getAts_id()));
		model.put("analysis",  employeeService.listAnalysis());
		model.put("atslist",  employeeService.listATS());
		model.put("atsstudies1",  employeeService.listATS_Studies(records.getRid()));
		return new ModelAndView("Executive_EditATS_Studies", model);
	}
/*====*/
/*ATS Studies End*/


				/*ATS Studies End*/
					//Image Upload 
	@RequestMapping(value="/Executive_Image_Upload.html" , method = RequestMethod.GET)
	public ModelAndView uploadVideos_1(@ModelAttribute("command") image imageBean,Records records,
			BindingResult result){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("images",  employeeService.listImage(records.getRid()));
		model.put("records", employeeService.getRecords(records.getRid()));
		return new ModelAndView("Executive_Image_Upload", model);
	}	
	@RequestMapping(value="executive_saveImage" , method = RequestMethod.POST)
	public ModelAndView saveImage_1(@ModelAttribute("command") image imageBean,Records records,BindingResult result,
			@RequestParam("file") MultipartFile file)
	{	
		System.out.println("Name:" + imageBean.getName());
		System.out.println("Desc:" + imageBean.getDescription());
		System.out.println("File:" + file.getName());
		System.out.println("ContentType:" + file.getContentType());		
		try {
			Blob blob = Hibernate.createBlob(file.getInputStream());
			imageBean.setName(file.getOriginalFilename());
			imageBean.setPhoto(blob);
			imageBean.setContentType(file.getContentType());
		} catch (IOException e) {
			e.printStackTrace();
		}		
		employeeService.addImage(imageBean);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("images",  employeeService.listImage(records.getRid()));
		model.put("records", employeeService.getRecords(records.getRid()));		
		return new ModelAndView("Executive_Image_Upload", model);
	}	
	@RequestMapping(value="/Executive_EditImage_Upload", method = RequestMethod.GET )
	public ModelAndView editImage_1(@ModelAttribute("command") image imageBean,Records records,BindingResult result)
	{
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("image", employeeService.getImage(imageBean.getId()));
		model.put("images",  employeeService.listImage(records.getRid()));
		/*model.put("records", employeeService.getRecords(imageBean.getRid()));	*/	
		return new ModelAndView("Executive_EditImage_Upload", model);
	}	
	@RequestMapping(value="/deleteImage_executive" , method = RequestMethod.GET )
	public ModelAndView deleteImage_1(@ModelAttribute("command") image imageBean,Records records,BindingResult result)
	{	
		Map<String, Object> model = new HashMap<String, Object>();
		System.out.print("records.getRid() "  + records.getRid());		
		model.put("records", employeeService.getRecords(records.getRid()));	
		model.put("images",  employeeService.listImage(records.getRid()));
		employeeService.deleteImage(imageBean);
		return new ModelAndView("Executive_Image_Upload", model);
	}	
					
					//-------For View Chief manager----------------------------------
					
					@RequestMapping(value="/View_Chief_Manager", method = RequestMethod.GET)
					public ModelAndView listChief_Manager(@ModelAttribute("command") chief_login chief, 
							BindingResult result){
					
							
						String Designation="Chief Manager";
					
						Map<String, Object> model = new HashMap<String, Object>();
						model.put("chief_list", employeeService.getEmployeeList(Designation));
						model.put("chief_records", employeeService.getEmp(chief.getId()));
								//return new ModelAndView("chief_list", model);
								
						
						return new ModelAndView("View_Chief_Manager",model);
					}
					
					@RequestMapping(value = "/DeleteChiefManager.html", method = RequestMethod.GET)
					public ModelAndView delete_Manager(@ModelAttribute("command")  chief_login manager,
							BindingResult result) {
						System.out.println("Active id="+manager.getId());
						employeeService.deleteEmp(manager);
						
						Map<String, Object> model = new HashMap<String, Object>();
						
						model.put("chief_records", employeeService.getEmp(manager.getId()));
						
						return new ModelAndView("View_Chief_Manager");
						//return new ModelAndView("Active_Details");
					}
					
					
					
					
					
					
					@RequestMapping(value = "/EditChiefManager.html", method = RequestMethod.GET)
					public ModelAndView editChief_Manager( @ModelAttribute("command")chief_login managerBean, BindingResult result  )
							{
						
						Map<String, Object> model = new HashMap<String, Object>();
						model.put("chief_records", employeeService.getEmp(managerBean.getId()));
					
						return new ModelAndView("EditChiefManager", model);
					}
				  
					
					
					@RequestMapping(value="/EditsaveManager",method = RequestMethod.POST)
					public ModelAndView EditManager(@Valid @ModelAttribute("command")chief_login managerBean, BindingResult result  ) 
					{
						System.out.println("in EditManager");
						DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						   
					    Calendar calobj = Calendar.getInstance();
					    System.out.println(df.format(calobj.getTime()));
						String regDate=df.format(calobj.getTime());
						
						String UserName=managerBean.getUserName();
						String password=managerBean.getPassword();
						String email1=managerBean.getEmailId();
						System.out.println("registraion date="+regDate.toString());
						List<chief_login> list = employeeService.listChief();
						int i =0;
						String Designation="Chief-Manager";
						
						try
						{
							managerBean.setRegDate(df.parse(regDate));
							
						 employeeService.addManager(managerBean);
						
						}
						catch(Exception ex)
						{
						
							return new ModelAndView("NewManager_Error");
				       
						}
				 
						System.out.println("For Sending Email");
						
						
						
						String subject="UserName and password";
						
						String str ="Dear"+ UserName+",Your UserName is  "+UserName+" and password is ";
						String message1=password+" thank you";
						String message=str.concat(message1);
						
						
						// prints debug info
						System.out.println("To: " + email1);
						System.out.println("Subject: " + subject);
						System.out.println("Message: " + message);
						
						// creates a simple e-mail object
						SimpleMailMessage email = new SimpleMailMessage();
						
						
						email.setTo(email1);
						email.setSubject(subject);
						email.setText(message);
						
						

				      mailSender.send(email);
				      System.out.println("Email send succefully");
						
						
						
						
						
						if (result.hasErrors()) {
							return new ModelAndView("New_Manager");
				        }
				 
				        
						return new ModelAndView("View_Chief_Manager");
					}
					
					
					
					
					///========================For Admin View==============================
					@RequestMapping(value="/View_Admin.html", method = RequestMethod.GET)
					public ModelAndView listAdmin_Manager(@ModelAttribute("command") chief_login chief, 
							BindingResult result){
					
							
						String Designation="Admin";
					
						Map<String, Object> model = new HashMap<String, Object>();
						model.put("chief_list", employeeService.getEmployeeList(Designation));
						model.put("chief_records", employeeService.getEmp(chief.getId()));
								//return new ModelAndView("chief_list", model);
								
						
						return new ModelAndView("View_Admin",model);
					}
					
					@RequestMapping(value = "/DeleteAdmin.html", method = RequestMethod.GET)
					public ModelAndView delete_Admin(@ModelAttribute("command")  chief_login manager,
							BindingResult result) {
						System.out.println("Emp id="+manager.getId());
						employeeService.deleteEmp(manager);
						
						Map<String, Object> model = new HashMap<String, Object>();
						
						model.put("chief_records", employeeService.getEmp(manager.getId()));
						
						return new ModelAndView("View_Admin");
						//return new ModelAndView("Active_Details");
					}
					
					
					
					///================For Excutive ManagerView==============================
					
					@RequestMapping(value="/View_Excutive_Manager.html", method = RequestMethod.GET)
					public ModelAndView listExcutive_Manager(@ModelAttribute("command") chief_login chief, 
							BindingResult result){
					
							
						String Designation="Executive Manager";
					
						Map<String, Object> model = new HashMap<String, Object>();
						model.put("chief_list", employeeService.getEmployeeList(Designation));
						model.put("chief_records", employeeService.getEmp(chief.getId()));
								//return new ModelAndView("chief_list", model);
								
						
						return new ModelAndView("View_Excutive_Manager",model);
					}
					
					@RequestMapping(value = "/DeleteExecutiveManager.html", method = RequestMethod.GET)
					public ModelAndView delete_ExecutiveManager(@ModelAttribute("command")  chief_login manager,
							BindingResult result) {
						System.out.println("Active id="+manager.getId());
						employeeService.deleteEmp(manager);
						
						Map<String, Object> model = new HashMap<String, Object>();
						
						model.put("chief_records", employeeService.getEmp(manager.getId()));
						
						return new ModelAndView("View_Excutive_Manager");
						//return new ModelAndView("Active_Details");
					}
					
					
					
					
					
}
