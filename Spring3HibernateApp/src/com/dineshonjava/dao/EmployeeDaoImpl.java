package com.dineshonjava.dao;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import java.util.Date;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.*;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dineshonjava.bean.SearchBean;
import com.dineshonjava.model.Active;
import com.dineshonjava.model.Composition;

import com.dineshonjava.model.formulation;
import com.dineshonjava.model.image;
import com.dineshonjava.model.recordcomment;
import com.dineshonjava.model.scancopy;
import com.dineshonjava.model.selflifestudy;

import util.DbUtil;

import com.dineshonjava.model.Records;
import com.dineshonjava.model.chief_login;
import com.dineshonjava.model.coldtest;
import com.dineshonjava.model.coldtestdays;
import com.dineshonjava.model.analysis;
import com.dineshonjava.model.analyticalstudy;
import com.dineshonjava.model.analyticalstudy1;
import com.dineshonjava.model.ats;
import com.dineshonjava.model.atsstudies;
import com.dineshonjava.model.atsstudies1;

//
@Repository("employeeDao")
public class EmployeeDaoImpl implements EmployeeDao {
	

	@Autowired
	private SessionFactory sessionFactory;
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<chief_login> listChief() {
		return (List<chief_login>) sessionFactory.getCurrentSession().createCriteria(chief_login.class).list();
	}
	

    /* varsha  start*/
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<formulation> listFormulation() {
		System.out.println("List of formulation");
		//return (List<formulation>) sessionFactory.getCurrentSession().createCriteria(formulation.class).list();
		return (List<formulation>) sessionFactory.getCurrentSession().createQuery("from formulation where status = 'Enabled'").list();
	}
	@Override
	public formulation getFormulation(int id) {
		return (formulation) sessionFactory.getCurrentSession().get(formulation.class, id);
 	}	
	public void addFormulation(formulation formulation) {
		sessionFactory.getCurrentSession().saveOrUpdate(formulation);		
	}
	@Override
	public void deleteFormulation(formulation formulationBean) {
		System.out.println("in  dao delete formualtion..");
		sessionFactory.getCurrentSession().createQuery("DELETE FROM formulation WHERE id = "+formulationBean.getId()).executeUpdate();		
	}
	@SuppressWarnings("unchecked")
	@Transactional
	public List<formulation> getFormulationList(int result, int offset_real) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(formulation.class).addOrder(Order.asc("formulationtype"));
		criteria.setFirstResult(offset_real);
		criteria.setMaxResults(result);
		List<formulation>  formulationList = (List<formulation>)criteria.list();
		return formulationList;
	}
	@Override
	@Transactional
	public int getFormulationSize() {
		return sessionFactory.getCurrentSession().createCriteria(formulation.class).list().size();
	}
	
	
	
	
	public void addAnalysis(analysis analysis) {
		sessionFactory.getCurrentSession().saveOrUpdate(analysis);		
	}
	@Override
	public analysis getAnalysis(int id) {
		return (analysis) sessionFactory.getCurrentSession().get(analysis.class, id);
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<analysis> listAnalysis() {
		/*Session ses = null;
		List<analysis>  list =null;
		try {
			ses =sessionFactory.openSession();
			
			Criteria  cri =ses.createCriteria(analysis.class) ;
			
			  cri.addOrder(Order.asc("analysis_name"));
			  
			  list =cri.list();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return list;*/
		
		return (List<analysis>) sessionFactory.getCurrentSession().createCriteria(analysis.class).list();
	} 
	@Override
	public void deleteAnalysis(analysis analysisBean) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM analysis WHERE id = "+analysisBean.getId()).executeUpdate();		
	}	
	@Override
	@SuppressWarnings("unchecked")
	@Transactional
	public List<analysis> getAnalysisList(int result, int offset_real) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(analysis.class).addOrder(Order.asc("analysisname"));
		criteria.setFirstResult(offset_real);
		criteria.setMaxResults(result);
		List<analysis>  analysisList = criteria.list();
		return analysisList;
	}
	@Override
	@Transactional
	public int getAnalysis_Size() {
		return sessionFactory.getCurrentSession().createCriteria(analysis.class).list().size();
	}
	
	
	
	
	public void addATS(ats atsBean) {		
		sessionFactory.getCurrentSession().saveOrUpdate(atsBean);
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<ats> listATS() {
		//return (List<ats>) sessionFactory.getCurrentSession().createCriteria(ats.class).list();
		return (List<ats>) sessionFactory.getCurrentSession().createQuery("from ats where status = 'Enabled'").list();
	}
	@Override
	public ats getATS(int id) {
		return (ats) sessionFactory.getCurrentSession().get(ats.class, id);
	}
	@Override
	public void deleteATS(ats atsBean) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM ats WHERE id = "+atsBean.getId()).executeUpdate();
	}
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<ats> getATSList(int result, int offset_real) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ats.class).addOrder(Order.asc("days_description"));
		criteria.setFirstResult(offset_real);
		criteria.setMaxResults(result);
		List<ats>  atsList = (List<ats>)criteria.list();
		return atsList;
	}
	@Override
	@Transactional
	public int getATS_Size() {
		return sessionFactory.getCurrentSession().createCriteria(ats.class).list().size();
	}
	
	
	
	
	
	public void addColdTestDays(coldtestdays coldtestdaysBean) {
		sessionFactory.getCurrentSession().saveOrUpdate(coldtestdaysBean);		
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<coldtestdays> listColdTestDays() {
		System.out.println("in listColdTestDays in dao...");
		return (List<coldtestdays>) sessionFactory.getCurrentSession().createCriteria(coldtestdays.class).list();
		//return (List<coldtestdays>) sessionFactory.getCurrentSession().createQuery("from coldtestdays where status = 'Enabled'").list();
	}
	@Override
	public coldtestdays getColdTestDays(int id) {
		return (coldtestdays) sessionFactory.getCurrentSession().get(coldtestdays.class, id);
	}
	@Override
	public void deleteColdTestDays(coldtestdays coldtestdaysBean) {
		
		sessionFactory.getCurrentSession().createQuery("DELETE FROM coldtestdays WHERE id = "+coldtestdaysBean.getId()).executeUpdate();
	}
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<coldtestdays> getColdTestDaysList(int result, int offset_real) {
		System.out.println("in getColdTestDaysList in dao...");
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(coldtestdays.class).addOrder(Order.asc("degree_description"));
		criteria.setFirstResult(offset_real);
		criteria.setMaxResults(result);
		List<coldtestdays> coldtestdaysList = (List<coldtestdays>)criteria.list();
		return coldtestdaysList;
	}
	@Override
	@Transactional
	public int getColdTestDays_Size() {
		return sessionFactory.getCurrentSession().createCriteria(coldtestdays.class).list().size();
	}
	
	
	
	
	
	public void addImage(image imageBean) {
		sessionFactory.getCurrentSession().saveOrUpdate(imageBean);
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<image> listImage() {
		return (List<image>) sessionFactory.getCurrentSession().createCriteria(image.class).list();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<image> listImage(int rid) {
		String hql = "FROM image i WHERE i.rid = :r_id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("r_id",rid);
		List<image> results = query.list();
		return results;	
	}
	@Override
	public image getImage(int id) {
		return (image) sessionFactory.getCurrentSession().get(image.class, id);
	}
	@Override
	public void deleteImage(image imageBean) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM image WHERE id = "+imageBean.getId()).executeUpdate();
	}

	
	/* varsha end*/
	
	
	
	
	
	
	
	
	
	/*Bhushan Start*/
	
	
	//Record Comment Start

		public void addRecord_Comment(recordcomment recordcomment) {
			sessionFactory.getCurrentSession().saveOrUpdate(recordcomment);
			
		}

		@SuppressWarnings("unchecked")
		@Override
		public List<recordcomment> getRecord_Comment(int rid) {
			String hql = "FROM recordcomment a WHERE a.rid = :r_id";
			Query query = sessionFactory.getCurrentSession().createQuery(hql);
			query.setParameter("r_id",rid);			
			List<recordcomment> results = query.list();
			return results;
		}


		@Override
		public void DeleteRecord_Comment(recordcomment recordcomment) {
			sessionFactory.getCurrentSession().createQuery("DELETE FROM recordcomment WHERE comment_id = "+recordcomment.getComment_id()).executeUpdate();

			
		}

		@SuppressWarnings("unchecked")
		@Override
		public List<Records> SerchByExecutive(String executive) {
			return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where research_executive='"+executive+"'").list();

		}
		//Record Comment End
	
	public void addRecords(Records records) {
		sessionFactory.getCurrentSession().saveOrUpdate(records);		
	}
	@Override
	public List<Records> listRecords() {
		return (List<Records>) sessionFactory.getCurrentSession().createCriteria(Records.class).list();
	}
	public Records getRecords(int rid) {
		return (Records) sessionFactory.getCurrentSession().get(Records.class, rid);
	}
	@Override
	public void deleteRecord(Records records) {
		System.out.println("record id in dao = " + records.getRid());
		sessionFactory.getCurrentSession().createQuery("DELETE FROM records WHERE rid = "+records.getRid()).executeUpdate();
	}
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Records> getRecordsList(int result, int offset_real) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Records.class).addOrder(Order.desc("rid"));
		criteria.setFirstResult(offset_real);
		criteria.setMaxResults(result);
		List<Records>  recordsList = (List<Records>)criteria.list();
		return recordsList;
	}
	@Transactional
	public int getRecords_Size() {
		return sessionFactory.getCurrentSession().createCriteria(Records.class).list().size();
	}
	@Override
	public List<Records> serachRecords(SearchBean bean){
		String hql = "FROM active a WHERE a.rid = :r_id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		//query.setParameter("r_id",rid);
		List<Records> results = query.list();
		return results;
	}
	
	
	
	
	@Override
	public List<Active> listActive() {
		return (List<Active>) sessionFactory.getCurrentSession().createCriteria(Active.class).list();		
	}	
	@SuppressWarnings("unchecked")
	@Override
	public List<Active> listActive(int rid) {
		/*String hql = "FROM active a WHERE a.rid = :r_id ORDER BY desc a.active_id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("r_id",rid);
		List<Active> results = query.list();
		return results;		*/
		return (List<Active>) sessionFactory.getCurrentSession().createQuery("from active where rid="+rid+" ORDER BY active_id DESC").list();

	}	
	public void addActive(Active active) {
		sessionFactory.getCurrentSession().saveOrUpdate(active);		
	}
	@Override
	public void deleteActive(Active active) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM active WHERE active_id = "+active.getActive_id()).executeUpdate();
	}
	@Override
	public Active getActive(int active_id) {
		return (Active) sessionFactory.getCurrentSession().get(Active.class, active_id);
	}
	
	
	
	@SuppressWarnings("unchecked") 
	@Override
	public List<Composition> listComposition() {
		return (List<Composition>) sessionFactory.getCurrentSession().createCriteria(Composition.class).list();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Composition> listComposition(int rid) {
		/*String hql = "FROM composition c WHERE c.rid = :r_id ORDER BY c.constituent";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("r_id",rid);
		List<Composition> results = query.list();
		return results;*/
		return (List<Composition>) sessionFactory.getCurrentSession().createQuery("from composition where rid="+rid+" ORDER BY composition_id DESC").list();

	}
	public void addComposition(Composition composition) {
		sessionFactory.getCurrentSession().saveOrUpdate(composition);
	}
	@Override
	public void deleteComposition(Composition composition) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM composition WHERE composition_id = "+composition.getComposition_id()).executeUpdate();
	}
	@Override
	public Composition getComposition(int conposition_id) {
		return (Composition) sessionFactory.getCurrentSession().get(Composition.class, conposition_id);
	}
	
	
	
	@Override
	public selflifestudy getSelfLife_Studies(int selflife_id) {
		return (selflifestudy) sessionFactory.getCurrentSession().get(selflifestudy.class, selflife_id);

	}


	@Override
	public void deleteSelfLife_Studies(selflifestudy selflifestudy) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM selflifestudy WHERE Selflife_id = "+selflifestudy.getSelflife_id()).executeUpdate();

	}


	
	public void addSelfLife_Studies(selflifestudy selflifestudy) {
		sessionFactory.getCurrentSession().saveOrUpdate(selflifestudy);
		
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<selflifestudy> listSelfLife_Studies() {
		return (List<selflifestudy>) sessionFactory.getCurrentSession().createCriteria(selflifestudy.class).list();

	}


	@SuppressWarnings("unchecked")
	@Override
	public List<selflifestudy> listSelfLife_Studies(int rid) {
		/*String hql = "FROM selflifestudy a WHERE a.rid = :r_id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("r_id",rid);
		List<selflifestudy> results = query.list();
		return results;*/
		return (List<selflifestudy>) sessionFactory.getCurrentSession().createQuery("from selflifestudy where rid="+rid+" ORDER BY selflife_id DESC").list();

	}
	
	
	
	public void addAnalytical_Studies(analyticalstudy analyticalstudy) {
		sessionFactory.getCurrentSession().saveOrUpdate(analyticalstudy);		
	}
	
	@Override
	public void addAnalytical_Studies1(int analyticalstudy_id, String active, String concentration, int rid) {
		//sessionFactory.getCurrentSession().createQuery("insert into analyticalstudy1 (analyticalstudy_id, analytical_active, concentration) values ("+analyticalstudy_id+", '"+active+"', '"+concentration+"')").executeUpdate();
		SessionFactory factory= new AnnotationConfiguration().configure().buildSessionFactory();
	    Session session= factory.openSession();
	    Transaction t=session.beginTransaction();
	    
	    analyticalstudy1 analyticalstudy1 = new analyticalstudy1();
	    analyticalstudy1.setAnalyticalstudy_id(analyticalstudy_id);
	    analyticalstudy1.setAnalytical_active(active);
	    analyticalstudy1.setConcentration(concentration);
	    analyticalstudy1.setRid(rid);
	    session.persist(analyticalstudy1);
	    t.commit();
	    session.close();
	}
	
	@Override
	public void updateAnalytical_Studies1(int analyticalstudy_id, String active, String concentration, int rid) {
		
		
		//sessionFactory.getCurrentSession().createQuery("update analyticalstudy1 set analytical_active= '"+active+"', concentration='"+concentration+"' where analyticalstudy_id1="+ analyticalstudy_id).executeUpdate();
		SessionFactory factory= new AnnotationConfiguration().configure().buildSessionFactory();
	    Session session= factory.openSession();
	    Transaction t=session.beginTransaction();
	    
	  org.hibernate.Query query1=session.createQuery("update analyticalstudy1 set analytical_active=:a, concentration=:c where analyticalstudy1_id=:n");
	    query1.setParameter("a" , active);
	    query1.setParameter("c" , concentration);
	    query1.setParameter("n" , analyticalstudy_id);
	    //query1.setParameter("r" , rid);
	    int count=query1.executeUpdate();
	    t.commit();
	    session.close();
		
	}

	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<analyticalstudy> listAnalytical_Studies() {
		return (List<analyticalstudy>) sessionFactory.getCurrentSession().createCriteria(analyticalstudy.class).list();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<analyticalstudy> listAnalytical_Studies(int rid) {
		/*String hql = "FROM analyticalstudy a WHERE a.rid = :r_id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("r_id",rid);
		List<analyticalstudy> results = query.list();
		return results;*/
		return (List<analyticalstudy>) sessionFactory.getCurrentSession().createQuery("from analyticalstudy where rid="+rid+" ORDER BY analyticalstudy_id DESC").list();

	}
	
	@Override
	public void deleteAnalytical_Studies1(String active, int rid) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM analyticalstudy1 WHERE analytical_active = '"+active+"' AND rid="+rid).executeUpdate();

		
	}
	
	@Override
	public void deleteAnalytical_Studies1(int analyticalstudy_id) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM analyticalstudy1 WHERE analyticalstudy_id = "+analyticalstudy_id).executeUpdate();

		
	}
	
	@Override
	public void deleteAnalytical_Studies(analyticalstudy analyticalstudy) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM analyticalstudy WHERE analyticalstudy_id = "+analyticalstudy.getAnalyticalstudy_id()).executeUpdate();
	}
	@Override
	public analyticalstudy getAnalytical_Studies(int analyticalstudy_id) {
		return (analyticalstudy) sessionFactory.getCurrentSession().get(analyticalstudy.class, analyticalstudy_id);
	}


	
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<scancopy> listScanned_Copies() {
		return (List<scancopy>) sessionFactory.getCurrentSession().createCriteria(scancopy.class).list();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<scancopy> listScanned_Copies(int rid) {
		String hql = "FROM scancopy s WHERE s.rid = :r_id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("r_id",rid);
		List<scancopy> results = query.list();
		return results;
	}
	@Override
	public String  addScanned_Copies(scancopy scancopy)  {
		System.out.println("in dao ");
		//sessionFactory.getCurrentSession().saveOrUpdate(scancopy);
		String hql = "FROM scancopy a WHERE a.name = :name";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("name",scancopy.getName());
		List<scancopy> results = query.list();
		if(!results.isEmpty())
		{
			return "fail";
		}
		
		sessionFactory.getCurrentSession().saveOrUpdate(scancopy);
		 return "success";
		
		/*Connection con  = DbUtil.getConnection();
		String sql1 = "select name from scancopy";
		 PreparedStatement stmt;
		 ResultSet rs;
		 try {
				stmt = con.prepareStatement(sql1);
				rs = stmt.executeQuery();
				while(rs.next())
				{
					if(rs.getString(1).equals(scancopy.getName()))
					{
						return "fail";
					}				
				}*/
				/*String sql = "INSERT INTO scancopy"
						+ "(rid,name) VALUES"
						+ "(?,?)";
				 PreparedStatement stmt1;
				 stmt1 = con.prepareStatement(sql);
					stmt1.setInt(1, scancopy.getRid());
					 stmt1.setString(2, scancopy.getName());
					 stmt1.executeUpdate();*/
				
		/* }
	 catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return "fail";
	}*/
		
		
		 
		 //System.out.println("select * from active where rid="+session.getAttribute("rid"));
		 
	}
	@Override
	public void deleteScanned_Copies(scancopy scancopy) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM scancopy WHERE scan_id = "+scancopy.getScan_id()).executeUpdate();		
	}
	@Override
	public scancopy getScanned_Copies(int scan_id) {
		return (scancopy) sessionFactory.getCurrentSession().get(scancopy.class, scan_id);
	}


	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<coldtest> listCold_Test() {
		return (List<coldtest>) sessionFactory.getCurrentSession().createCriteria(coldtest.class).list();
	}	
	@SuppressWarnings("unchecked")
	@Override
	public List<coldtest> listCold_Test(int rid) {
		String hql = "FROM coldtest c WHERE c.rid = :r_id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("r_id",rid);
		List<coldtest> results = query.list();
		return results;
	}	
	public void addCold_Test(coldtest coldtest) {
		sessionFactory.getCurrentSession().saveOrUpdate(coldtest);		
	}	
	public void deleteCold_Test(coldtest coldtest) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM coldtest WHERE cold_id = "+coldtest.getCold_id()).executeUpdate();		
	}
	@Override
	public coldtest getCold_Test(int cold_id) {
		return (coldtest) sessionFactory.getCurrentSession().get(coldtest.class, cold_id);
	}


	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<atsstudies> listATS_Studies() {
		return (List<atsstudies>) sessionFactory.getCurrentSession().createCriteria(atsstudies.class).list();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<atsstudies> listATS_Studies(int rid) {
		/*String hql = "FROM atsstudies a WHERE a.rid = :r_id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("r_id",rid);
		List<atsstudies> results = query.list();
		return results;*/
		return (List<atsstudies>) sessionFactory.getCurrentSession().createQuery("from atsstudies where rid="+rid+" ORDER BY ats_id DESC").list();

		
		
		
	}
	@Override
	public atsstudies getATS_Studies(int ats_id) {
		return (atsstudies) sessionFactory.getCurrentSession().get(atsstudies.class, ats_id);
	}
	@Override
	public void deleteATS_Studies(atsstudies atsstudies) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM atsstudies WHERE ats_id = "+atsstudies.getAts_id()).executeUpdate();		
	}
	public void addATS_Studies(atsstudies atsstudies) {
		sessionFactory.getCurrentSession().saveOrUpdate(atsstudies);		
	}

	
//SEARCH RECORDS OF CHIEF ONLY
	
								@SuppressWarnings("unchecked")
								@Override
								public List<Records> ChiefRecordSerchBy_Status_Active(String status, String active, String username) {
									//return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where status='"+status+"' And records_active LIKE'%"+active+"%' AND research_executive='"+username+"'").list();
									return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where status='"+status+"' And records_active LIKE'%"+active+"%' AND research_executive='"+username+"'").list();
								}
							
							
								@SuppressWarnings("unchecked")
								@Override
								public List<Records> ChiefRecordSerchBy_Fomulation_Active(String formulation, String active, String username) {
									//return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where formulation='"+formulation+"' And records_active LIKE'%"+active+"%' AND research_executive='"+username+"'").list();
									return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where formulation='"+formulation+"' And records_active LIKE'%"+active+"%' AND research_executive='"+username+"'").list();
								}
							
							
								@SuppressWarnings("unchecked")
								@Override
								public List<Records> ChiefRecordSerchBy_Fomulation_Status(String formulation, String status, String username) {
									//return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where formulation='"+formulation+"' AND status='"+status+"'AND research_executive='"+username+"'").list();
									return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where formulation='"+formulation+"' AND status='"+status+"'AND research_executive='"+username+"'").list();
								}
							
							
								@SuppressWarnings("unchecked")
								@Override
								public List<Records> ChiefRecordSerchBy_Fomulation_Status_Active(String formulation, String status, String active, String username) {
									//return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where formulation='"+formulation+"' AND status='"+status+"' And records_active LIKE'%"+active+"%'AND research_executive='"+username+"'").list();
									return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where formulation='"+formulation+"' AND status='"+status+"' And records_active LIKE'%"+active+"%'AND research_executive='"+username+"'").list();
								}
								
								@SuppressWarnings("unchecked")
								@Override
								public List<Records> ChiefRecordSerchByFormulation(String formulation, String username) {
									// TODO Auto-generated method stub
									//return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where formulation='"+formulation+"'AND research_executive='"+username+"'").list();
									return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where formulation='"+formulation+"'AND research_executive='"+username+"'").list();
								}
							
							
							
							
								@SuppressWarnings("unchecked")
								@Override
								public List<Records> ChiefRecordSerchByActive(String active, String username) {
									//return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where research_executive='"+username+"'And ( records_active='"+active+"'OR records_active LIKE '%"+active+"%')").list();
									return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where research_executive='"+username+"'And ( records_active='"+active+"'OR records_active LIKE '%"+active+"%')").list();
								}
							
							
							
							
								@SuppressWarnings("unchecked")
								@Override
								public List<Records> ChiefRecordSerchByStatus(String status, String username) {
									//return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where status='"+status+"'AND research_executive='"+username+"'").list();
									return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where status='"+status+"'AND research_executive='"+username+"'").list();
								}
							
							
							
							
								@SuppressWarnings("unchecked")
								@Override
								public List<Records> ChiefRecordSerchByDate(Date todate, Date fromdate, String username) {
									/*SimpleDateFormat s= new SimpleDateFormat("yyyy/MM/dd");
									Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Records.class)
											   .add(Restrictions.between("date", s.format(fromdate) , s.format(todate) ));
											List<Records> listCustomer = criteria.list();
											return listCustomer;*/
									SimpleDateFormat s= new SimpleDateFormat("yyyy/MM/dd");
									return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where date BETWEEN '"+s.format(fromdate)+"' AND '"+s.format(todate)+"'AND research_executive='"+username+"'").list();
									
								}
								
								@SuppressWarnings("unchecked")
								@Override
								public List<Records> ChiefRecordSerchBySingleDate(Date todate, Date fromdate, String username) {
									SimpleDateFormat s= new SimpleDateFormat("yyyy/MM/dd");
									return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where date='"+s.format(todate)+"' or date='"+s.format(fromdate)+"'AND research_executive='"+username+"'").list();
								}
								
								@SuppressWarnings("unchecked")
								@Override
								public List<Records> listChiefRecord(String username) {
									//return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where research_executive='"+username+"'").list();
									return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where research_executive='"+username+"'").list();
								}

	
	//SEARCH RECORDS OF CHIEF ONLY
	
	
// Search for all records
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Records> SerchBy_Status_Active(String status, String active) {
		//return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where status='"+status+"' And records_active LIKE'%"+active+"%'").list();
		return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where status='"+status+"' And records_active LIKE'%"+active+"%'").list();
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Records> SerchBy_Fomulation_Active(String formulation, String active) {
		//return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where formulation='"+formulation+"' And records_active LIKE'%"+active+"%'").list();
		return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where formulation='"+formulation+"' And records_active LIKE'%"+active+"%'").list();
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Records> SerchBy_Fomulation_Status(String formulation, String status) {
		//return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where formulation='"+formulation+"' AND status='"+status+"'").list();
		return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where formulation='"+formulation+"' AND status='"+status+"'").list();
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Records> SerchBy_Fomulation_Status_Active(String formulation, String status, String active) {
		//return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where formulation='"+formulation+"' AND status='"+status+"' And records_active LIKE'%"+active+"%'").list();
		return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where formulation='"+formulation+"' AND status='"+status+"' And records_active LIKE'%"+active+"%'").list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Records> SerchByFormulation(String formulation) {
		// TODO Auto-generated method stub
		//return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where formulation='"+formulation+"'").list();
		return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where formulation='"+formulation+"'").list();
	}




	@SuppressWarnings("unchecked")
	@Override
	public List<Records> SerchByActive(String active) {
		//return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where records_active='"+active+"'OR records_active LIKE '%"+active+"%'").list();
		return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where records_active='"+active+"'OR records_active LIKE '%"+active+"%'").list();
	}




	@SuppressWarnings("unchecked")
	@Override
	public List<Records> SerchByStatus(String status) {
		//return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where status='"+status+"'").list();
		return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where status='"+status+"'").list();
	}




	@SuppressWarnings("unchecked")
	@Override
	public List<Records> SerchByDate(Date todate, Date fromdate, String username) {
		//SimpleDateFormat s= new SimpleDateFormat("yyyy/MM/dd");
		/*Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Records.class)
				   .add(Restrictions.between("date", s.format(fromdate) , s.format(todate) ));
				List<Records> listCustomer = criteria.list();
				return listCustomer;*/
		//return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where date BETWEEN '"+s.format(fromdate)+"' AND '"+s.format(todate)+"'AND research_executive='"+username+"'").list();
		SimpleDateFormat s= new SimpleDateFormat("yyyy/MM/dd");
		/*Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Records.class)
		.add(Restrictions.between("date", s.format(fromdate) , s.format(todate) ));
		List<Records> listCustomer = criteria.list();
		return listCustomer;*/
		return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where date BETWEEN '"+s.format(fromdate)+"' AND '"+s.format(todate)+"'AND research_executive='"+username+"'").list();

		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Records> SerchByDate(Date todate, Date fromdate) {
		/*SimpleDateFormat s= new SimpleDateFormat("yyyy/MM/dd");
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Records.class)
				   .add(Restrictions.between("date", s.format(fromdate) , s.format(todate) ));
				List<Records> listCustomer = criteria.list();
				return listCustomer;*/
		SimpleDateFormat s= new SimpleDateFormat("yyyy/MM/dd");
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Records.class)
		.add(Restrictions.between("date", s.format(fromdate) , s.format(todate) ));
		List<Records> listCustomer = criteria.list();
		return listCustomer;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Records> SerchBySingleDate(Date todate) {
		//SimpleDateFormat s= new SimpleDateFormat("yyyy/MM/dd");
		//return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where date='"+s.format(todate)+"' or date='"+s.format(fromdate)+"'").list();
		SimpleDateFormat s= new SimpleDateFormat("yyyy/MM/dd");
		return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where date='"+s.format(todate)+"'").list();

	}
	  
	@SuppressWarnings("unchecked")
	@Override
	public List<Records> SerchBySingleDate1(Date fromdate) {
		//SimpleDateFormat s= new SimpleDateFormat("yyyy/MM/dd");
		//return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where date='"+s.format(todate)+"' or date='"+s.format(fromdate)+"'").list();
		SimpleDateFormat s= new SimpleDateFormat("yyyy/MM/dd");
		return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where date='"+s.format(fromdate)+"'").list();

	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Records> TodayRecordCount(Date format) throws HibernateException, ParseException {
		/*SimpleDateFormat s= new SimpleDateFormat("yyyy/MM/dd");
		System.out.println("Todays date is"+s.format(format));
		String date=s.format(format);
		DateFormat df = new SimpleDateFormat("mm/dd/yyyy");			
				return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where date='"+s.format(format)+"'").list();
			*/
		SimpleDateFormat s= new SimpleDateFormat("yyyy/MM/dd");
		System.out.println("Todays date is"+s.format(format));
		String date=s.format(format);
		DateFormat df = new SimpleDateFormat("mm/dd/yyyy");
		return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where date='"+s.format(format)+"'").list();


		

	}




	@SuppressWarnings("unchecked")
	@Override
	public List<Active> List_Chief_Preview_Active(int rid) {
		return (List<Active>) sessionFactory.getCurrentSession().createQuery("from active where rid="+rid).list();
		
	}




	@SuppressWarnings("unchecked")
	@Override
	public List<Composition> List_Chief_Preview_Composition(int rid) {
		return (List<Composition>) sessionFactory.getCurrentSession().createQuery("from composition where rid="+rid).list();
		
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<analyticalstudy> List_Chief_Preview_Analytical_Studies(int rid) {
		return (List<analyticalstudy>) sessionFactory.getCurrentSession().createQuery("from analyticalstudy where rid="+rid).list();
		
	}




	@SuppressWarnings("unchecked")
	@Override
	public List<atsstudies> List_Chief_Preview_ATS_Studies(int rid) {
		return (List<atsstudies>) sessionFactory.getCurrentSession().createQuery("from atsstudies where rid="+rid).list();
		
	}




	@SuppressWarnings("unchecked")
	@Override
	public List<coldtest> List_Chief_Preview_Cold_Test(int rid) {
		return (List<coldtest>) sessionFactory.getCurrentSession().createQuery("from coldtest where rid="+rid).list();
		
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<chief_login> ListChiefLogin() {
		return (List<chief_login>) sessionFactory.getCurrentSession().createCriteria(chief_login.class).list();

	}

	
	
	
	
	@Override
	public void addATS_Studies1(int ats_id, String active, String concentration, int rid) {
		SessionFactory factory= new AnnotationConfiguration().configure().buildSessionFactory();
	    Session session= factory.openSession();
	    Transaction t=session.beginTransaction();
	    
	    atsstudies1 atsstudies1 = new atsstudies1();
	    atsstudies1.setAts_id(ats_id);;
	    atsstudies1.setAts_active(active);;
	    atsstudies1.setConcentration(concentration);;
	    atsstudies1.setRid(rid);;
	    session.persist(atsstudies1);
	    t.commit();
	    session.close();
		
	}


	@Override
	public void deleteATS_Studies1(String activevalue, int rid) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM atsstudies1 WHERE ats_active = '"+activevalue+"' AND rid="+rid).executeUpdate();

		
	}


	@Override
	public void deleteATS_Studies1(int ats_id) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM atsstudies1 WHERE ats_id = "+ats_id).executeUpdate();

		
	}


	@Override
	public void updateATS_Studies1(int ats_id, String active, String concentration, int rid) {
		
		//sessionFactory.getCurrentSession().createQuery("update analyticalstudy1 set analytical_active= '"+active+"', concentration='"+concentration+"' where analyticalstudy_id1="+ analyticalstudy_id).executeUpdate();
		SessionFactory factory= new AnnotationConfiguration().configure().buildSessionFactory();
	    Session session= factory.openSession();
	    Transaction t=session.beginTransaction();
	    
	  org.hibernate.Query query1=session.createQuery("update atsstudies1 set ats_active=:a, concentration=:c where ats1_id=:n");
	    query1.setParameter("a" , active);
	    query1.setParameter("c" , concentration);
	    query1.setParameter("n" , ats_id);
	    //query1.setParameter("r" , rid);
	    int count=query1.executeUpdate();
	    t.commit();
	    session.close();
	}
	/*Bhushan end*/
	
	
	
	
	/*Dipali start*/

	@Override
	public void addchief(chief_login cf) {
		sessionFactory.getCurrentSession().saveOrUpdate(cf);
	}
	@Override
	public void addManager(chief_login manager) {
		sessionFactory.getCurrentSession().saveOrUpdate(manager);		
	}




	

	
	@Override
	public List<chief_login> getEmployeeList(String chief) {
		// TODO Auto-generated method stub
		
		System.out.println("in  dao Employee get..");
		String hql = "FROM chief_login s WHERE s.designation = :designation";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("designation",chief);
		List<chief_login> results = query.list();
		return results;
		
	}
	
	@Override
	public void addManager3(chief_login manager) {
		// TODO Auto-generated method stub
		
	}




	@Override
	public chief_login getEmp(int emp_id) {
		// TODO Auto-generated method stub
		return (chief_login) sessionFactory.getCurrentSession().get(chief_login.class,emp_id);
	}




	@Override
	public void deleteEmp(chief_login managerBean) {
		// TODO Auto-generated method stub
		System.out.println("in  dao delete emp..");
		sessionFactory.getCurrentSession().createQuery("DELETE FROM chief_login WHERE id = "+managerBean.getId()).executeUpdate();		
	}
 
	@SuppressWarnings("unchecked")
	@Override
	public List<Records> SerchByDate1(Date todate , String username) {
		// TODO Auto-generated method stub
		SimpleDateFormat s= new SimpleDateFormat("yyyy/MM/dd");
		
		/*String hql = "FROM records s1 WHERE s1.date = :todate ";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("todate",s.format(todate));
		List<Records> results = query.list();
		return results;*/
		return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where date= '"+s.format(todate)+"'AND research_executive='"+username+"'").list();
		
		
		
	}
	@Override
	public List<Records> listRemainder1(String userid) {
		// TODO Auto-generated method stub
		System.out.println("In List from username Remaindder");
		//return (List<Records>) sessionFactory.getCurrentSession().createCriteria(Records.class).list();
		
		Date date=new Date();
		Calendar calendar = Calendar.getInstance();
		date=calendar.getTime(); 
		SimpleDateFormat s;
		//   s=new SimpleDateFormat("MM/dd/yyyy");

		s=new SimpleDateFormat("yyyy/MM/dd");
		String currentdate=s.format(date);

		System.out.println("Date is"+currentdate);
		System.out.println(s.format(date));
		
		
		
		
		return(List<Records>)sessionFactory.getCurrentSession().createQuery("from records where reminder_date >='"+currentdate+"' and research_executive='"+userid+"' ").list();
	}
	
	

	/*Dipali end*/
	
	
	@Override
	public List<Records> listRemainder() {
		
		System.out.println("In List Remaindder");
		//return (List<Records>) sessionFactory.getCurrentSession().createCriteria(Records.class).list();
		
		Date date=new Date();
		Calendar calendar = Calendar.getInstance();
		date=calendar.getTime(); 
		SimpleDateFormat s;
		//   s=new SimpleDateFormat("MM/dd/yyyy");

		s=new SimpleDateFormat("yyyy/MM/dd");
		String currentdate=s.format(date);

		System.out.println("Date is"+currentdate);
		System.out.println(s.format(date));
		
		
		
		
		return(List<Records>)sessionFactory.getCurrentSession().createQuery("from records where reminder_date >='"+currentdate+"'").list();
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Records> ListRecords() {
		return (List<Records>) sessionFactory.getCurrentSession().createCriteria(Records.class).list();

	}


	@Override
	public List<Records> getRecordsList1(int result, int offset_real, String username) {
		// TODO Auto-generated method stub
		
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Records.class). add(Restrictions.eq("research_executive",username)).addOrder(Order.desc("rid"));
		criteria.setFirstResult(offset_real);
		criteria.setMaxResults(result);
		List<Records>  recordsList = (List<Records>)criteria.list();
		return recordsList;
		
		
		
		//return null;
	}


	


	
		
		
		
		
		
		
		
		//return null;
	
 

	

	

	

	


	




	
 





	

	





	

	

	

}
