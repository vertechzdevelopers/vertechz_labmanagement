package com.dineshonjava.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileSearch {
	private String fileNameToSearch;
	  private List<String> result = new ArrayList<String>();

	  public String getFileNameToSearch() {
		return fileNameToSearch;
	  }

	  public void setFileNameToSearch(String fileNameToSearch) {
		this.fileNameToSearch = fileNameToSearch;
	  }

	  public List<String> getResult() {
		return result;
	  }

	  public void searchDirectory(File directory, String fileNameToSearch) {

			setFileNameToSearch(fileNameToSearch);

			if (directory.isDirectory()) {
			    search(directory);
			} else {
			    System.out.println(directory.getAbsoluteFile() + " is not a directory!");
			}

		  }
	 
		  private void search(File file) {

			if (file.isDirectory())
			{
			  System.out.println("Searching directory ... " + file.getAbsoluteFile());

		            //do you have permission to read this directory?
			    if (file.canRead()) 
			    {
			    	System.out.println(" permission to read this directory");
					for (File temp : file.listFiles()) 
					{
						System.out.println(" file temp " + temp);
					    if (temp.isDirectory())
					    {
					    	System.out.println(" in if temp.isDirectory() ");
					    	search(temp);
					    } 
					    else 
					    {
					    	System.out.println(" in else file temp name " + temp.getName());
					    	System.out.println(" in else getFileNameToSearch() " + getFileNameToSearch());
					    	if (getFileNameToSearch().equals(temp.getName()))
					    	{
					    		System.out.println(temp.getName() + " is added into results ");
					    		result.add(temp.getAbsoluteFile().toString());
					    	}
	
					    }
				    }

			    } 
			    else 
			    {
			    	System.out.println(file.getAbsoluteFile() + "Permission Denied");
			    }
		    }

		  }
		 
		public void deleteFile(File dir, String name) {
			File delfile = new File(dir.getAbsolutePath()
					+ File.separator + name);
			if(delfile.delete()){
    			System.out.println(delfile.getName() + " is deleted!");
    		}else{
    			System.out.println("Delete operation is failed.");
    		}
			
		}
}
