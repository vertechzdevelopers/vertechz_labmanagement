package com.dineshonjava.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name="coldtestdays")
public class coldtestdays {
	@Id @GeneratedValue
	private int id;
	
	@Column(name="degree_description")
	private int degree_description;
	
	@Column(name="status")
	private String status;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDegree_description() {
		return degree_description;
	}

	public void setDegree_description(int degree_description) {
		this.degree_description = degree_description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	

}
