package com.dineshonjava.model;



import java.io.File;
import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.validator.constraints.NotEmpty;




@Entity
@Table(name="scancopy",uniqueConstraints={@UniqueConstraint(columnNames={"name","name"})})
public class scancopy {	
	@Id
	@GeneratedValue
	private int scan_id;	
	
	private int rid;	
	/*@Lob
	private Blob scanimage;
	
	private File scanfile;*/
	@NotEmpty
	@Column(name="name",nullable=false)
	private String name;
	
	
	
	public String getName() {
		return name;
	}
	/*public Blob getScanimage() {
		return scanimage;
	}
	public void setScanimage(Blob scanimage) {
		this.scanimage = scanimage;
	}
	public File getScanfile() {
		return scanfile;
	}
	public void setScanfile(File scanfile) {
		this.scanfile = scanfile;
	}*/
	
	public void setName(String name) {
		this.name = name;
	}		
	
	public int getScan_id() {
		return scan_id;
	}
	public void setScan_id(int scan_id) {
		this.scan_id = scan_id;
	}
	public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}
	
	
	

}
